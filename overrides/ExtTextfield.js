Ext.define('Admin.ExtTextfield', {
    override: 'Ext.form.field.Text',
    initComponent: function () {
        this.callParent(arguments);
        if (this.allowBlank === false && this.readOnly === false) {
            this.beforeLabelTextTpl = '<span style="color:red;font-weight:bold" data-qtip="必填项">*</span>';
        }
    }
});
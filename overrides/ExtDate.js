Ext.define('Admin.ExtDate', {
    override: 'Ext.form.field.Date',
    format: 'Y-m-d'
});
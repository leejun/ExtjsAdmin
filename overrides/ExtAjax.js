Ext.define('Admin.ExtAjax', {
    override: 'Ext.data.Connection',

    /*重写Ext.Ajax.request底层方法 每次请求携带请求头(token)信息*/
    request: function (options) {
        var me = this;

        var sessionToken = Ext.util.LocalStorage.get('ext').getItem('auth-token');
        var jwtToken = Ext.util.LocalStorage.get('ext').getItem('jwt-token');
        options.headers = {'auth-token': sessionToken, 'jwt-token': jwtToken};
        console.log('Admin.ExtAjax.request 设置请求头 :--> ' + Ext.JSON.encode(options.headers));
        me.callParent(arguments);
    }
});

/*请求前遮罩处理*/
Ext.Ajax.on('beforerequest', function (conn, options, eOpts) {
    if (options.maskContainer) {
        console.log("Admin.ExtAjax.beforerequest:--> 设置遮罩开始");
        options.maskContainer.setLoading('操作中...');
    }
});

/*请求成功遮罩处理*/
Ext.Ajax.on('requestcomplete', function (conn, response, options, eOpts) {
    if (options.maskContainer) {
        console.log("Admin.ExtAjax.requestcomplete:--> 设置遮罩关闭");
        options.maskContainer.setLoading(false);
    }

    //Jwt自动登录时 服务器会重新下发auth-token
    var sessionId = response.getResponseHeader('auth-token');
    if(sessionId) {
        console.log("响应头中发现(auth-token)设置到浏览器缓存中 :-> " + sessionId);
        Ext.util.LocalStorage.get('ext').setItem('auth-token', sessionId);
    }
});

/*请求异常遮罩处理*/
Ext.Ajax.on('requestexception', function (conn, response, options, eOpts) {
    if (options.maskContainer) {
        console.log("Admin.ExtAjax.requestexception:--> 设置遮罩关闭");
        options.maskContainer.setLoading(false);
    }

    //Ajax请求异常-弹出错误提示
    if (response.statusText === 'communication failure') {
        Ext.popup.Msg('提示信息', '与服务器连接超时');
        return;
    }

    Ext.popup.Msg('提示信息', '网络请求出现错误');
});
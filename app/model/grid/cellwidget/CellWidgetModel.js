Ext.define('Admin.model.grid.cellwidget.CellWidgetModel', {
    extend: 'Ext.data.Model',

    //name需要与后端json对应上
    fields: [
        {name: 'id', type: 'string'},
        {name: 'name', type: 'string'},
        {name: 'progress', type: 'float'},
        {name: 'mode', type: 'string'},
        {name: 'sequence1', type: 'array'},
        {name: 'sequence2', type: 'array'},
        {name: 'sequence3', type: 'array'},
        {name: 'sequence4', type: 'array'},
        {name: 'sequence5', type: 'array'},
        {name: 'sequence6', type: 'array'},
        {name: 'sequence7', type: 'array'}
    ]
});
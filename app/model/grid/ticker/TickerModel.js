Ext.define('Admin.model.grid.ticker.TickerModel', {
    extend: 'Ext.data.Model',

    fields: [
        { name: 'name' },
        { name: 'phone', type: 'string' },
        { name: 'price', type: 'float' },
        { name: 'priceChange', type: 'float' },
        { name: 'priceChangePct', type: 'float' },
        { name: 'industry' },
        { name: 'desc' },
        { name: 'priceLastChange', type: 'date', dateReadFormat: 'n/j' },

        {
            //额外增加的字段 用来存储价格(price)数组
            name: 'trend',
            calculate: function(data) {
                //data代表一条数据行 如果trend不存在 则创建一个数组并且赋值
                var trend = data['trend'] || (data['trend'] = []);

                //添加到数组中
                trend.push(data.price);

                if (trend.length > 10) {
                    //如果长度大于10 则移除1个 始终让数组保持10个元素
                    trend.shift();
                }

                return trend;
            },

            // It's the same array. But we need Model#set to see it as modified so it
            // is flushed to the UI
            isEqual: function() {
                return false;
            }
        },

        //根据历史加个计算当前改变的差值
        {
            name: 'change',
            type: 'float',
            calculate: function(data) {
                var trend = data.trend,
                    len = trend.length;
                //计算改变的值 用最后一个减去前面一个
                return len > 1 ? trend[len - 1] - trend[len - 2] : 0;
            }
        },

        // 计算字段。 取决于价格历史和最近的变化被填充。
        {
            name: 'pctChange',
            type: 'float',
            calculate: function(data) {
                var trend = data.trend,
                    len = trend.length;

                return len > 1 ? (data.change / trend[len - 2]) * 100 : 0;
            }
        },

        //加个最后改变时间
        {
            name: 'lastChange',
            type: 'date',
            depends: ['price'],
            // 每当价格变化时，此方法就会运行。
            // 此字段为纯计算值，不能编辑。
            calculate: function() {
                return new Date();
            }
        },


        // Rating dependent upon last price change performance 0 = best, 2 = worst
        {
            name: 'rating',
            type: 'int',

            // Use a converter to only derive the value onces on record creation
            convert: function(value, record) {
                var data = record.data,
                    pct = data.pctChange;

                // Only calculate it first time.
                if (!data.hasOwnProperty('rating')) {
                    return (pct < -5) ? 2 : ((pct < 5) ? 1 : 0);
                }

                return value;
            }
        }
    ],

    //修改价格price为随机值
    addPriceTick: function() {
        //设置数据后不显示红色角标
        this.set('price', this.generateNewPrice(), {
            dirty: false
        });
    },

    //生成一个随机价格
    generateNewPrice: function() {
        var newPrice = Math.abs(this.data.price + Ext.Number.randomInt(-2345, 2345) / 100);
        return Math.round(newPrice * 100) / 100;
    }
});
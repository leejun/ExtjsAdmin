Ext.define('Admin.model.grid.summary.SummaryModel', {
    extend: 'Ext.data.Model',

    idProperty: 'taskId',

    fields: [
        {name: 'taskId', type: 'int'},
        {name: 'projectId', type: 'int'},
        {name: 'project', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'estimate', type: 'float'},
        {name: 'rate', type: 'float'},
        {name: 'cost', type: 'float'},
        {name: 'due', type: 'date', dateFormat: 'm/d/Y'}
    ]
});
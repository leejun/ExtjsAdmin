Ext.define('Admin.model.grid.rowediting.RowEditingModel', {
    extend: 'Ext.data.Model',

    fields: [
        {name: 'id', type: 'string'},
        {name: 'name', type: 'string'},
        {name: 'email', type: 'string'},
        {name: 'phone', type: 'string'},
        {name: 'joinDate', type: 'date'},
        {name: 'salary', type: 'float'},
        {name: 'active', type: 'bool'}
    ],

    proxy: {
        type: 'ajax',
        url: ServerUrl + '/ExtjsGridApi/rowEditingUpdate',

        writer: {
            type: 'json',
            allowSingle: true,
            encode: true,
            rootProperty: 'records'
        }
    }
});
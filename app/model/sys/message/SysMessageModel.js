Ext.define('Admin.model.sys.message.SysMessageModel', {
    extend: 'Ext.data.Model',
    idProperty: 'Id_',

    fields: [
        'Id_',
        'SysUserId_',
        'CreateTime_',
        'CreateUser_',
        'SysDicId_',
        'Message_',
        'IsRead_',
        'ReadTime_',
        'SysDicCode_',
        'SysDicName_'
    ]
});

Ext.define('Admin.model.sys.task.SysTaskModel', {
    extend: 'Ext.data.Model',
    idProperty: 'jobName',

    fields: [
        'jobName',
        'jobGroupName',
        'jobDescription',
        'jobClassName',
        'triggerName',
        'triggerGroupName',
        'cronExpression',
        'triggerState',
        'startTime',
        'nextFireTime',
        'previousFireTime'
    ]
});

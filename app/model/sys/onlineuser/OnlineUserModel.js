Ext.define('Admin.model.sys.onlineuser.OnlineUserModel', {
    extend: 'Ext.data.Model',
    idProperty: 'userId',

    fields: [
        'userId',
        'username',
        'loginName',
        'deptName',
        'roles',
        'loginTime',
        'userHost',
        'profile'
    ]
});

Ext.define('Admin.model.sys.dictype.SysDicTypeModel', {
    extend: 'Ext.data.Model',
    identifier: 'longIdGenerator',

    requires: ['Admin.base.LongIdGenerator'],

    fields:
        [
            {name: 'id', mapping: 'id'},
            {name: 'text', mapping: 'text'},
            {name: 'code', mapping: 'code'},
            {name: 'type', mapping: 'type'},
            {name: 'index', mapping: 'index'},
            {name: 'parentId', mapping: 'parentId'},
            {name: 'leaf', mapping: 'leaf'},
            {name: 'expanded', mapping: 'expanded'},
            {name: 'stop', mapping: 'stop'},
            {name: 'children', mapping: 'children'},
            {name: 'expandedEdit', mapping: 'expandedEdit'}
        ]
});
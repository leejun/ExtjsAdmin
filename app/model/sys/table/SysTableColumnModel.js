Ext.define('Admin.model.sys.table.SysTableColumnModel', {
    extend: 'Ext.data.Model',
    idProperty: 'Id_',
    fields: [
        'Id_',
        'SysTableId_',
        'Name_',
        'DataType_',
        'Comment_',
        'Position_',
        'IsInsert_',
        'IsUpdate_',
        'IsList_',
        'IsQuery_',
        'IsRequired_',
        'Xtype_',
        'SysDicTypeCode_',
        'IsQueryAction_',
        'IsDetail_'
    ]
});

Ext.define('Admin.model.sys.table.SysTableModel', {
    extend: 'Ext.data.Model',
    idProperty: 'Id_',

    fields: [
        'Id_',
        'TableName_',
        'TableComment_',
        'CreateTime_',
        'SyncTime_',
        'Package_',
        'LayoutType_',
        'FormLayout_',
        'ModelName_',
        'SysDicTypeId_',
        'SysDicTypeCode_',
        'SelectModel_',
        'IsPagination_',
        'isRowNum_'
    ]
});

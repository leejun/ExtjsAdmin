Ext.define('Admin.model.sys.NavigationTreeModel', {
    extend: 'Ext.data.Model',
    identifier: 'longIdGenerator',

    /*Long类型Id生成器组件*/
    requires: ['Admin.base.LongIdGenerator'],

    fields:
        [
            {name: 'id', mapping: 'id'},
            //树节点显示的内容必须叫text
            {name: 'text', mapping: 'text'},
            //树节点的顺序
            {name: 'index', mapping: 'index'},
            //父节点id
            {name: 'parentId', mapping: 'parentId'},
            //节点左侧图标
            {name: 'iconCls', mapping: 'iconCls'},
            //节点右侧图标
            {name: 'rowCls', mapping: 'rowCls'},
            //是否叶子节点
            {name: 'leaf', mapping: 'leaf'},
            //节点是否展开
            {name: 'expanded', mapping: 'expanded'},
            //子节点的集合
            {name: 'children', mapping: 'children'},
            //鼠标悬停显示的文本
            {name: 'qtip', mapping: 'qtip'},
            //节点前checkbox是否选中
            {name: 'checked', mapping: 'checked'},
            //自定义字段-主视图类型
            {name: 'viewType', mapping: 'viewType'},
            //自定义字段-是否禁用
            {name: 'stop', mapping: 'stop'},
            //自定义字段-是否展开
            {name: 'expandedEdit', mapping: 'expandedEdit'}
        ]
});
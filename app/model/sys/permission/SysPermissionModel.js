Ext.define('Admin.model.sys.permission.SysPermissionModel', {
    extend: 'Ext.data.Model',
    idProperty: 'Id_',

    fields: [
        'Id_',
        'ControllerName_',
        'MethodName_',
        'ActionName_',
        'HttpMethod_',
        'ParametersType_',
        'UrlPattern_',
        'CodeIDelete_',
        'CheckLogin_',
        'CheckPermission_',
        'RequireLog_',
        'LogRequestParam_',
        'LogReturnValue_',
        'LastSyncTime_',
        'RoleBind_'
    ]
});

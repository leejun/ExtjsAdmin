Ext.define('Admin.model.sys.config.SysConfigModel', {
    extend: 'Ext.data.Model',
    idProperty: 'Id_',

    fields: [
        'Id_',
        'ConfigName_',
        'ConfigKey_',
        'ConfigValue_',
        'SystemIn_',
        'Remark_',
        'UpdateTime_'
    ]
});

Ext.define('Admin.model.sys.role.SysRoleModel', {
    extend: 'Ext.data.Model',
    idProperty: 'Id_',

    fields: [
        'Id_',
        'RoleName_',
        'RoleCode_',
        'Index_',
        'Stop_',
        'CreateTime_'
    ]
});

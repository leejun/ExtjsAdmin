Ext.define('Admin.model.sys.dic.SysDicModel', {
    extend: 'Ext.data.Model',
    identifier: 'longIdGenerator',

    requires: ['Admin.base.LongIdGenerator'],

    fields:
        [
            {name: 'id', mapping: 'id'},
            {name: 'text', mapping: 'text'},
            {name: 'code', mapping: 'code'},
            {name: 'sysDicTypeId', mapping: 'sysDicTypeId'},
            {name: 'sysDicTypeCode', mapping: 'sysDicTypeCode'},
            {name: 'index', mapping: 'index'},
            {name: 'parentId', mapping: 'parentId'},
            {name: 'leaf', mapping: 'leaf'},
            {name: 'expanded', mapping: 'expanded'},
            {name: 'stop', mapping: 'stop'},
            {name: 'children', mapping: 'children'},
            {name: 'expandedEdit', mapping: 'expandedEdit'}
        ]
});
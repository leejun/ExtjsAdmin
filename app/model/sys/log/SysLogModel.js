Ext.define('Admin.model.sys.log.SysLogModel', {
    extend: 'Ext.data.Model',
    idProperty: 'Id_',

    fields: [
        'Id_',
        'LogTime_',
        'RequestHost_',
        'RequestUrl_',
        'ActionName_',
        'RequestMethod_',
        'RequestParams_',
        'RunTime_',
        'ReturnValue_',
        'Exception_',
        'ExceptionClass_',
        'UserId_',
        'UserName_'
    ]
});

Ext.define('Admin.model.sys.dept.SysDeptModel', {
    extend: 'Ext.data.Model',
    identifier: 'longIdGenerator',

    requires: ['Admin.base.LongIdGenerator'],

    fields:
        [
            {name: 'id', mapping: 'id'},
            {name: 'text', mapping: 'text'},
            {name: 'index', mapping: 'index'},
            {name: 'parentId', mapping: 'parentId'},
            {name: 'leaf', mapping: 'leaf'},
            {name: 'expanded', mapping: 'expanded'},
            {name: 'children', mapping: 'children'},
            {name: 'deptCode', mapping: 'deptCode'},
            {name: 'stop', mapping: 'stop'},
            {name: 'createTime', mapping: 'createTime'},
            {name: 'expandedEdit', mapping: 'expandedEdit'}
        ]
});
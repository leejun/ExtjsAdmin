Ext.define('Admin.model.sys.generator.SysGeneratorModel', {
    extend: 'Ext.data.Model',
    idProperty: 'Id_',

    fields: [
        'Id_',
        'StringVal1_',
        'StringVal2_',
        'SysDicSexId_',
        'SysDicCityId_',
        'IntVal1_',
        'IntVal2_',
        'DateVal_',
        'BooleanVal_'
    ]
});
Ext.define('Admin.model.sys.user.SysUserModel', {
    extend: 'Ext.data.Model',
    idProperty: 'Id_',

    fields: [
        'Id_',
        'Name_',
        'LoginName_',
        'Password_',
        'Salt_',
        'Email_',
        'Phone_',
        'SysDeptId_',
        'SysDeptName_',
        'SysDeptCode_',
        'CreateTime_',
        'Stop_',
        'PictureUrl_',
        'RoleBind_',
        'Index_'
    ]
});

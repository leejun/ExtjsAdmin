Ext.define('Admin.model.chart.aspose.AsposeCellsModel', {
    extend: 'Ext.data.Model',
    idProperty: 'id',

    fields: [
        'id',
        'name',
        'age',
        'email',
        'address',
        'phone',
        'weight',
        'sex',
        'cardno'
    ]
});

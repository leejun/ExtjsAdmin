Ext.define('Admin.store.chart.aspose.AsposeCellsStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.chart.aspose.AsposeCellsModel',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/AsposeCellsApi/selectList',
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});
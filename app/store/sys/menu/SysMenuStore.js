/*菜单管理模块数据源*/
Ext.define('Admin.store.sys.menu.SysMenuStore', {
    extend: 'Ext.data.TreeStore',
    model: 'Admin.model.sys.NavigationTreeModel',

    autoLoad: true,
    root: {
        expanded: false,
        id: '0',
        text: '',
        leaf: false
    },

    proxy: {
        type: 'ajax',
        api: {
            create: ServerUrl + '/SysMenuController/insertOrUpdateBatch',
            read: ServerUrl + '/SysMenuController/selectMenuTreeList',
            update: ServerUrl + '/SysMenuController/insertOrUpdateBatch',
            destroy: ServerUrl + '/SysMenuController/deleteBatch'
        },
        reader: {
            type: 'json',
            rootProperty: function (data) {
                return data.data || data.children;
            }
        },
        writer: {
            type: 'json',
            allowSingle: false,
            encode: true,
            rootProperty: 'records',
            writeAllFields: true
        }
    },
    autoDestroy: true
});
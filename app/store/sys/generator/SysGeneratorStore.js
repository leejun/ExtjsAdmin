Ext.define('Admin.store.sys.generator.SysGeneratorStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.generator.SysGeneratorModel',

    autoLoad: true,

    proxy: {
        type: 'ajax',
        url: ServerUrl + '/SysGeneratorController/selectPage',
        reader: {
            type: 'json',
            rootProperty: 'data.records',
            totalProperty: 'data.total',
            successProperty: 'success'
        }
    },

    autoDestroy: true
});
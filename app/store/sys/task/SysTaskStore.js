Ext.define('Admin.store.sys.task.SysTaskStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.task.SysTaskModel',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/QuartzController/getAllJobs',
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});
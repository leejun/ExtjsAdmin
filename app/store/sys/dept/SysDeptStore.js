Ext.define('Admin.store.sys.dept.SysDeptStore', {
    extend: 'Ext.data.TreeStore',
    model: 'Admin.model.sys.dept.SysDeptModel',

    autoLoad: true,
    root: {
        expanded: false,
        id: '0',
        text: 'Root',
        leaf: false
    },

    proxy: {
        type: 'ajax',
        api: {
            create: ServerUrl + '/SysDeptController/insertOrUpdateBatch',
            read: ServerUrl + '/SysDeptController/selectTreeList',
            update: ServerUrl + '/SysDeptController/insertOrUpdateBatch',
            destroy: ServerUrl + '/SysDeptController/deleteBatch'
        },
        reader: {
            type: 'json',
            rootProperty: function (data) {
                // Extract child nodes from the items or children property
                // in the dataset
                return data.data || data.children;
            }
        },
        writer: {
            type: 'json',
            allowSingle: false,
            encode: true,
            rootProperty: 'records',
            writeAllFields: true
        }
    },
    autoDestroy: true
});
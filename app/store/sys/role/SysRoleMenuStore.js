/*角色菜单绑定数据源*/
Ext.define('Admin.store.sys.role.SysRoleMenuStore', {
    extend: 'Ext.data.TreeStore',
    model: 'Admin.model.sys.NavigationTreeModel',

    autoLoad: false,
    root: {
        expanded: true,
        id: '0',
        text: '',
        loaded: true,
        leaf: false
    },

    proxy: {
        type: 'ajax',
        api: {
            read: ServerUrl + '/SysMenuController/selectRoleMenuTreeList',
            update: ServerUrl + '/SysMenuController/updateRoleMenuBindInfo'
        },
        reader: {
            type: 'json',
            rootProperty: function (data) {
                return data.data || data.children;
            }
        },
        writer: {
            type: 'json',
            allowSingle: false,
            encode: true,
            rootProperty: 'records',
            writeAllFields: false
        }
    },
    autoDestroy: true
});
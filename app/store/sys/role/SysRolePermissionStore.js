Ext.define('Admin.store.sys.role.SysRolePermissionStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.permission.SysPermissionModel',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        api: {
            read: ServerUrl + '/SysPermissionController/selectPermissionList',
            update: ServerUrl + '/SysPermissionController/updateRolePermission'
        },
        reader: {
            //不分页的返回结构与分页不同
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        },

        writer: {
            type: 'json',
            allowSingle: false,
            encode: true,
            rootProperty: 'records'
        }
    },
    autoDestroy: true
});
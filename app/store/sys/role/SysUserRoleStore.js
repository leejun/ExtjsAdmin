Ext.define('Admin.store.sys.role.SysUserRoleStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.user.SysUserModel',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        api: {
            create: ServerUrl + '/SysUserController/updateRoleUsers',
            read: ServerUrl + '/SysUserController/selectUserListByRoleId',
            update: ServerUrl + '/SysUserController/updateRoleUsers'
        },
        reader: {
            type: 'json',
            rootProperty: 'data.records',
            totalProperty: 'data.total',
            successProperty: 'success'
        },
        writer: {
            type: 'json',
            allowSingle: false,
            encode: true,
            rootProperty: 'records'
        }
    },
    autoDestroy: true
});
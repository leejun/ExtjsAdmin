Ext.define('Admin.store.sys.role.SysRoleStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.role.SysRoleModel',

    autoLoad: false,

    proxy: {
        type: 'ajax',
        url: ServerUrl + '/SysRoleController/selectPage',
        reader: {
            type: 'json',
            rootProperty: 'data.records',
            totalProperty: 'data.total',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});
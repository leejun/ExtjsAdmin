Ext.define('Admin.store.sys.role.SysRoleComboStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.role.SysRoleModel',

    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/SysRoleController/selectAllList',
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});
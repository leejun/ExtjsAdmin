Ext.define('Admin.store.sys.permission.SysPermissionStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.permission.SysPermissionModel',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        api: {
            read: ServerUrl + '/SysPermissionController/selectPage',
            update: ServerUrl + '/SysPermissionController/updateBatch'
        },
        reader: {
            type: 'json',
            rootProperty: 'data.records',
            totalProperty: 'data.total',
            successProperty: 'success'
        },

        writer: {
            type: 'json',
            allowSingle: false,
            encode: true,
            rootProperty: 'records'
        }
    },
    autoDestroy: true
});
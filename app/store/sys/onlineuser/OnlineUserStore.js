Ext.define('Admin.store.sys.onlineuser.OnlineUserStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.onlineuser.OnlineUserModel',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/AppController/getOnlileUserList',
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});
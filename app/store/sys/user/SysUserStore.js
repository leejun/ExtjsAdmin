Ext.define('Admin.store.sys.user.SysUserStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.user.SysUserModel',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/SysUserController/selectPage',
        reader: {
            type: 'json',
            rootProperty: 'data.records',
            totalProperty: 'data.total',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});
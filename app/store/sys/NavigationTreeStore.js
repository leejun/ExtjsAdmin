/*框架左侧菜单树 用户加载当前用户下的菜单列表*/
Ext.define('Admin.store.sys.NavigationTreeStore', {
    extend: 'Ext.data.TreeStore',
    model: 'Admin.model.sys.NavigationTreeModel',

    autoLoad: false,
    root: {
        expanded: false,
        id: '0',
        text: '',
        leaf: false
    },
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/SysMenuController/selectUserMenuTreeList',
        reader: {
            type: 'json',
            rootProperty: function (data) {
                // Extract child nodes from the items or children property
                // in the dataset
                return data.data || data.children;
            }
        }
    },
    autoDestroy: true
});
Ext.define('Admin.store.sys.table.SysTableColumnStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.table.SysTableColumnModel',

    proxy: {
        type : 'ajax',
        api : {
           // create : 'SysPermissionController.do?method=insertBatch',
            read : ServerUrl + '/SysTableColumnController/selectListByTableId',
            update : ServerUrl + '/SysTableColumnController/updateBatch'
            //destroy : 'SysPermissionController.do?method=deleteByPrimaryKey'
        },
        reader : {
            type : 'json',
            rootProperty : 'data',
            successProperty : 'success'
        },
        writer : {
            type : 'json',
            allowSingle : false,
            encode : true,
            rootProperty : 'records'
        }
    },

    autoDestroy: true
});
Ext.define('Admin.store.sys.table.SysTableStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.table.SysTableModel',

    autoLoad: true,
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/SysTableController/selectPage',
        reader: {
            type: 'json',
            rootProperty: 'data.records',
            totalProperty: 'data.total',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});
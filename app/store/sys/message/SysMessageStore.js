Ext.define('Admin.store.sys.message.SysMessageStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.message.SysMessageModel',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/SysMessageController/selectPage',
        reader: {
            type: 'json',
            rootProperty: 'data.records',
            totalProperty: 'data.total',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});
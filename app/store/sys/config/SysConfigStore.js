Ext.define('Admin.store.sys.config.SysConfigStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.config.SysConfigModel',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/SysConfigController/selectPage',
        reader: {
            type: 'json',
            rootProperty: 'data.records',
            totalProperty: 'data.total',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});
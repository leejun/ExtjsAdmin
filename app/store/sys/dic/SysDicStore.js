Ext.define('Admin.store.sys.dic.SysDicStore', {
    extend: 'Ext.data.TreeStore',
    model: 'Admin.model.sys.dic.SysDicModel',

    autoLoad: false,
    root: {
        expanded: false,
        id: '0',
        text: '',
        leaf: false
    },

    proxy: {
        type: 'ajax',
        api: {
            create: ServerUrl + '/SysDicController/insertOrUpdateBatch',
            read: ServerUrl + '/SysDicController/selectTreeList',
            update: ServerUrl + '/SysDicController/insertOrUpdateBatch',
            destroy: ServerUrl + '/SysDicController/deleteBatch'
        },
        reader: {
            type: 'json',
            rootProperty: function (data) {
                // Extract child nodes from the items or children property
                // in the dataset
                return data.data || data.children;
            }
        },
        writer: {
            type: 'json',
            allowSingle: false,
            encode: true,
            rootProperty: 'records',
            writeAllFields: true
        }
    },
    autoDestroy: true
});
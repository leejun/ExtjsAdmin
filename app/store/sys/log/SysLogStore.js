Ext.define('Admin.store.sys.log.SysLogStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.sys.log.SysLogModel',

    autoLoad: false,
    proxy: {
        type: 'ajax',
        url: ServerUrl + '/SysLogController/selectPage',
        reader: {
            type: 'json',
            rootProperty: 'data.records',
            totalProperty: 'data.total',
            successProperty: 'success'
        }
    },
    autoDestroy: true
});
Ext.define('Admin.store.sys.dictype.SysDicTypeStore', {
	extend : 'Ext.data.TreeStore',
	model : 'Admin.model.sys.dictype.SysDicTypeModel',

	autoLoad : true,
	root: {
		expanded: false,
		id: '0',
		text: '',
		leaf: false
	},

	proxy : {
		type : 'ajax',
		api : {
			create : ServerUrl + '/SysDictypeController/insertOrUpdateBatch',
			read : ServerUrl + '/SysDictypeController/selectTreeList',
			update : ServerUrl + '/SysDictypeController/insertOrUpdateBatch',
			destroy : ServerUrl + '/SysDictypeController/deleteBatch'
		},
		reader : {
			type : 'json',
			rootProperty: function(data){
				// Extract child nodes from the items or children property
				// in the dataset
				return data.data || data.children;
			}
		},
		writer : {
			type : 'json',
			allowSingle : false,
			encode : true,
			rootProperty : 'records',
			writeAllFields: true
		}
	},
	autoDestroy : true
});
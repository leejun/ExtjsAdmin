Ext.define('Admin.store.grid.rowediting.RowEditingStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.grid.rowediting.RowEditingModel',

    autoLoad: true,

    proxy: {
        type: 'ajax',
        url: ServerUrl + '/ExtjsGridApi/selectRowEditingList',
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        }
    },

    autoDestroy: true
});
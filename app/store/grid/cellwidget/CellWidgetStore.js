Ext.define('Admin.store.grid.cellwidget.CellWidgetStore', {
    extend: 'Ext.data.Store',
    model: 'Admin.model.grid.cellwidget.CellWidgetModel',

    autoLoad: true,

    proxy: {
        //请求法术
        type: 'ajax',
        api: {
            //读取时请求地址
            read: ServerUrl + '/ExtjsGridApi/cellWidgetDataList',
            //数据修改保存地址
            update: ServerUrl + '/ExtjsGridApi/cellWidgetDataUpdate'
        },
        //解码服务器返回的json
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
        },
        //数据修改后发送到后端的格式,可以在浏览器控制台查看
        writer: {
            type: 'json',
            allowSingle: false,
            encode: true,
            rootProperty: 'records'
        }
    },

    autoDestroy: true
});
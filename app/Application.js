Ext.define('Admin.Application', {
    extend: 'Ext.app.Application',

    //应用名称,可以通过名称获取当前application对象
    //var app = Admin.getApplication();
    name: 'Admin',

    //默认首页面
    defaultToken: 'admindashboard',

    //主视图入口Ext.Viewport
    mainView: 'Admin.view.main.Main',

    //版本更新
    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    },

    //模板方法(主视图mainView创建之前会被调用)
    init: function () {
        console.log("Admin.Application.init() :--> 应用程序初始化");

        //初始化状态管理器(可以在grid中设置stateful:true记住表格状态)
        Ext.state.Manager.setProvider(Ext.state.LocalStorageProvider.create({prefix: 'state-'}));

        // Init the singleton.  Any tag-based quick tips will start working.
        Ext.tip.QuickTipManager.init(true, {
            //显示延迟时间毫秒
            showDelay: 100,
            //关闭自动关闭tip
            dismissDelay: 0
        });

        //初始化消息提示工具
        Ext.onReady(Ext.popup.init, Ext.popup);
    },

    //当页面完全加载时自动调用
    launch: function () {
        console.log("Admin.Application.launch() :--> 应用程序初始化");
        var me = this;

        //浏览器本地缓存
        var localStorage = Ext.util.LocalStorage.get('ext');

        //全局Ajax请求处理
        me.addGlobaAjaxListener();

        var authToken = localStorage.getItem('auth-token');
        if (authToken) {
            console.log("浏览器LocalStorage读取{auto-token:" + authToken + "}成功! --> 开始获取用户基本信息");
            me.getUserInfo();
        } else {
            console.log("浏览器LocalStorage读取{auto-token}失败! --> 跳转到登录页面");
            me.redirectTo(LoginViewXtype);
        }
    },

    //全局Ajax处理
    addGlobaAjaxListener: function () {
        var me = this;

        Ext.Ajax.on('requestcomplete', function (conn, response, options, eOpts) {
            console.log("GlobaAjaxlListener.requestcomplete");
            var result = {};
            try {
                /*解码服务器返回的数据*/
                if (response.responseType && response.responseType === "json") {
                    result = response.responseJson;
                } else {
                    result = Ext.JSON.decode(response.responseText);
                }
            } catch (e) {
                Ext.popup.Msg('Ajax返回数据格式错误', e.msg);
                return;
            }

            /*请求后台接口 当前账户未登录*/
            if (result.code === 'ACCOUNT_UNAUTHENTICATED') {
                console.log('Ajax请求结果处理.当前账户未认证.关闭WebSocket.跳转到登录页.');
                me.getMainView().getController().closeWebsocket();
                me.redirectTo(LoginViewXtype);
                return;
            }

            //服务器返回(success=false并且message!=null)
            if (!result.success && result.message) {
                Ext.popup.Msg('提示信息', result.message);
            }

            //服务器返回(success=true并且message!=null)
            //同时前端代码中设置了successHint=true属性时,则弹出服务器返回的消息.
            if (result.success && result.message) {
                if ((options && options.successHint) || (options.scope && options.scope.successHint)) {
                    Ext.popup.Msg('提示信息', result.message);
                }
            }
        });
    },

    /*获取当前用户信息*/
    getUserInfo: function () {
        var me = this;

        Ext.Ajax.request({
            url: ServerUrl + '/AppController/getUserInfo',
            method: 'GET',
            success: function (response) {
                var responseStr = response.responseText;
                var result = Ext.JSON.decode(responseStr);
                if (result.success) {
                    console.log("获取用户基本信息成功: " + responseStr);
                    me.getMainView().getController().setMainViewData({user: result.data});
                } else {
                    console.log("获取用户基本信息失败: " + responseStr);
                }
            },
            async: false //同步调用
        });
    }
});
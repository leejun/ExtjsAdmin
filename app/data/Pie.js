Ext.define('Admin.data.Pie', {
    extend: 'Admin.data.Simulated',

    data: [
        {
            "xvalue": "Drama",
            "yvalue": 13
        },
        {
            "xvalue": "Fantasy",
            "yvalue": 10
        },
        {
            "xvalue": "Action",
            "yvalue": 12
        }
    ]
});

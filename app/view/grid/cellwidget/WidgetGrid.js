Ext.define('Admin.view.grid.cellwidget.widgetgrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid_cellwidget_widgetgrid',

    requires: [
        'Ext.grid.column.Action',
        'Ext.ProgressBarWidget',
        'Ext.slider.Widget',
        'Ext.sparkline.*'
    ],

    cls: 'user-grid',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Admin.store.grid.cellwidget.CellWidgetStore')
        });
        me.callParent(arguments);
    },

    viewConfig: {
        stripeRows: true,
        enableTextSelection: false,
        markDirty: false
    },

    trackMouseOver: false,
    disableSelection: true,

    columns: [{
        text: 'User',
        dataIndex: 'name',
        width: 120
    }, {
        text: 'Button',
        width: 120,
        xtype: 'widgetcolumn',
        widget: {
            textAlign: 'left',
            bind: '{record.progress}',
            xtype: 'splitbutton',
            handler: 'onButtonWidgetClick'
        }
    }, {
        text: 'Progress',
        xtype: 'widgetcolumn',
        flex: 1,
        width: 120,
        widget: {
            bind: '{record.progress}',
            xtype: 'progressbarwidget',
            textTpl: [
                '{percent:number("0")}% done'
            ]
        }
    }, {
        text: 'Run Mode',
        width: 150,
        flex: 1,
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'combo',
            bind: '{record.mode}',
            store: [
                'Local',
                'Remote'
            ]
        }
    }, {
        text: 'Slider',
        xtype: 'widgetcolumn',
        width: 120,
        flex: 1,
        widget: {
            xtype: 'sliderwidget',
            minValue: 0,
            maxValue: 1,
            bind: '{record.progress}',
            publishOnComplete: false,
            decimalPrecision: 2
        }
    }, {
        text: 'Line',
        width: 100,
        flex: 1,
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'sparklineline',
            bind: '{record.sequence1}',
            tipTpl: 'Value: {y:number("0.00")}'
        }
    }, {
        text: 'Bar',
        width: 100,
        flex: 1,
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'sparklinebar',
            bind: '{record.sequence2}'
        }
    }, {
        text: 'Discrete',
        width: 100,
        flex: 1,
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'sparklinediscrete',
            bind: '{record.sequence3}'
        }
    }, {
        text: 'Bullet',
        width: 100,
        flex: 1,
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'sparklinebullet',
            bind: '{record.sequence4}'
        }
    }, {
        text: 'Pie',
        width: 60,
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'sparklinepie',
            bind: '{record.sequence5}'
        }
    }, {
        text: 'Box',
        width: 100,
        flex: 1,
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'sparklinebox',
            bind: '{record.sequence6}'
        }
    }, {
        text: 'TriState',
        width: 100,
        flex: 1,
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'sparklinetristate',
            bind: '{record.sequence7}'
        }
    }]
});
Ext.define('Admin.view.grid.cellwidget.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.grid_cellwidget_mainviewcontroller',

    //单元格组件单机事件
    onButtonWidgetClick: function (btn) {
        var rec = btn.lookupViewModel().get('record');
        this.lookup('widgetgrid').getStore().sync();
        Ext.Msg.alert("Button clicked", "Hey! " + rec.get('name'));
    },

    //保存单元格
    onSave: function () {
        var me = this;

        var store = this.lookup('widgetgrid').getStore();
        if (store.getModifiedRecords().length <= 0) {
            Ext.popup.Msg('提示信息', '没有修改过的数据');
            return;
        }

        me.getView().setLoading('数据保存中...');
        store.sync({
            success: function () {
                Ext.popup.Msg('提示信息', '保存成功');
            },
            callback: function () {
                me.getView().setLoading(false);
            }
        });
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, true);
        var store = this.lookup('widgetgrid').getStore();
        store.proxy.extraParams = requestParams;
        store.loadPage(1);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchRecord();
    }
});

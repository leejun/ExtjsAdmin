Ext.define('Admin.view.grid.cellwidget.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'grid_cellwidget_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'grid_cellwidget_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'grid_cellwidget_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'grid_cellwidget_widgetgrid',
                reference: 'widgetgrid',
                flex: 1
            }]
        });
        me.callParent(arguments);
    }

});
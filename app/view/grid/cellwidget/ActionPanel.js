Ext.define('Admin.view.grid.cellwidget.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'grid_cellwidget_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [

                    {
                        xtype: 'basesearchform',
                        reference: 'searchForm',
                        items: [
                            {
                                name: 'QueryParam_',
                                margin: '0 0 0 0',
                                fieldLabel: '查询条件'
                            }
                        ]
                    }, {
                        iconCls: 'x-fa fa-times-circle',
                        ui: 'soft-blue',
                        handler: 'clearSearch'
                    }, {
                        iconCls: 'x-fa fa-search',
                        ui: 'soft-blue',
                        handler: 'searchRecord'
                    },
                    {
                        xtype: 'tbfill'
                    }, {
                        xtype: 'savebutton',
                        handler: 'onSave'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
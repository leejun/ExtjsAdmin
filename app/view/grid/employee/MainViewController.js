Ext.define('Admin.view.grid.employee.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.grid_employee_mainviewcontroller',

    //单元格格式化显示
    concatNames: function (v, cellValues, rec) {
        return rec.get('forename') + ' ' + rec.get('surname');
    },

    //重写排序算法
    nameSorter: function (rec1, rec2) {
        // Sort prioritizing surname over forename as would be expected.
        var rec1Name = rec1.get('surname') + rec1.get('forename'),
            rec2Name = rec2.get('surname') + rec2.get('forename');

        if (rec1Name > rec2Name) {
            return 1;
        }

        if (rec1Name < rec2Name) {
            return -1;
        }

        return 0;
    },

    onBeforeRenderNoticeEditor: function (editor) {
        var view = this.getView(),
            store = view.store;

        editor.setStore(store.collect('noticePeriod', false, true));
    },

    // This method is called as a listener to the grid's headermenucreated event.
    // This is a useful way to inject extra options into the grid's header menu.
    // 添加一个表格头部子菜单
    onHeaderMenuCreate: function (grid, menu) {
        menu.insert(menu.items.indexOfKey('columnItem') + 1, {
            text: 'Header Borders',
            xtype: 'menucheckitem',
            checked: grid.headerBorders,
            checkHandler: this.onShowHeadersToggle,
            scope: this
        });
    },

    //文本框输入事件
    onNameFilterKeyup: function () {
        var grid = this.lookup('employeegrid'),
            // Access the field using its "reference" property name.
            filterField = this.lookupReference('nameFilterField'),
            filters = grid.store.getFilters();

        //如果文本框不为空 则添加一个过滤器
        if (filterField.value) {
            this.nameFilter = filters.add({
                id: 'nameFilter',
                property: 'name',
                value: filterField.value,
                anyMatch: true,
                caseSensitive: false
            });
        } else if (this.nameFilter) {
            //如果文本框为空 但是添加过过滤器 则移除过滤器
            filters.remove(this.nameFilter);
            this.nameFilter = null;
        }
    },

    //设置标头是否显示边框
    onShowHeadersToggle: function (checkItem, checked) {
        this.lookup('employeegrid').setHeaderBorders(checked);
    },

    //渲染email
    renderMailto: function (v) {
        return '<a href="mailto:' + encodeURIComponent(v) + '">' +
            Ext.htmlEncode(v) + '</a>';
    }

});

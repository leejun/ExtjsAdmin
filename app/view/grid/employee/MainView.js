Ext.define('Admin.view.grid.employee.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'grid_employee_mainview',

    layout: 'fit',

    controller: 'grid_employee_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'grid_employee_employeegrid',
                reference: 'employeegrid'
            }]
        });
        me.callParent(arguments);
    }

});
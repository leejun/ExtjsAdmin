Ext.define('Admin.view.grid.employee.EmployeeGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid_employee_employeegrid',

    requires: [
        'Ext.grid.filters.Filters',
        'Ext.sparkline.Line',
        'Ext.ux.rating.Picker'
    ],

    cls: 'user-grid',

    store: Ext.create('Admin.store.grid.employee.EmployeeStore'),

    //columnLines: true,
    //multiColumnSort: true,

    plugins: {
        gridfilters: true
    },

    features: [{
        ftype: 'groupingsummary',
        groupHeaderTpl: '{name}',
        hideGroupedHeader: false,
        enableGroupingMenu: false
    }, {
        ftype: 'summary',
        dock: 'bottom'
    }],

    layout: 'border',
    split: true,

    lockedGridConfig: {
        title: 'Employees',
        header: false,
        collapsible: true,
        width: 325,
        minWidth: 290,
        forceFit: true
    },

    selModel: {
        type: 'checkboxmodel',
        checkOnly: true
    },

    viewConfig: {
        stripeRows: true
    },

    listeners: {
        headermenucreate: 'onHeaderMenuCreate'
    },

    columns: [{
        xtype: 'rownumberer',
        width: 40,
        sortable: false,
        locked: true
    }, {
        text: 'Id',
        sortable: true,
        dataIndex: 'employeeNo',
        groupable: false,
        width: 80,
        locked: true
    }, {
        text: 'Name (Filter)',
        dataIndex: 'name',
        sortable: true,
        sorter: {
            //重写排序算法
            sorterFn: 'nameSorter'
        },
        width: 140,
        groupable: false,
        layout: 'hbox',
        locked: true,
        //数据显示格式化
        renderer: 'concatNames',
        items: {
            xtype: 'textfield',
            fieldStyle: "",
            reference: 'nameFilterField',
            flex: 1,
            margin: 2,
            enableKeyEvents: true,
            listeners: {
                //键盘输入事件 延迟500毫秒触发事件
                keyup: 'onNameFilterKeyup',
                buffer: 500
            }
        }
    }, {
        text: 'Rating',
        width: 100,
        sortable: true,
        dataIndex: 'rating',
        groupable: false,
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'sparklineline'
        }
    }, {
        text: 'Date of birth',
        dataIndex: 'dob',
        xtype: 'datecolumn',
        groupable: false,
        width: 115,
        filter: {}
    }, {
        text: 'Join date',
        dataIndex: 'joinDate',
        xtype: 'datecolumn',
        groupable: false,
        width: 120,
        filter: {}
    }, {
        text: 'Notice<br>period',
        dataIndex: 'noticePeriod',
        groupable: false,
        width: 115,
        filter: {
            type: 'list'
        }
    }, {
        text: 'Email address',
        dataIndex: 'email',
        width: 200,
        groupable: false,
        renderer: 'renderMailto'
    }, {
        text: 'Department',
        dataIndex: 'department',
        hidden: true,
        hideable: false,
        filter: {
            type: 'list'
        }
    }, {
        text: 'Absences',
        shrinkWrap: true,
        columns: [{
            text: 'Illness',
            dataIndex: 'sickDays',
            width: 100,
            groupable: false,
            summaryType: 'sum',
            summaryFormatter: 'number("0")',
            filter: {}
        }, {
            text: 'Holidays',
            dataIndex: 'holidayDays',
            width: null, // Size column to title text
            groupable: false,
            summaryType: 'sum',
            summaryFormatter: 'number("0")',
            filter: {}
        }, {
            text: 'Holiday Allowance',
            dataIndex: 'holidayAllowance',
            width: null, // Size column to title text
            groupable: false,
            summaryType: 'sum',
            summaryFormatter: 'number("0.00")',
            formatter: 'number("0.00")',
            filter: {}
        }]
    }, {
        text: 'Rating<br>This Year',
        dataIndex: 'ratingThisYear',
        groupable: false,
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'rating',
            tip: 'Set to {tracking:plural("Star")}'
        }
    }, {
        text: 'Salary',
        width: 155,
        sortable: true,
        dataIndex: 'salary',
        align: 'right',
        formatter: 'usMoney',
        groupable: false,
        summaryType: 'average',
        summaryFormatter: 'usMoney',
        filter: {}
    }]
});
Ext.define('Admin.view.grid.expander.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'grid_expander_mainview',

    layout: 'fit',

    //controller: 'grid_expander_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'grid_expander_expandergrid',
                reference: 'expandergrid'
            }]
        });
        me.callParent(arguments);
    }

});
Ext.define('Admin.view.grid.expander.ExpanderGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid_expander_expandergrid',

    cls: 'user-grid',

    requires: ['Ext.grid.plugin.RowExpander'],

    store: Ext.create('Admin.store.grid.ticker.TickerStore'),

    columns: [{
        text: "Company",
        width: 200,
        dataIndex: 'name'
    }, {
        text: "Industry",
        width: 150,
        dataIndex: 'industry'
    }, {
        text: "Phone",
        width: 150,
        dataIndex: 'phone'
    }, {
        text: "Price",
        formatter: 'usMoney',
        width: 100,
        dataIndex: 'price'
    }, {
        text: "Change",
        width: 100,
        dataIndex: 'priceChange'
    }, {
        text: "% Change",
        width: 100,
        dataIndex: 'priceChangePct'
    }, {
        text: "Last Updated",
        width: 120,
        formatter: 'date("m/d/Y")',
        dataIndex: 'priceLastChange'
    }, {
        text: "Desc",
        width: 120,
        flex: 1,
        dataIndex: 'desc'
    }],

    plugins: {
        rowexpander: {
            rowBodyTpl: new Ext.XTemplate(
                '<p><b>Company:</b> {name}</p>',
                '<p><b>Change:</b> {change:this.formatChange}</p><br>',
                '<p><b>Summary:</b> {desc}</p>',
                {
                    formatChange: function (v) {
                        var color = v >= 0 ? 'green' : 'red';

                        return '<span style="color: ' + color + ';">' +
                            Ext.util.Format.usMoney(v) + '</span>';
                    }
                })
        }
    }
});
Ext.define('Admin.view.grid.summary.SummaryGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid_summary_summarygrid',

    cls: 'user-grid',

    requires: ['Ext.grid.feature.AdvancedGrouping'],

    store: Ext.create('Admin.store.sys.summary.SummaryStore'),

    plugins: {
        cellediting: {
            clicksToEdit: 1
        }
    },

    dockedItems: [{
        dock: 'top',
        xtype: 'toolbar',
        items: [{
            tooltip: 'Toggle the visibility of the summary row',
            text: 'Toggle Summary',
            enableToggle: true,
            pressed: true,
            handler: function () {
                //grid.getView().getFeature('group').toggleSummaryRow();
            }
        }]
    }],
    features: [{
        id: 'group',
        ftype: 'groupingsummary',
        groupHeaderTpl: '{name}',
        hideGroupedHeader: true,
        enableGroupingMenu: false
    }],
    columns: [{
        text: 'Task',
        flex: 1,
        tdCls: 'task',
        sortable: true,
        dataIndex: 'description',
        hideable: false,
        //统计每一组行数
        summaryType: 'count',
        summaryRenderer: function (value, summaryData, dataIndex) {
            return ((value === 0 || value > 1) ? '(' + value + ' Tasks)' : '(1 Task)');
        }
    }, {
        header: 'Project',
        width: 180,
        sortable: true,
        dataIndex: 'project'
    }, {
        header: 'Due Date',
        width: 136,
        sortable: true,
        dataIndex: 'due',
        //统计最大值
        summaryType: 'max',
        renderer: Ext.util.Format.dateRenderer('m/d/Y'),
        summaryRenderer: Ext.util.Format.dateRenderer('m/d/Y'),
        field: {
            xtype: 'datefield'
        }
    }, {
        header: 'Estimate',
        width: 100,
        sortable: true,
        dataIndex: 'estimate',
        //按组统计求和
        summaryType: 'sum',
        renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
            return value + ' hours';
        },
        summaryRenderer: function (value, summaryData, dataIndex) {
            return value + ' hours';
        },
        field: {
            xtype: 'numberfield'
        }
    }, {
        header: 'Rate',
        width: 120,
        sortable: true,
        //显示结果格式化
        renderer: Ext.util.Format.usMoney,
        //统计结果格式化
        summaryRenderer: Ext.util.Format.usMoney,
        dataIndex: 'rate',
        //按组统计平均数
        summaryType: 'average',
        field: {
            xtype: 'numberfield'
        }
    }, {
        header: 'Cost',
        width: 100,
        sortable: false,
        groupable: false,
        //显示数据 为 estimate*rate
        renderer: function (value, metaData, record, rowIdx, colIdx, store, view) {
            return Ext.util.Format.usMoney(record.get('estimate') * record.get('rate'));
        },
        dataIndex: 'cost',
        //自定义统计结果  records为一组数据
        summaryType: function (records, values) {
            var i = 0,
                length = records.length,
                total = 0,
                record;

            for (; i < length; ++i) {
                record = records[i];
                total += record.get('estimate') * record.get('rate');
            }

            return total;
        },
        //统计结果格式化显示
        summaryRenderer: Ext.util.Format.usMoney
    }]

});
Ext.define('Admin.view.grid.summary.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'grid_summary_mainview',

    layout: 'fit',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'grid_summary_summarygrid',
                reference: 'summarygrid'
            }]
        });
        me.callParent(arguments);
    }

});
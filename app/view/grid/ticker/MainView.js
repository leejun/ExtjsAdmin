Ext.define('Admin.view.grid.ticker.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'grid_ticker_mainview',

    layout: 'fit',

    controller: 'grid_ticker_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'grid_ticker_tickergrid',
                reference: 'tickergrid'
            }]
        });
        me.callParent(arguments);
    }

});
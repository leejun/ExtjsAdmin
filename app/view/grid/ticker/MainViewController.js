Ext.define('Admin.view.grid.ticker.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.grid_ticker_mainviewcontroller',

    //Called when the view initializes. This is called after the view's initComponent method has been called.
    init: function(view) {
        this.callParent(arguments);

        var store = this.lookup('tickergrid').getStore();
        //store加载后开始刷新
        if (store.isLoaded() && store.getCount()) {
            this.startTicker(store);
        }

        store.on('load', 'onStoreLoad', this);
    },

    onStoreLoad: function(store) {
        this.startTicker(store);
    },

    startTicker: function(store) {
        var count, i, j, rec;

        if (this.timer) {
            return;
        }

        //store.removeAt(15, 70);

        count = store.getCount();

        for (i = 0; i < count; i++) {
            rec = store.getAt(i);
            rec.beginEdit();

            for (j = 0; j < 10; j++) {
                rec.addPriceTick();
            }

            rec.endEdit(true);
        }

        //这里可以使用websocket 由有台推送数据
        this.timer = Ext.interval(function() {
            //我们根据ID在store.getById(id)中找到rec，然后再修改数据。
            //这里我们使用随机方式修改数据(演示目的)
            rec = store.getAt(Ext.Number.randomInt(0, store.getCount() - 1));
            rec.addPriceTick();
        }, Ext.isIE || !Ext.is.Desktop ? 100 : 20);
    },

    //格式化数据 显示绿色和红色
    renderPositiveNegative: function(val, format) {
        var out = Ext.util.Format.number(val, format),
            s = '<span',
            theme = Ext.theme.name;

        if (theme === "Graphite") {
            s += ' style="color:unset;"';
        }
        else {
            if (val > 0) {
                s += ' style="color:#73b51e;"';
            }
            else if (val < 0) {
                s += ' style="color:#cf4c35;"';
            }
        }

        return s + '>' + out + '</span>';
    },

    renderChange: function(val, metaData) {
        //如果设置了flashBackground=true 则加个改变时在单元格td上添加css 可以自定义颜色
        if (this.lookup('tickergrid').lookupViewModel().get('flashBackground')) {
            metaData.tdCls = [val < 0 ? 'ticker-cell-loss' : val > 0 ? 'ticker-cell-gain' : ''];
        }

        return this.renderPositiveNegative(val, '0.00');
    },

    renderChangePercent: function(val, metaData) {
        return this.renderPositiveNegative(val, '0.00%');
    },

    updaterPositiveNegative: function(cell, value, format) {
        var innerSpan = Ext.fly(cell).down('span', true);

        innerSpan.style.color = value > 0 ? '#73b51e' : '#cf4c35';
        innerSpan.firstChild.data = Ext.util.Format.number(value, format);
    },

    //格式化数字 和 设置颜色
    updateChange: function(cell, value, record, view) {
        this.updaterPositiveNegative(cell, value, '0.00');
    },

    //格式化数据 和 设置颜色
    updateChangePercent: function(cell, value, record, view) {
        this.updaterPositiveNegative(cell, value, '0.00%');
    },

    //销毁时关闭计时器
    destroy: function() {
        Ext.uninterval(this.timer);
    },

    /**
     * @param {Ext.slider.Multi} slider
     * @param {Number/null} newValue
     * @param {Ext.slider.Thumb/null} thumb
     * @param {String} type
     */
    onTickDelayChange: function(slider, newValue, thumb, type) {
        Ext.view.AbstractView.updateDelay = newValue;
    }
});

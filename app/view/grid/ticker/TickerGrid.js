Ext.define('Admin.view.grid.ticker.TickerGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid_ticker_tickergrid',

    requires: [
        'Ext.grid.plugin.CellEditing'
    ],

    cls: 'user-grid',

    title: '后端websocket推送数据，前端表格实时刷新',

    store: Ext.create('Admin.store.grid.ticker.TickerStore'),

    viewModel: {
        data: {
            tickDelay: Ext.view.AbstractView.updateDelay,
            flashBackground: false
        }
    },

    viewConfig: {
        throttledUpdate: true
    },

    columns: [{
        text: '序号',
        width: 60,
        dataIndex: 'id'
    }, {
        text: '公司名称',
        sortable: true,
        width: 120,
        dataIndex: 'name'
    }, {
        text: '实时价格',
        width: 120,
        //原始值为59.47 显示值为$59.47
        formatter: 'usMoney',
        dataIndex: 'price',
        align: 'right',
        producesHTML: false,
        sortable: false
    }, {
        text: '折线图',
        width: 120,
        dataIndex: 'trend',
        xtype: 'widgetcolumn',
        widget: {
            xtype: 'sparklineline',
            tipTpl: 'Price: {y:number("0.00")}'
        },
        sortable: false
    }, {
        text: '价格差',
        width: 90,
        producesHTML: true,
        //首次渲染时格式化value （根据value显示红和绿背景)
        renderer: 'renderChange',
        //数据更新时格式化value （根据value显示红和绿背景)
        updater: 'updateChange',
        dataIndex: 'change',
        align: 'right',
        sortable: false
    }, {
        text: '价格差%',
        width: 100,
        renderer: 'renderChangePercent',
        updater: 'updateChangePercent',
        dataIndex: 'pctChange',
        align: 'right',
        sortable: false
    }, {
        text: '最后改变时间',
        xtype: 'datecolumn',
        format: 'Y-m-d H:i:s',
        dataIndex: 'lastChange',
        width: 160
    }, {
        text: '备注',
        dataIndex: 'desc',
        flex: 1,
        width: 120
    }],
    bbar: {
        docked: 'bottom',
        xtype: 'toolbar',
        defaults: {
            margin: '0 10 0 0'
        },
        items: [{
            fieldLabel: 'Update\u00a0delay',
            xtype: 'sliderfield',
            minValue: 200,
            maxValue: 2000,
            increment: 10,
            labelWidth: 100,
            bind: '{tickDelay}',
            liveUpdate: true,
            listeners: {
                change: 'onTickDelayChange'
            },
            flex: 1
        }, {
            xtype: 'textfield',
            editable: false,
            bind: '{tickDelay}',
            width: 80,
            clearable: false,
            readOnly: true
        }, {
            xtype: 'checkboxfield',
            bind: '{flashBackground}',
            listeners: {
                render: function (c) {
                    c.inputEl.dom.setAttribute('data-qtip', 'Flash background color on change');
                },
                single: true
            }
        }]
    }
});
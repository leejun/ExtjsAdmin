Ext.define('Admin.view.grid.spreadsheet.SpreadsheetGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid_spreadsheet_spreadsheetgrid',

    cls: 'user-grid',

    requires: [
        'Ext.grid.selection.SpreadsheetModel',
        'Ext.grid.plugin.Clipboard',
        'Ext.grid.selection.Replicator'
    ],

    store: Ext.create('Admin.store.grid.spreadsheet.SpreadsheetStore'),

    selModel: {
        type: 'spreadsheet',
        // Disables sorting by header click, though it will be still available via menu
        columnSelect: true,
        checkboxSelect: false,
        pruneRemoved: false,
        extensible: 'y'
    },

    title: '可以CTRL+C/X/V控制表格',

    viewConfig: {
        columnLines: true,
        trackOver: false
    },

    // Enable CTRL+C/X/V hot-keys to copy/cut/paste to the system clipboard.
    plugins: {
        clipboard: true,
        selectionreplicator: true
    },

    columns: [
        {text: 'A', dataIndex: 'year', flex: 1, minWidth: 100},
        {text: 'B', dataIndex: 'jan', width: 100},
        {text: 'C', dataIndex: 'feb', width: 100},
        {text: 'D', dataIndex: 'mar', width: 100},
        {text: 'E', dataIndex: 'apr', width: 100},
        {text: 'F', dataIndex: 'may', width: 100},
        {text: 'G', dataIndex: 'jun', width: 100},
        {text: 'H', dataIndex: 'jul', width: 100},
        {text: 'I', dataIndex: 'aug', width: 100},
        {text: 'J', dataIndex: 'sep', width: 100},
        {text: 'K', dataIndex: 'oct', width: 100},
        {text: 'L', dataIndex: 'nov', width: 100},
        {text: 'M', dataIndex: 'dec', width: 100}
    ],
    forceFit: true
});
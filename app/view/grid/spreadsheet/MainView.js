Ext.define('Admin.view.grid.spreadsheet.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'grid_spreadsheet_mainview',

    layout: 'fit',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'grid_spreadsheet_spreadsheetgrid',
                reference: 'spreadsheetgrid'
            }]
        });
        me.callParent(arguments);
    }

});
Ext.define('Admin.view.grid.rowwidget.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'grid_rowwidget_mainview',

    layout: 'fit',

    //controller: 'sys_log_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'panel'
            }]
        });
        me.callParent(arguments);
    }

});
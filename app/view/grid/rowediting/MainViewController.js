Ext.define('Admin.view.grid.rowediting.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.grid_rowediting_mainviewcontroller',

    listen: {
        component: {
            'grid_rowediting_roweditinggrid[reference=roweditinggrid]': {
                edit: 'onRowEdit'
            }
        }
    },

    onRowEdit: function (editor, e) {
        var me = this;
        me.getView().setLoading('数据保存中...');
        e.record.save({
            success: function (record, operation) {
                Ext.popup.Msg('提示信息', '数据保存成功');
                e.record.commit();
            },
            callback: function (record, operation, success) {
                me.getView().setLoading(false);
            }
        });
    }
});

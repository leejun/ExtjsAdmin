Ext.define('Admin.view.grid.rowediting.RowEditingGrid', {
    extend: 'Ext.grid.Panel',
    xtype: 'grid_rowediting_roweditinggrid',

    cls: 'user-grid',

    requires: ['Ext.grid.plugin.RowEditing'],

    plugins: {
        rowediting: {
            clicksToMoveEditor: 1,
            autoCancel: false,
            saveBtnText: "保存",
            cancelBtnText: "取消"
        }
    },

    title: '双击表格行可编辑数据',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            store: Ext.create('Admin.store.grid.rowediting.RowEditingStore'),

            columns: [{
                header: '姓名(随机字符串)',
                dataIndex: 'name',
                width: 100,
                flex: 1,
                dirtyText: 'Name has been edited',
                editor: {
                    allowBlank: false
                }
            }, {
                header: '电话号码',
                dataIndex: 'phone',
                width: 100,
                dirtyText: 'E-mail was changed',
                flex: 1,
                editor: {
                    allowBlank: false
                }
            }, {
                header: '邮箱地址',
                dataIndex: 'email',
                dirtyText: 'E-mail was changed',
                flex: 1,
                editor: {
                    allowBlank: false,
                    vtype: 'email'
                }
            }, {
                xtype: 'datecolumn',
                header: '开始日期',
                dataIndex: 'joinDate',
                format: 'Y-m-d',
                width: 150,
                editor: {
                    xtype: 'datefield',
                    allowBlank: false,
                    format: 'Y-m-d',
                    //minValue: '2021-12-31',
                    minText: 'Cannot have a start date before the company existed!',
                    maxValue: Ext.Date.format(new Date(), 'Y-m-d')
                }
            }, {
                xtype: 'numbercolumn',
                header: '薪水',
                dataIndex: 'salary',
                format: '$0,0',
                width: 150,
                dirtyText: null,
                editor: {
                    xtype: 'numberfield',
                    allowBlank: false,
                    minValue: 1,
                    maxValue: 150000
                }
            }, {
                xtype: 'checkcolumn',
                header: '是否激活?',
                dataIndex: 'active',
                width: 150,
                editor: {
                    xtype: 'checkbox'
                }
            }]
        });
        me.callParent(arguments);
    }
});
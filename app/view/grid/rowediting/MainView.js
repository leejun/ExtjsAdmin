Ext.define('Admin.view.grid.rowediting.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'grid_rowediting_mainview',

    layout: 'fit',

    controller: 'grid_rowediting_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'grid_rowediting_roweditinggrid',
                reference: 'roweditinggrid'
            }]
        });
        me.callParent(arguments);
    }

});
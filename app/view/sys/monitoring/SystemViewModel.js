Ext.define('Admin.view.sys.monitoring.SystemViewModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.sys_monitoring_systemviewmodel',

    "data": {
        "cpu": {
            "cpuNum": "10",
            "totalFree": "79.99%",
            "sys": "7.9%",
            "used": "11.54%",
            "wait": "0%"
        },
        "cpuPie": [
            {
                "name": "95.23%",
                "data": 95
            },
            {
                "name": "0.75%",
                "data": 0
            },
            {
                "name": "3.83%",
                "data": 3
            }
        ],
        "mem": {
            "total": "15.89GB",
            "used": "12.44GB",
            "usedRate": "78.28%",
            "free": "3.45GB"
        },
        "jvm": {
            "max": "3.97GB",
            "total": "308MB",
            "use": "169.21MB",
            "usedRate": "54.94%",
            "free": "138.79MB",
            "version": "11.0.10",
            "home": "C:\\Java\\jdk-11.0.10",
            "startTime": "2022-02-03 19:14:45",
            "runTime": "0天0小时-3分钟"
        },
        "sys": {
            "computerName": "DESKTOP-UL11U4C",
            "computerIp": "192.168.80.1",
            "userDir": "D:\\IdeaProject\\YinXing",
            "osName": "Windows 10",
            "osArch": "amd64"
        },
        "sysFiles": [
            {
                "dirName": "C:\\",
                "sysTypeName": "NTFS",
                "typeName": "本地固定磁盘 (C:)",
                "total": "140.2 GB",
                "free": "68.9 GB",
                "used": "71.3 GB",
                "usage": 50.88
            },
            {
                "dirName": "D:\\",
                "sysTypeName": "NTFS",
                "typeName": "本地固定磁盘 (D:)",
                "total": "97.7 GB",
                "free": "67.9 GB",
                "used": "29.7 GB",
                "usage": 30.46
            }
        ]
    },

    stores: {
        systemfile: {
            fields:[ 'name', 'email', 'phone'],
            data: '{sysFiles}'
        },

        cpupie: {
            fields: ['name', 'data'],
            data: '{cpuPie}'
        }
    }
});
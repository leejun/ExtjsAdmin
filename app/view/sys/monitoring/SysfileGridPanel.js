Ext.define('Admin.view.sys.monitoring.SysfileGridPanel', {
    extend: 'Ext.grid.Panel',
    xtype: 'sys_monitoring_sysfilegridpanel',


    ui: 'light',
    iconCls: 'x-fa fa-folder',

    title: '磁盘使用情况',

    scrollable: {
        x: false,
        y: false
    },
    headerBorders: false,

    viewConfig: {
        stripeRows: false,
        enableTextSelection: true,
        loadingText: "数据读取中...",
        columnLines: false
    },

    bind: {
        store: '{systemfile}'
    },

    columns: [
        {text: '盘符路径', dataIndex: 'dirName', flex: 1},
        {text: '文件系统', dataIndex: 'sysTypeName', flex: 1},
        {text: '盘符类型', dataIndex: 'typeName', flex: 1},
        {text: '总大小', dataIndex: 'total', flex: 1},
        {text: '使用大小', dataIndex: 'used', flex: 1},
        {text: '使用率', dataIndex: 'usage', flex: 1},
        {text: '剩余大小', dataIndex: 'free', flex: 1}
    ]
});
Ext.define('Admin.view.sys.monitoring.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_monitoring_mainviewcontroller',

    listen: {
        controller: {
            'main_maincontroller': {
                //监听主控制器websocket事件
                onMonitoringMessageUpdate: 'onMonitoringMessageUpdate'
            }
        }
    },

    //websocket推送消息处理
    onMonitoringMessageUpdate: function (data) {
        console.log(data);
        this.getViewModel().setData(data);
    },

    //组件渲染完成后查询数据
    afterRender: function () {
        var me = this;
        me.getView().setLoading('数据加载中...');
        Ext.Ajax.request({
            url: ServerUrl + '/AppController/systemInfo',
            method: 'GET',
            success: function (response, options) {
                var result = Ext.JSON.decode(response.responseText);
                if (result.success) {
                    me.getViewModel().setData(result.data);
                }
            },
            callback: function () {
                me.getView().setLoading(false);
            }
        });
    }
});

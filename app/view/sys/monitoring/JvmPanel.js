Ext.define('Admin.view.sys.monitoring.JvmPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'sys_monitoring_jvmpanel',

    bodyPadding: 20,
    height: 240,

    cls: 'quick-graph-panel shadow',
    ui: 'light',
    iconCls: 'x-fa fa-coffee',

    title: 'JVM基本信息',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: 'JVM最大可申请内存'
                },
                {
                    xtype: 'component',
                    bind: {
                        html: '{jvm.max}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: 'JDK版本号'
                },
                {
                    xtype: 'component',
                    bind: {
                        html: '{jvm.version}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: 'JDK安装路径'
                },
                {
                    xtype: 'component',
                    bind: {
                        html: '{jvm.home}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: 'JVM启动时间'
                },
                {
                    xtype: 'component',
                    bind: {
                        html: '{jvm.startTime}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: 'JVM运行时间'
                },
                {
                    xtype: 'component',
                    bind: {
                        html: '{jvm.runTime}'
                    }
                }
            ]
        }
    ]
});

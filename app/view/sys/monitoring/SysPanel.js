Ext.define('Admin.view.sys.monitoring.SysPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'sys_monitoring_syspanel',

    bodyPadding: 20,
    height: 240,

    cls: 'quick-graph-panel shadow',
    ui: 'light',
    iconCls: 'x-fa fa-server',

    title: '服务器基本信息',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: '计算机名称'
                },
                {
                    xtype: 'component',
                    bind: {
                        html: '{sys.computerName}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: '服务器IP'
                },
                {
                    xtype: 'component',
                    bind: {
                        html: '{sys.computerIp}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: '程序所在位置'
                },
                {
                    xtype: 'component',
                    bind: {
                        html: '{sys.userDir}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: '操作系统'
                },
                {
                    xtype: 'component',
                    bind: {
                        html: '{sys.osName}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: '系统架构'
                },
                {
                    xtype: 'component',
                    bind: {
                        html: '{sys.osArch}'
                    }
                }
            ]
        }
    ]
});

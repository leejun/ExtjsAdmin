Ext.define('Admin.view.sys.monitoring.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_monitoring_mainview',

    scrollable: 'y',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    viewModel: 'sys_monitoring_systemviewmodel',

    controller: 'sys_monitoring_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [{
                    xtype: 'sys_monitoring_cpupanel',
                    flex: 1
                }, {
                    xtype: 'component',
                    width: 10
                }, {
                    xtype: 'sys_monitoring_mempanel',
                    flex: 1
                }]
            }, {
                xtype: 'component',
                height: 10
            }, {
                xtype: 'container',
                layout: {
                    type: 'hbox',
                    align: 'stretch'
                },
                items: [{
                    xtype: 'sys_monitoring_jvmpanel',
                    flex: 1
                }, {
                    xtype: 'component',
                    width: 10
                }, {
                    xtype: 'sys_monitoring_syspanel',
                    flex: 1
                }]
            }, {
                xtype: 'component',
                height: 10
            }, {
                xtype: 'sys_monitoring_sysfilegridpanel'
            }, {
                xtype: 'panel',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
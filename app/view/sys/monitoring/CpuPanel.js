Ext.define('Admin.view.sys.monitoring.CpuPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'sys_monitoring_cpupanel',

    bodyPadding: 20,
    height: 240,

    cls: 'quick-graph-panel shadow',
    ui: 'light',
    iconCls: 'x-fa fa-magnet',

    title: 'CPU使用情况',

    layout: {
        type: 'hbox',
        align: 'stretch'
    },

    items: [
        {
            flex: 3,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            flex: 1,
                            html: '属性名称'
                        },
                        {
                            xtype: 'component',
                            html: '属性值'
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            flex: 1,
                            html: 'CPU核心数'
                        },
                        {
                            xtype: 'component',
                            bind: {
                                html: '{cpu.cpuNum}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            flex: 1,
                            html: '系统使用率'
                        },
                        {
                            xtype: 'component',
                            bind: {
                                html: '{cpu.sys}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            flex: 1,
                            html: '用户使用率'
                        },
                        {
                            xtype: 'component',
                            bind: {
                                html: '{cpu.used}'
                            }
                        }
                    ]
                },
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        align: 'stretch'
                    },
                    items: [
                        {
                            xtype: 'component',
                            flex: 1,
                            html: '当前空闲率'
                        },
                        {
                            xtype: 'component',
                            bind: {
                                html: '{cpu.totalFree}'
                            }
                        }
                    ]
                }
            ]
        },
        {
            flex: 2,
            xtype: 'polar',
            background: 'rgba(255, 255, 255, 1)',
            colors: [
                '#6aa5dc',
                '#fdbf00',
                '#ee929d'
            ],
            bind: {
                store: '{cpupie}'
            },
            series: [
                {
                    type: 'pie',
                    angleField: 'data',
                    showInLegend: true,
                    xField: 'data',
                    label: {
                        field: 'name',
                        display: 'inside',
                        contrast: true,
                        font: '12px Arial'
                    },
                    donut: 50
                }
            ]
        }
    ]
});

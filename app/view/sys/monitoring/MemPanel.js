Ext.define('Admin.view.sys.monitoring.MemPanel', {
    extend: 'Ext.panel.Panel',
    xtype: 'sys_monitoring_mempanel',

    bodyPadding: 20,
    height: 240,

    cls: 'quick-graph-panel shadow',
    ui: 'light',
    iconCls: 'x-fa fa-database',

    title: '内存使用情况',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    items: [
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: '属性名称'
                },
                {
                    xtype: 'component',
                    cls: 'sys-monitoring-mainview-html-right',
                    flex: 1,
                    html: '操作系统'
                },
                {
                    xtype: 'component',
                    cls: 'sys-monitoring-mainview-html-right',
                    flex: 1,
                    html: 'JVM虚拟机'
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: '总内存'
                },
                {
                    xtype: 'component',
                    cls: 'sys-monitoring-mainview-html-right',
                    flex: 1,
                    bind: {
                        html: '{mem.total}'
                    }
                },
                {
                    xtype: 'component',
                    cls: 'sys-monitoring-mainview-html-right',
                    flex: 1,
                    bind: {
                        html: '{jvm.total}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: '已用内存'
                },
                {
                    xtype: 'component',
                    flex: 1,
                    cls: 'sys-monitoring-mainview-html-right',
                    bind: {
                        html: '{mem.used}'
                    }
                },
                {
                    xtype: 'component',
                    cls: 'sys-monitoring-mainview-html-right',
                    flex: 1,
                    bind: {
                        html: '{jvm.use}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: '剩余内存'
                },
                {
                    xtype: 'component',
                    cls: 'sys-monitoring-mainview-html-right',
                    flex: 1,
                    bind: {
                        html: '{mem.free}'
                    }
                },
                {
                    xtype: 'component',
                    cls: 'sys-monitoring-mainview-html-right',
                    flex: 1,
                    bind: {
                        html: '{jvm.free}'
                    }
                }
            ]
        },
        {
            xtype: 'container',
            flex: 1,
            layout: {
                type: 'hbox',
                align: 'stretch'
            },
            items: [
                {
                    xtype: 'component',
                    flex: 1,
                    html: '使用率'
                },
                {
                    xtype: 'component',
                    cls: 'sys-monitoring-mainview-html-right',
                    flex: 1,
                    bind: {
                        html: '{mem.usedRate}'
                    }
                },
                {
                    xtype: 'component',
                    cls: 'sys-monitoring-mainview-html-right',
                    flex: 1,
                    bind: {
                        html: '{jvm.usedRate}'
                    }
                }
            ]
        }
    ]
});

Ext.define('Admin.view.sys.onlineuser.GridList', {
    extend: 'Admin.base.BaseGridPanel',
    xtype: 'sys_onlineuser_gridlist',

    pagination: false,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Admin.store.sys.onlineuser.OnlineUserStore', {
                autoLoad: true
            }),
            columns: [
                {text: 'PK', dataIndex: 'userId', hidden: true},
                {
                    xtype: 'gridcolumn',
                    renderer: Admin.base.BaseUtils.rendererProfile,
                    width: 75,
                    dataIndex: 'profile',
                    text: '头像'
                },
                {text: '登录时间', dataIndex: 'loginTime', minWidth: 150},
                {text: '用户姓名', dataIndex: 'username', width: 120},
                {text: '登录名', dataIndex: 'loginName', width: 140},
                {text:'所属部门', dataIndex:'deptName', width: 140},
                {text: '客户端地址', dataIndex: 'userHost', width: 140},
                {text: '角色编码', dataIndex: 'roles', minWidth: 150, flex: 1}
            ]
        });
        me.callParent(arguments);
    }
});
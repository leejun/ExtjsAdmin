Ext.define('Admin.view.sys.onlineuser.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_onlineuser_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_onlineuser_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_onlineuser_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'sys_onlineuser_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
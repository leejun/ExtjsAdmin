Ext.define('Admin.view.sys.onlineuser.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_onlineuser_mainviewcontroller',

    listen: {
        component: {
            'basesearchform[reference=searchForm] > textfield': {
                //antionPanel查询条件控件键盘事件
                specialkey: 'specialkeyHandler'
            }
        }
    },

    /*高级查询条件*/
    searchPlusParams: {},

    /*actionpanel-强退事件*/
    onDel: function () {
        var me = this;
        if (!me.lookup('gridlist').isSelected()) {
            Ext.popup.Msg('提示信息', '请选择一行记录!');
            return;
        }
        var userId = me.lookup('gridlist').getSelectedRecordId();

        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            recordId: userId,
            fn: me.deleteRequest,
            scope: me
        });
    },
    deleteRequest: function (buttonId, text, opt) {
        if (buttonId === "ok") {
            var me = this;
            Ext.Ajax.request({
                url: ServerUrl + '/AppController/setUserOffline',
                method: 'POST',
                successHint: true,
                maskContainer: me.getView(),
                params: {userId: opt.recordId},
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function (field, e) {
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, true);
        Ext.apply(requestParams, this.searchPlusParams);
        this.lookup('gridlist').search(requestParams);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchPlusParams = {};
        this.searchRecord();
    }
});

Ext.define('Admin.view.sys.onlineuser.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'sys_onlineuser_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [

                    {
                        xtype: 'basesearchform',
                        reference: 'searchForm',
                        items: [
                            {
                                xtype: 'textfield',
                                name: 'QueryParam_',
                                emptyText: '用户姓名|登录名称',
                                margin: '0 0 0 0',
                                fieldLabel: '查询条件'
                            }
                        ]
                    }, {
                        iconCls: 'x-fa fa-times-circle',
                        ui: 'soft-blue',
                        handler: 'clearSearch'
                    }, {
                        iconCls: 'x-fa fa-search',
                        ui: 'soft-blue',
                        handler: 'searchRecord'
                    },
                    {
                        xtype: 'tbfill'
                    }, {
                        xtype: 'delbutton',
                        text: '强退',
                        iconCls: 'x-fa fa-power-off',
                        handler: 'onDel'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
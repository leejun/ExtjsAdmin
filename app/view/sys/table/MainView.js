Ext.define('Admin.view.sys.table.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_table_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_table_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_table_actionpanel'
            }, {
                xtype: 'sys_table_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
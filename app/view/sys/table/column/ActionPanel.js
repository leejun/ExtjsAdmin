Ext.define('Admin.view.sys.table.column.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'sys_table_column_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'tbfill'
                    }, {
                        xtype: 'refreshbutton',
                        tooltip: '表结构刷新',
                        handler: 'onRefresh'
                    }, '-', {
                        xtype: 'savebutton',
                        handler: 'onSave'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
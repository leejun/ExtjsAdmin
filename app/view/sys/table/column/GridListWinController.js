Ext.define('Admin.view.sys.table.column.GridListWinController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_table_column_gridListwincontroller',

    //主表ID
    tableId: 0,

    //保存修改数据
    onSave: function () {
        var me = this;
        if (me.getGridList().getStore().getModifiedRecords().length <= 0) {
            Ext.popup.Msg('提示信息', '没有修改过的数据');
            return;
        }
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.doSaveRecord,
            scope: this
        });
    },
    doSaveRecord: function (buttonId) {
        if (buttonId === "ok") {
            var me = this;
            me.getView().setLoading('数据保存中...');
            me.getGridList().getStore().sync({
                success: function () {
                    Ext.popup.Msg('提示信息', '保存成功');
                    //me.getGridList().refresh();
                },
                callback: function () {
                    me.getView().setLoading(false);
                }
            });
        }
    },

    /*与数据库同步*/
    onRefresh: function () {
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.doRefresh,
            scope: this
        });
    },
    doRefresh: function (buttonId) {
        var me = this;
        if (buttonId === "ok") {
            Ext.Ajax.request({
                url: ServerUrl + '/SysTableColumnController/syncColumnList',
                method: 'get',
                maskContainer: me.getView(),
                params: {'tableId': me.tableId},
                success: function () {
                    Ext.popup.Msg('提示信息', '表结构刷新成功');
                    me.getGridList().refresh();
                }
            });
        }
    },

    //加载子表数据
    loadData: function (tableId) {
        var me = this;
        me.tableId = tableId;
        me.getGridList().search({'tableId': tableId});
    },

    //获取表格
    getGridList: function () {
        return this.lookup('gridlist');
    },

    //关闭窗口
    onCancel: function () {
        this.getView().close();
    }
});

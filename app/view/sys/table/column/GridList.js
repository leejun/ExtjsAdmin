Ext.define('Admin.view.sys.table.column.GridList', {
    extend : 'Admin.base.BaseGridPanel',
    xtype : 'sys_table_column_gridlist',

    pagination: false,

    initComponent : function() {
        var me = this;

        me.plugins = [ me.cellEditingPlugin = Ext.create('Ext.grid.plugin.CellEditing') ];

        Ext.applyIf(me, {
            store : Ext.create('Admin.store.sys.table.SysTableColumnStore', {
                autoLoad : false
            }),
            columns : [
                {text:'Id_',dataIndex:'Id_', hidden:true, hideable: false},
                {text:'SysTableId_',dataIndex:'SysTableId_', hidden:true, hideable: false},
                {text:'序号',dataIndex:'Position_', width:50},
                {text:'字段名称',dataIndex:'Name_', minWidth:100, flex : 1},
                {text:'类型',dataIndex:'DataType_', width:120},
                {text:'说明',dataIndex:'Comment_', editor:'textfield', width:140},
                {text:'插入',dataIndex:'IsInsert_', xtype: 'checkcolumn',  width:60},
                {text:'更新',dataIndex:'IsUpdate_', xtype: 'checkcolumn',  width:60},
                {text:'详细',dataIndex:'IsDetail_', xtype: 'checkcolumn',  width:60},
                {text:'列表',dataIndex:'IsList_', xtype: 'checkcolumn',  width:60},
                {text:'弹窗查询',dataIndex:'IsQuery_', xtype: 'checkcolumn',  width:80},
                {text:'页面查询',dataIndex:'IsQueryAction_', xtype: 'checkcolumn',  width:80},
                {text:'必填',dataIndex:'IsRequired_', xtype: 'checkcolumn', width:60},
                {text:'类型',dataIndex:'Xtype_', width:150,
                    editor: {
                        xtype: 'combobox',
                        queryMode: 'local',
                        displayField: 'name',
                        valueField: 'value',
                        store: [
                            { value: 'textfield', name: 'textfield'},
                            { value: 'hiddenfield', name: 'hiddenfield'},
                            { value: 'datefield', name: 'datefield'},
                            { value: 'numberfield', name: 'numberfield'},
                            { value: 'textareafield', name: 'textareafield'},
                            { value: 'passwordfield', name: 'passwordfield'},
                            { value: 'combobox', name: 'combobox'},
                            { value: 'yesnocombobox', name: 'yesnocombobox'},
                            { value: 'sys_dic_sysdiccombobox', name: 'sysdiccombobox'},
                            { value: 'sys_dic_sysdictreepicker', name: 'sysdictreepicker'}
                        ]
                    }
                },
                {text:'字典类型',dataIndex:'SysDicTypeCode_', editor:'sys_dictype_sysdictypecombobox', width:150}
            ]
        });
        me.callParent(arguments);
    }
});
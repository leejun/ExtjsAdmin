Ext.define('Admin.view.sys.table.column.GridListWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_table_column_gridlistwin',

    layout : {
        align : 'stretch',
        type : 'vbox'
    },

    width : 1200,
    height: 500,

    controller: 'sys_table_column_gridListwincontroller',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_table_column_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'sys_table_column_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });
        me.callParent(arguments);
    }

});
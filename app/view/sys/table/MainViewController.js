Ext.define('Admin.view.sys.table.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_table_mainviewcontroller',

    listen: {
        component: {
            'basesearchform[reference=searchForm] > textfield': {
                //actionpanel中控件回车事件
                specialkey: 'specialkeyHandler'
            }
        },

        controller: {
            'sys_table_formwindowcontroller': {
                //监听控制器事件，用于刷新表格.
                recordAdded: 'onRecordAdded'
            }
        }
    },

    /*编辑数据*/
    onEdit: function (grid, rowIndex) {
        var me = this;
        var record = grid.getStore().getAt(rowIndex);

        var win = Ext.create({
            xtype: 'sys_table_formwindow'
        });

        me.getView().add(win).show();
        var form = win.lookup("form");
        form.load({
            url: ServerUrl + '/SysTableController/selectById',
            method: 'get',
            params: {id: record.getId()}
        });
    },

    /*actionpanel-删除事件*/
    onDel: function () {
        var me = this;
        if (!me.lookup('gridlist').isSelected()) {
            Ext.popup.Msg('提示信息', '请选择一行记录!');
            return;
        }

        //获取选中行
        var record = me.lookup('gridlist').getSelectedRecord();

        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            recordId: record.getId(),
            fn: me.deleteRequest,
            scope: me
        });
    },
    deleteRequest: function (buttonId, text, opt) {
        if (buttonId === "ok") {
            var me = this;
            Ext.Ajax.request({
                url: ServerUrl + '/SysTableController/deleteById',
                //请求方式
                method: 'POST',
                //操作成功提示
                successHint: true,
                //网络请求时遮罩当前视图
                maskContainer: me.getView(),
                //请求参数
                params: {id: opt.recordId},
                //成功回调
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*字段列表*/
    onColumn: function (view, rowIndex, colIndex, item, e, record) {
        var win = Ext.create({
            xtype: 'sys_table_column_gridlistwin',
            title: '数据库表[' + record.get('TableName_') + ']中的全部字段'
        });
        win.show();
        win.getController().loadData(record.getId());
    },

    /*代码生成*/
    onGen: function (view, rowIndex, colIndex, item, e, record) {
        window.location.href = ServerUrl + '/SysTableController/download?tableId=' + record.getId();
    },

    /*刷新表格*/
    onRecordAdded: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function (field, e) {
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*条件查询*/
    searchRecord: function () {
        var params = this.lookup('searchForm').getValues(false, true);
        this.lookup('gridlist').search(params);
    },

    /*清除条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchRecord();
    }
});

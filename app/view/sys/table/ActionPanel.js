Ext.define('Admin.view.sys.table.ActionPanel', {
	extend : 'Ext.container.Container',
	xtype : 'sys_table_actionpanel',

	margin: '0 0 5 0',

	initComponent : function() {
		var me = this;

		Ext.applyIf(me, {
			items: [{
	    		xtype: 'toolbar',
				items: [{
					xtype : 'basesearchform',
					reference : 'searchForm',
					items : [{
						xtype: 'textfield',
						name: 'QueryParam_',
						emptyText: '表名|ExtView|Controller',
						margin: '0 0 0 0',
						fieldLabel: '查询条件'
					}]
				}, {
					iconCls : 'x-fa fa-times-circle',
					ui : 'soft-blue',
					handler : 'clearSearch'
				}, {
					iconCls : 'x-fa fa-search',
					ui : 'soft-blue',
					handler : 'searchRecord'
				}, {
					xtype: 'tbfill'
				},{
					xtype: 'delbutton',
					handler: 'onDel'
				}]
		    }]
		});

		me.callParent(arguments);
	}
});
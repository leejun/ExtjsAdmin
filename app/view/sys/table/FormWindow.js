Ext.define('Admin.view.sys.table.FormWindow', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_table_formwindow',

    controller: 'sys_table_formwindowcontroller',

    height: 460,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'form',

                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'Id_'
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            name: 'TableName_',
                            fieldLabel: '表名称',
                            readOnly: true
                        }, {
                            name: 'TableComment_',
                            fieldLabel: '表说明'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            name: 'Package_',
                            fieldLabel: 'ExtView包名'
                        }, {
                            name: 'ModelName_',
                            fieldLabel: 'Controller前缀'
                        }]
                    },
                    {
                        xtype: 'combobox',
                        name: 'SelectModel_',
                        fieldLabel: 'Grid选择',
                        queryMode: 'local',
                        displayField: 'name',
                        valueField: 'value',
                        store: [
                            {value: 'single', name: '单选'},
                            {value: 'multi', name: '多选'},
                            {value: 'none', name: '不选'}
                        ]
                    },
                    {
                        xtype: 'yesnocombobox',
                        name: 'IsPagination_',
                        fieldLabel: 'Grid分页',
                        yesText: '分页',
                        noText: '不分页'
                    },
                    {
                        xtype: 'yesnocombobox',
                        name: 'isRowNum_',
                        fieldLabel: 'Grid序号',
                        yesText: '显示',
                        noText: '不显示'
                    },
                    {
                        name: 'LayoutType_',
                        fieldLabel: '布局类型',
                        xtype: 'combobox',
                        queryMode: 'local',
                        displayField: 'name',
                        valueField: 'value',
                        store: [
                            {value: 'grid', name: '单表增删改'},
                            {value: 'treegrid', name: '左树增删改'}
                        ]
                    },
                    {
                        xtype: 'sys_dictype_sysdictypetreepicker',
                        name: 'SysDicTypeId_',
                        fieldLabel: '字典类型',
                        allowBlank: true
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        text: '保存',
        iconCls: 'x-fa fa-save',
        ui: 'soft-green',
        handler: 'onSave'
    }, {
        text: '取消',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
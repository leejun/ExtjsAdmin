Ext.define('Admin.view.sys.table.GridList', {
    extend : 'Admin.base.BaseGridPanel',
    xtype : 'sys_table_gridlist',

    //记住表格状态 用户可以手动调整列的顺序|快递
    //再次刷新浏览器用记住上次用户调整的状态
    stateful: true,

    /*单选*/
    singleSelect: true,

    initComponent : function() {
        var me = this;
        Ext.applyIf(me, {
            store : Ext.create('Admin.store.sys.table.SysTableStore'),
            columns : [
                {text:'id',dataIndex:'id', hidden:true},
                {text:'表名称',dataIndex:'TableName_', minWidth:130, flex : 1},
                {text:'表说明',dataIndex:'TableComment_', width:130},
                {text:'同步时间',dataIndex:'SyncTime_', width:150},
                {text:'ExtView包名',dataIndex:'Package_', minWidth:140, flex : 1},
                {text:'Controller前缀',dataIndex:'ModelName_', minWidth:140, flex : 1,
                    renderer: function(value, metaData) {
                        metaData.tdAttr = 'data-qtip="请求地址前缀，例如填写：SysUser 则ext请求路径为:/SysUserController/后台增删改查方法"';
                        return value;
                    }
                },
                {text:'Grid选择', dataIndex:'SelectModel_', width:90,
                    renderer: function (value) {
                        var v = value;
                        if(value === 'multi') {v="多选";}
                        if(value === 'single') {v="单选";}
                        if(value === 'none') {v="不选";}
                        return v;
                    }
                },
                {text:'Grid分页', dataIndex:'IsPagination_', width:80,
                    renderer: function (value) {
                        var v = value;
                        if(value === true) {v="分页";}
                        if(value === false) {v="不分";}
                        return v;
                    }
                },
                {text:'Grid序号', dataIndex:'isRowNum_', width:80,
                    renderer: function (value) {
                        var v = value;
                        if(value === true) {v="显示";}
                        if(value === false) {v="不显示";}
                        return v;
                    }},
                {text:'布局类型',dataIndex:'LayoutType_', width:80,
                    renderer: function (value) {
                        if(value === 'grid') {return '单表';}
                        if(value === 'treegrid') {return '左树';}
                    }
                },
                //{text:'字典类型编码',dataIndex:'SysDicTypeCode_', minWidth: 100, flex : 1},
                {
                    xtype: 'actioncolumn',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-pencil-alt',
                            tooltip: '编辑',
                            handler: 'onEdit'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-table',
                            tooltip: '字段',
                            handler: 'onColumn'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-file',
                            tooltip: '生成',
                            handler: 'onGen'
                        }
                    ],
                    dataIndex: 'bool',
                    text: '操作'
                }
            ]
        });
        me.callParent(arguments);
    }
});
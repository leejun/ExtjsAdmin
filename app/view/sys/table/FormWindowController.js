Ext.define('Admin.view.sys.table.FormWindowController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_table_formwindowcontroller',

    onSave: function(){
        Ext.Msg.show({
            title:'提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.doSaveRecord,
            scope: this
        });
    },

    onCancel: function(){
        this.getView().close();
    },

    doSaveRecord: function(buttonId) {
        if(buttonId === "ok") {
            var me = this;
            var form = this.lookup("form");
            form.submit({
                url: ServerUrl + '/SysTableController/insertOrUpdate',
                success: function() {
                    me.fireEvent('recordAdded');
                    me.getView().close();
                }
            });
        }
    }
});

Ext.define('Admin.view.sys.log.GridList', {
    extend: 'Admin.base.BaseGridPanel',
    xtype: 'sys_log_gridlist',

    multiSelect: true,
    pagination: true,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Admin.store.sys.log.SysLogStore', {
                autoLoad: true
            }),
            columns: [
                {text: 'PK', dataIndex: 'Id_', hidden: true},
                {text: '记录时间', dataIndex: 'LogTime_', width: 150},
                {text: '请求路径', dataIndex: 'RequestUrl_', minWidth: 150, flex: 2},
                {text: '操作名称', dataIndex: 'ActionName_', minWidth: 120, flex: 1},
                {text:'来源IP', dataIndex:'RequestHost_', width: 120},
                {text: 'Http方式', dataIndex: 'RequestMethod_', width: 80},
                {text: '请求参数JSON', dataIndex: 'RequestParams_', minWidth: 150, flex: 1},
                {
                    text: '执行时间', dataIndex: 'RunTime_', width: 80,
                    renderer: function (value) {
                        return Ext.util.Format.number(value / 1000, '0.00') + '秒';
                    }
                },
                //{text:'返回结果JSON', dataIndex:'ReturnValue_', width:100},
                {text: '是否异常', dataIndex: 'Exception_', align:'center', width: 80, renderer: Admin.base.BaseUtils.rendererBoolean},
                {text: '用户姓名', dataIndex: 'UserName_', width: 100},
                {
                    xtype: 'actioncolumn',
                    width: 80,
                    dataIndex: 'bool',
                    text: '操作',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-times',
                            tooltip: '删除',
                            handler: 'onDelete'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-file',
                            tooltip: '详细',
                            handler: 'onShow'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});
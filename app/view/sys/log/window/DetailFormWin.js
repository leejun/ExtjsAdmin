Ext.define('Admin.view.sys.log.DetailFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_log_detailformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_log_detailformwincontroller',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name: 'LogTime_',
                            readOnly: true,
                            fieldLabel: '记录时间'
                        }, {
                            xtype: 'textfield',
                            name: 'RequestHost_',
                            readOnly: true,
                            fieldLabel: '请求IP'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name: 'ActionName_',
                            readOnly: true,
                            fieldLabel: '操作名称'
                        }, {
                            xtype: 'textfield',
                            name: 'RequestMethod_',
                            readOnly: true,
                            fieldLabel: 'Http方式'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name: 'ExceptionStr_',
                            readOnly: true,
                            fieldLabel: '是否异常'
                        }, {
                            xtype: 'textfield',
                            name: 'ExceptionClass_',
                            readOnly: true,
                            fieldLabel: '异常类型'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name: 'UserId_',
                            readOnly: true,
                            fieldLabel: '用户ID'
                        }, {
                            xtype: 'textfield',
                            name: 'UserName_',
                            readOnly: true,
                            fieldLabel: '用户姓名'
                        }]
                    },
                    {
                        xtype: 'textfield',
                        name: 'RequestUrl_',
                        readOnly: true,
                        fieldLabel: '请求路径'
                    },
                    {
                        xtype: 'textfield',
                        name: 'RunTimeStr_',
                        readOnly: true,
                        fieldLabel: '执行时间'
                    },
                    {
                        xtype: 'textfield',
                        name: 'RequestParams_',
                        readOnly: true,
                        fieldLabel: '请求参数JSON'
                    },
                    {
                        xtype: 'textareafield',
                        name: 'ReturnValue_',
                        readOnly: true,
                        flex: 1,
                        fieldLabel: '返回结果JSON'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        text: '关闭',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
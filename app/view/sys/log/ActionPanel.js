Ext.define('Admin.view.sys.log.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'sys_log_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [

                    {
                        xtype: 'basesearchform',
                        reference: 'searchForm',
                        items: [
                            {
                                xtype: 'textfield',
                                name: 'QueryParam_',
                                emptyText: '请求路径|用户姓名或ID',
                                fieldLabel: '查询条件'
                            },
                            {
                                xtype: 'datefield',
                                name: 'LogTimeBegin_',
                                emptyText: '记录时间:起始'
                            },
                            {
                                xtype: 'datefield',
                                name: 'LogTimeEnd_',
                                margin: '0 0 0 0',
                                emptyText: '记录时间:结束'
                            }
                        ]
                    }, {
                        iconCls: 'x-fa fa-times-circle',
                        ui: 'soft-blue',
                        handler: 'clearSearch'
                    }, {
                        iconCls: 'x-fa fa-search',
                        ui: 'soft-blue',
                        handler: 'searchRecord'
                    },
                    {
                        xtype: 'tbfill'
                    }, {
                        xtype: 'delbutton',
                        handler: 'onDel'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
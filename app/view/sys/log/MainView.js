Ext.define('Admin.view.sys.log.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_log_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_log_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_log_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'sys_log_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
Ext.define('Admin.view.sys.dept.SysDeptTree', {
    extend: 'Admin.base.BaseTreePanel',
    xtype: 'sys_dept_sysdepttree',

    //是否隐藏roo节点  false:隐藏  true:显示
    rootVisible: false,

    tools:[{
        type:'refresh',
        tooltip: '刷新',
        handler: function(event, toolEl, panelHeader) {
            this.up('sys_dept_sysdepttree').refreshView();
        }
    }],

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            viewConfig: {
                loadingText: "数据读取中..."
            },
            store: Ext.create('Ext.data.TreeStore', {
                model : 'Admin.model.sys.dept.SysDeptModel',
                autoLoad : true,
                root: {
                    expanded: false,
                    id: '0',
                    text: '组织机构',
                    leaf: false
                },
                proxy: {
                    type: 'ajax',
                    url: ServerUrl + '/SysDeptController/selectTreeListNoStop',
                    reader: {
                        type : 'json',
                        rootProperty: function(data) {
                            return data.data || data.children;
                        },
                        successProperty: 'success'
                    }
                }
            })
        });
        me.callParent(arguments);
    }
});
Ext.define('Admin.view.sys.dept.SysDeptTreePicker', {
    extend: 'Ext.ux.TreePicker',
    alias: 'widget.sys_dept_sysdepttreepicker',

    selectChildren: true,
    rootVisible: true,
    displayField: 'text',
    valueField: 'id',

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            viewConfig: {
                loadingText: "数据读取中..."
            },
            store: Ext.create('Ext.data.TreeStore', {
                model: 'Admin.model.sys.dept.SysDeptModel',
                autoLoad: true,
                root: {
                    expanded: false,
                    id: '0',
                    text: '组织机构',
                    leaf: false
                },
                proxy: {
                    type: 'ajax',
                    url: ServerUrl + '/SysDeptController/selectTreeListNoStop',
                    reader: {
                        type: 'json',
                        rootProperty: function (data) {
                            return data.data || data.children;
                        },
                        successProperty: 'success'
                    }
                }
            }),
            columns: [{
                xtype: 'treecolumn',
                sortable: false,
                text: '机构名称',
                dataIndex: 'text',
                flex: 1,
                minWidth: 200
            }, {
                sortable: false,
                text: '机构编码',
                dataIndex: 'deptCode',
                width: 200
            }]
        });
        me.callParent(arguments);
    },

    setValue: function (value) {
        var me = this;
        // if(value) {
        //     var node = me.store.getNodeById(value);
        //     if(!node.isLeaf()) {
        //         Ext.popup.Msg("提示信息", "不允许选择目录节点");
        //         return;
        //     }
        // }
        me.callParent(arguments);
    }
});
Ext.define('Admin.view.sys.dept.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_dept_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_dept_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_dept_actionpanel'
            }, {
                xtype: 'sys_dept_treegridlist',
                reference: 'treeGrid',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
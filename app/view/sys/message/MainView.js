Ext.define('Admin.view.sys.message.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_message_mainview',

    controller: 'sys_message_mainviewcontroller',

    layout : {
        align : 'stretch',
        type : 'hbox'
    },

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_dic_sysdictree',
                sysDicTypeCode: 'DIC_MESSAGE_TYPE',
                title: '消息类型',
                reference: 'leftTreeGrid',
                margin: '0 5 0 0',
                width: 200
            }, {
                xtype: 'container',
                flex: 1,
                layout: {
                    align: 'stretch',
                    type: 'vbox'
                },
                items: [{
                    xtype: 'sys_message_actionpanel',
                    reference: 'actionpanel'
                }, {
                    xtype: 'sys_message_gridlist',
                    reference: 'gridlist',
                    flex: 1
                }]
            }]
        });

        me.callParent(arguments);
    }

});
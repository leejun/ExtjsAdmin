Ext.define('Admin.view.sys.message.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_message_mainviewcontroller',

    listen: {
        component: {
            'basesearchform[reference=searchForm] > textfield': {
                //antionPanel查询条件控件键盘事件
                specialkey: 'specialkeyHandler'
            },

            //监听左侧树点击事件
            'treepanel[reference=leftTreeGrid]': {
                itemclick: 'leftTreeItemclick'
            }
        },

        controller: {
            'main_maincontroller': {
                //监听主控制器websocket事件
                unReadMsgCountUpdate: 'unReadMsgCountUpdate'
            }
        }
    },

    /*高级查询条件*/
    searchPlusParams: {},

    /*监听websocket广播事件*/
    unReadMsgCountUpdate: function () {
        this.refreshGridList();
    },

    /*左侧树单击事件*/
    leftTreeItemclick: function (tree, record, item, index, e, eOpts) {
        this.searchRecord();
    },

    /*actionpanel-批量删除事件*/
    onDel: function () {
        var me = this;
        if (!me.lookup('gridlist').isSelected()) {
            Ext.popup.Msg('提示信息', '请选择一行记录!');
            return;
        }
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: me.deleteBatchRequest,
            scope: me
        });
    },
    deleteBatchRequest: function (buttonId) {
        if (buttonId === "ok") {
            var me = this;
            var records = this.lookup('gridlist').getSelectedRecords(),
                ids = [];
            Ext.each(records, function (item) {
                ids.push(item.getId());
            });
            ids = ids.join(",");
            Ext.Ajax.request({
                url: ServerUrl + '/SysMessageController/deleteBatchById',
                method: 'POST',
                successHint: true,
                maskContainer: me.getView(),
                params: {
                    ids: ids
                },
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*gridlist-详细事件*/
    onShow: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        Ext.Ajax.request({
            url: ServerUrl + '/SysMessageController/selectById',
            method: 'GET',
            successHint: false,
            maskContainer: me.getView(),
            params: {id: record.getId()},
            success: function () {
                //消息类型编码 就是对应模块的视图类型
                var viewType = record.get('SysDicCode_');
                if(viewType) {
                    //路由到指定模块
                    me.redirectTo(viewType);
                }
                me.refreshGridList();
            }
        });
    },

    /*gridlist-标记已读*/
    onRead: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        if(record.get('IsRead_') === true) {
            return;
        }
        Ext.Ajax.request({
            url: ServerUrl + '/SysMessageController/selectById',
            method: 'GET',
            successHint: true,
            maskContainer: me.getView(),
            params: {id: record.getId()},
            success: function () {
                me.refreshGridList();
            }
        });
    },

    /*gridlist-删除事件*/
    onDelete: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            recordId: record.getId(),
            fn: me.deleteRequest,
            scope: me
        });
    },

    /*发送删除数据请求*/
    deleteRequest: function (buttonId, text, opt) {
        var me = this;
        if (buttonId === "ok") {
            Ext.Ajax.request({
                url: ServerUrl + '/SysMessageController/deleteById',
                //请求方式
                method: 'POST',
                //操作成功提示
                successHint: true,
                //网络请求时遮罩当前视图
                maskContainer: me.getView(),
                //请求参数
                params: {id: opt.recordId},
                //成功回调
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function (field, e) {
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, true);
        //左侧树选中节点ID
        var treeNodeId = this.lookup('leftTreeGrid').getSelectedRecordId();
        if (treeNodeId && treeNodeId !== '0') {
            requestParams.treeNodeId = treeNodeId;
        }
        Ext.apply(requestParams, this.searchPlusParams);
        this.lookup('gridlist').search(requestParams);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchPlusParams = {};
        this.lookup('leftTreeGrid').deselectAll();
        this.searchRecord();
    }
});

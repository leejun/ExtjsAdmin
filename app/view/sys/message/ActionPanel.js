Ext.define('Admin.view.sys.message.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'sys_message_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [

                    {
                        xtype: 'basesearchform',
                        reference: 'searchForm',
                        items: [
                            {
                                xtype: 'datefield',
                                name: 'TimeBegin_',
                                emptyText: '创建时间:起始',
                                fieldLabel: '查询条件'
                            },
                            {
                                xtype: 'datefield',
                                name: 'TimeEnd_',
                                emptyText: '创建时间:结束'
                            },
                            {
                                xtype: 'combobox',
                                name: 'IsRead_',
                                queryMode: 'local',
                                displayField: 'name',
                                valueField: 'value',
                                editable: false,
                                value: 'all',
                                store: [
                                    { value: 'all', name: '全部'},
                                    { value: 'unread', name: '未读'},
                                    { value: 'read', name: '已读'}
                                ],
                                margin: '0 0 0 0'
                            }
                        ]
                    }, {
                        iconCls: 'x-fa fa-times-circle',
                        ui: 'soft-blue',
                        handler: 'clearSearch'
                    }, {
                        iconCls: 'x-fa fa-search',
                        ui: 'soft-blue',
                        handler: 'searchRecord'
                    },
                    {
                        xtype: 'tbfill'
                    }, {
                        xtype: 'delbutton',
                        handler: 'onDel'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
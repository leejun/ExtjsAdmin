Ext.define('Admin.view.sys.message.GridList', {
    extend: 'Admin.base.BaseGridPanel',
    xtype: 'sys_message_gridlist',

    multiSelect: true,
    pagination: true,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Admin.store.sys.message.SysMessageStore', {
                autoLoad: true
            }),
            columns: [
                {text: 'PK', dataIndex: 'Id_', hidden: true},
                {text: '消息内容', dataIndex: 'Message_', minWidth: 100, flex: 1,
                    renderer: function (value, metaData, record) {
                        metaData.tdAttr = 'data-qtip="' + value + '"';
                        var SysDicName_ = record.get('SysDicName_');
                        return SysDicName_+'：'+value;
                    }
                },
                {text: '创建时间', dataIndex: 'CreateTime_', width: 160},
                //{text:'用户表ID', dataIndex:'SysUserId_', minWidth:100, flex : 1},
                //{text: '消息类型', dataIndex: 'SysDicName_', width: 140},
                {text: '是否阅读', dataIndex: 'IsRead_', width: 80, align: 'center', renderer: Admin.base.BaseUtils.rendererBoolean},
                {text: '创建用户', dataIndex: 'CreateUser_', width: 100, align: 'center'},
                {text: '消息类型编码', dataIndex: 'SysDicCode_', width: 160},
                //{text:'阅读时间', dataIndex:'ReadTime_', width:100},
                {
                    xtype: 'actioncolumn',
                    dataIndex: 'bool',
                    text: '操作',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-times',
                            tooltip: '删除',
                            handler: 'onDelete'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-eye',
                            tooltip: '标记为已读',
                            handler: 'onRead'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-file',
                            tooltip: '跳转到详细页面',
                            handler: 'onShow'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});
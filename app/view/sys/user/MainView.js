Ext.define('Admin.view.sys.user.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_user_mainview',

    controller: 'sys_user_mainviewcontroller',

    layout : {
        align : 'stretch',
        type : 'hbox'
    },

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_dept_sysdepttree',
                title: '组织机构',
                reference: 'leftTreeGrid',
                margin: '0 5 0 0',
                width: 200
            }, {
                xtype: 'container',
                flex: 1,
                layout: {
                    align: 'stretch',
                    type: 'vbox'
                },
                items: [{
                    xtype: 'sys_user_actionpanel',
                    reference: 'actionpanel'
                }, {
                    xtype: 'sys_user_gridlist',
                    reference: 'gridlist',
                    flex: 1
                }]
            }]
        });

        me.callParent(arguments);
    }

});
Ext.define('Admin.view.sys.user.EditFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_user_editformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_user_editformwincontroller',
    height: 500,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [
                    {
                        xtype: 'hiddenfield',
                        name: 'Id_',
                        fieldLabel: 'PK'
                    },
                    {
                        xtype: 'textfield',
                        name: 'Name_',
                        allowBlank: false,
                        fieldLabel: '姓名'
                    },
                    {
                        xtype: 'textfield',
                        name: 'LoginName_',
                        allowBlank: false,
                        fieldLabel: '登录名'
                    },
                    {
                        xtype: 'numberfield',
                        name: 'Phone_',
                        allowBlank: false,
                        fieldLabel: '电话'
                    },
                    {
                        xtype: 'textfield',
                        name: 'Email_',
                        vtype: 'email',
                        allowBlank: false,
                        fieldLabel: '邮箱'
                    },
                    {
                        xtype: 'sys_dept_sysdepttreepicker',
                        name: 'SysDeptId_',
                        allowBlank: false,
                        fieldLabel: '选择部门'
                    },
                    {
                        xtype: 'yesnocombobox',
                        name: 'Stop_',
                        allowBlank: false,
                        value: false,
                        yesText: '禁用',
                        noText: '启用',
                        fieldLabel: '用户状态'
                    },
                    {
                        xtype: 'filefield',
                        name: 'file',
                        buttonConfig: {
                            text: '选择照片'
                        },
                        fieldLabel: '选择照片',
                        listeners:{
                            afterrender: function(cmp){
                                cmp.fileInputEl.set({
                                    accept:'image/*'
                                });
                            }
                        }
                    },
                    {
                        xtype: 'tagfield',
                        store: Ext.create('Admin.store.sys.role.SysRoleComboStore'),
                        value: [],
                        displayField: 'RoleName_',
                        valueField: 'Id_',
                        filterPickList: true,
                        queryMode: 'local',
                        name: 'Roles_',
                        fieldLabel: '选择角色'
                    },
                    {
                        xtype: 'numberfield',
                        name: 'Index_',
                        allowBlank: false,
                        value: 0,
                        fieldLabel: '序号'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        xtype: 'savebutton',
        handler: 'onSave'
    }, {
        text: '取消',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
Ext.define('Admin.view.sys.user.DetailFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_user_detailformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_user_detailformwincontroller',
    height: 380,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            name: 'Name_',
                            readOnly: true,
                            fieldLabel: '姓名'
                        }, {
                            name: 'Phone_',
                            fieldLabel: '电话',
                            readOnly: true
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            name: 'LoginName_',
                            readOnly: true,
                            fieldLabel: '登录名'
                        }, {
                            readOnly: true,
                            name: 'Password_',
                            fieldLabel: '登录密码'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            name: 'Salt_',
                            fieldLabel: '盐值',
                            readOnly: true
                        }, {
                            readOnly: true,
                            name: 'Email_',
                            fieldLabel: '邮箱'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            readOnly: true,
                            name: 'SysDeptName_',
                            fieldLabel: '部门名称'
                        }, {
                            readOnly: true,
                            name: 'SysDeptCode_',
                            fieldLabel: '部门编码'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            name: 'CreateTime_',
                            fieldLabel: '创建时间',
                            readOnly: true
                        }, {
                            readOnly: true,
                            xtype: 'yesnocombobox',
                            name: 'Stop_',
                            yesText: '禁用',
                            noText: '启用',
                            fieldLabel: '用户状态'
                        }]
                    },
                    {
                        readOnly: true,
                        xtype: 'tagfield',
                        store: Ext.create('Admin.store.sys.role.SysRoleComboStore'),
                        value: [],
                        displayField: 'RoleName_',
                        valueField: 'Id_',
                        filterPickList: true,
                        queryMode: 'local',
                        name: 'Roles_',
                        fieldLabel: '用户角色'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        text: '关闭',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
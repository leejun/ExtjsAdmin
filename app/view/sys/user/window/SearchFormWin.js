Ext.define('Admin.view.sys.user.SearchFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_user_searchformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_user_searchformwincontroller',

    height: 350,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [
                    {
                        xtype: 'textfield',
                        name : 'Name_',
                        fieldLabel : '姓名'
                    },
                    {
                        xtype: 'textfield',
                        name : 'LoginName_',
                        fieldLabel : '登录名'
                    },
                    {
                        xtype: 'textfield',
                        name : 'Email_',
                        vtype: 'email',
                        fieldLabel : '邮箱'
                    },
                    {
                        xtype: 'textfield',
                        name : 'Phone_',
                        fieldLabel : '电话'
                    },
                    {
                        xtype: 'yesnocombobox',
                        name: 'Stop_',
                        yesText: '禁用',
                        noText: '启用',
                        fieldLabel: '用户状态'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    tools: [{
        type:'refresh',
        tooltip: '清空查询条件',
        handler: 'onClean'
    }],

    buttons: [{
        text: '查询',
        iconCls : 'x-fa fa-search-plus',
        ui: 'soft-blue',
        handler: 'onSearch'
    }, {
        text: '取消',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
Ext.define('Admin.view.sys.user.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'sys_user_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'basesearchform',
                        reference: 'searchForm',
                        items: [
                            {
                                xtype: 'textfield',
                                name: 'QueryParam_',
                                emptyText: '姓名|登录名|电话|邮箱',
                                margin: '0 0 0 0',
                                fieldLabel: '查询条件'
                            }
                        ]
                    }, {
                        iconCls: 'x-fa fa-times-circle',
                        ui: 'soft-blue',
                        handler: 'clearSearch'
                    }, {
                        iconCls: 'x-fa fa-search',
                        ui: 'soft-blue',
                        handler: 'searchRecord'
                    }, {
                        iconCls: 'x-fa fa-search-plus',
                        ui: 'soft-blue',
                        handler: 'searchPlusRecord'
                    },
                    {
                        xtype: 'tbfill'
                    }, {
                        xtype: 'addbutton',
                        handler: 'onAdd'
                    }, '-', {
                        xtype: 'delbutton',
                        handler: 'onDel'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
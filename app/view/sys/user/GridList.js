Ext.define('Admin.view.sys_user.GridList', {
    extend: 'Admin.base.BaseGridPanel',
    xtype: 'sys_user_gridlist',

    /*多选*/
    multiSelect: true,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Admin.store.sys.user.SysUserStore', {
                autoLoad: true,
                pageSize: 15
            }),
            columns: [
                {text: 'PK', dataIndex: 'Id_', hidden: true},
                {
                    xtype: 'gridcolumn',
                    renderer: Admin.base.BaseUtils.rendererProfile,
                    width: 75,
                    dataIndex: 'PictureUrl_',
                    text: '头像'
                },
                {text: '姓名', dataIndex: 'Name_', width: 100},
                {
                    text: '状态',
                    dataIndex: 'Stop_',
                    align: 'center',
                    width: 80,
                    renderer: Admin.base.BaseUtils.rendererStop
                },
                {text: '登录名', dataIndex: 'LoginName_', minWidth: 100, flex: 1},
                {text: '邮箱', dataIndex: 'Email_', minWidth: 100, flex: 1.5},
                {text: '电话', dataIndex: 'Phone_', minWidth: 100, flex: 1},
                {text: '部门名称', dataIndex: 'SysDeptName_', minWidth: 100, flex: 1},
                {text: '部门编码', dataIndex: 'SysDeptCode_', minWidth: 100, flex: 1},
                {text: '序号', dataIndex: 'Index_', width: 60},
                {
                    xtype: 'actioncolumn',
                    dataIndex: 'bool',
                    text: '操作',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-pencil-alt',
                            tooltip: '编辑',
                            handler: 'onEdit'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-times',
                            tooltip: '删除',
                            handler: 'onDelete'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-file',
                            tooltip: '详细',
                            handler: 'onShow'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});
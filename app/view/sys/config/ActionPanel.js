Ext.define('Admin.view.sys.config.ActionPanel', {
	extend : 'Admin.base.BaseActionPanel',
	xtype : 'sys_config_actionpanel',

	initComponent : function() {
		var me = this;

		Ext.applyIf(me, {
			items: {
				xtype: 'toolbar',
				items: [
					 
					{
						xtype : 'basesearchform',
						reference : 'searchForm',
						items : [ 
							{
								xtype: 'textfield',
								name : 'queryParam',
								margin : '0 0 0 0',
								emptyText: '配置键名',
								fieldLabel : '条件查询'
							} 
						]
					}, {
						iconCls : 'x-fa fa-times-circle',
						ui : 'soft-blue',
						handler : 'clearSearch'
					}, {
						iconCls : 'x-fa fa-search',
						ui : 'soft-blue',
						handler : 'searchRecord'
					},
					{
						xtype: 'tbfill'
					}, {
						xtype: 'addbutton',
						handler: 'onAdd'
					}
				]
		    }
		});

		me.callParent(arguments);
	}
});
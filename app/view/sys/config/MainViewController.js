Ext.define('Admin.view.sys.config.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_config_mainviewcontroller',

    listen: {
        component: {
            'basesearchform[reference=searchForm] > textfield': {
                //antionPanel查询条件控件键盘事件
                specialkey: 'specialkeyHandler'
            }
        },

        controller: {
            'sys_config_insertformwincontroller': {
                //监听数据新增事件(刷新表格)
                recordAdded: 'onRecordAdded'
            },
            'sys_config_editformwincontroller': {
                //监听数据编辑事件(刷新表格)
                recordUpdated: 'onRecordUpdated'
            }
        }
    },


    /*actionpanel-新增事件*/
    onAdd: function () {
        var me = this;
        var win = Ext.create({
            xtype: 'sys_config_insertformwin',
            title: '新增'
        });
        me.getView().add(win).show();
    },



    /*gridlist-编辑事件*/
    onEdit: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        var win = Ext.create({
            xtype: 'sys_config_editformwin',
            title: '编辑'
        });
        me.getView().add(win).show();
        win.getController().loadDataById(record.getId());
    },


    /*gridlist-删除事件*/
    onDelete: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            recordId: record.getId(),
            fn: me.deleteRequest,
            scope: me
        });
    },

    /*发送删除数据请求*/
    deleteRequest: function (buttonId, text, opt) {
        var me = this;
        if (buttonId === "ok") {
            Ext.Ajax.request({
                url: ServerUrl + '/SysConfigController/deleteById',
                //请求方式
                method: 'POST',
                //操作成功提示
                successHint: true,
                //网络请求时遮罩当前视图
                maskContainer: me.getView(),
                //请求参数
                params: {id: opt.recordId},
                //成功回调
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*添加数据完成事件*/
    onRecordAdded: function () {
        this.refreshGridList();
    },

    /*编辑数据完成事件*/
    onRecordUpdated: function () {
        this.refreshGridList();
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, true);
        this.lookup('gridlist').search(requestParams);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchRecord();
    }
});

Ext.define('Admin.view.sys.config.GridList', {
    extend: 'Admin.base.BaseGridPanel',
    xtype: 'sys_config_gridlist',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Admin.store.sys.config.SysConfigStore', {
                autoLoad: true
            }),
            columns: [
                {text: '', dataIndex: 'Id_', hidden: true},
                {text: '配置名称', dataIndex: 'ConfigName_', width: 150, flex: 1},
                {text: '配置键名', dataIndex: 'ConfigKey_', width: 150},
                {text: '配置键值', dataIndex: 'ConfigValue_', width: 150},
                {text: '系统内置', dataIndex: 'SystemIn_', align: 'center', width: 80, renderer:Admin.base.BaseUtils.rendererBoolean},
                {text: '备注', dataIndex: 'Remark_', minWidth: 100, flex: 1},
                {text: '更新时间', dataIndex: 'UpdateTime_', width: 160},
                {
                    xtype: 'actioncolumn',
                    dataIndex: 'bool',
                    width: 80,
                    text: '操作',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-pencil-alt',
                            tooltip: '编辑',
                            handler: 'onEdit'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-times',
                            tooltip: '删除',
                            handler: 'onDelete'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});
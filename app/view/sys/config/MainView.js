Ext.define('Admin.view.sys.config.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_config_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_config_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_config_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'sys_config_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
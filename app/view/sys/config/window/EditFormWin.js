Ext.define('Admin.view.sys.config.EditFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_config_editformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_config_editformwincontroller',

    height: 350,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [
                    {
                        xtype: 'hiddenfield',
                        name : 'Id_'
                    },
                    {
                        xtype: 'textfield',
                        name : 'ConfigName_',
                        fieldLabel : '配置名称'
                    },
                    {
                        xtype: 'textfield',
                        name : 'ConfigKey_',
                        fieldLabel : '配置键名'
                    },
                    {
                        xtype: 'textfield',
                        name : 'ConfigValue_',
                        fieldLabel : '配置键值'
                    },
                    {
                        xtype: 'yesnocombobox',
                        name : 'SystemIn_',
                        fieldLabel : '系统内置'
                    },
                    {
                        xtype: 'textfield',
                        name : 'Remark_',
                        fieldLabel : '备注'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        xtype: 'savebutton',
        handler: 'onSave'
    }, {
        xtype: 'cancelbutton',
        handler: 'onCancel'
    }]

});
Ext.define('Admin.view.sys_role.GridList', {
    extend : 'Admin.base.BaseGridPanel',
    xtype : 'sys_role_gridlist',

    pagination: true,

    initComponent : function() {
        var me = this;
        Ext.applyIf(me, {
            store : Ext.create('Admin.store.sys.role.SysRoleStore', {
                autoLoad : true
            }),
            columns : [
                {text:'PK', dataIndex:'Id_', hidden:true},
                {xtype: 'rownumberer'},
                {text:'角色名称', dataIndex:'RoleName_', minWidth:150, flex : 1},
                {text:'角色编码', dataIndex:'RoleCode_', minWidth:150, flex : 1},
                {text:'角色状态', dataIndex:'Stop_', width:120, align:'center', renderer: Admin.base.BaseUtils.rendererStop},
                {
                    text : '配置用户',
                    dataIndex : 'bindUser',
                    renderer: function(val, metadata) {
                        metadata.style = 'cursor: pointer;';
                        return '<span class="bindUser" style="border-radius:0.25em; padding: 2px; background-color: #86c747; color: #fff">' + '配置用户' + '</span>';
                    }
                },
                {
                    text : '配置菜单',
                    dataIndex : 'bindMenu',
                    renderer: function(val, metadata) {
                        metadata.style = 'cursor: pointer;';
                        return '<span class="bindMenu" style="border-radius:0.25em; padding: 2px; background-color: #86c747; color: #fff">' + '配置菜单' + '</span>';
                    }
                },
                {
                    text : '配置权限',
                    dataIndex : 'bindPermission',
                    renderer: function(val, metadata) {
                        metadata.style = 'cursor: pointer;';
                        return '<span class="bindPermission" style="border-radius:0.25em; padding: 2px; background-color: #86c747; color: #fff">' + '配置权限' + '</span>';
                    }
                },
                {text:'显示顺序', dataIndex:'Index_', align:'center', width:80},
                {text:'创建时间', dataIndex:'CreateTime_', width:160},
                {
                    xtype: 'actioncolumn',
                    dataIndex: 'bool',
                    text: '操作',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-pencil-alt',
                            tooltip: '编辑',
                            handler: 'onEdit'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-times',
                            tooltip: '删除',
                            handler: 'onDelete'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-ban',
                            tooltip: '启用|禁用',
                            handler: 'onStop'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});
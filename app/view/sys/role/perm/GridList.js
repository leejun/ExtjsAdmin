Ext.define('Admin.view.sys.role.perm.GridList', {
    extend : 'Admin.base.BaseGridPanel',
    xtype : 'sys_role_perm_gridlist',

    pagination: false,

    initComponent : function() {
        var me = this;

        Ext.applyIf(me, {
            store : Ext.create('Admin.store.sys.role.SysRolePermissionStore'),
            columns : [
                {text:'PK', dataIndex:'Id_', hidden:true},
                {xtype: 'checkcolumn', text: '绑定', dataIndex: 'RoleBind_', width: 70},
                {text:'请求路径', dataIndex:'UrlPattern_', minWidth:200, flex : 1.5},
                {text:'功能说明', dataIndex:'ActionName_', minWidth:120, flex: 1},
                {text:'请求方式', dataIndex:'HttpMethod_', width:100},
                {text:'请求参数类型', dataIndex:'ParametersType_', width: 100, renderer: Admin.base.BaseUtils.rendererQtip},
                {text:'代码已删除', dataIndex:'CodeIDelete_', width:100, align:'center', renderer: Admin.base.BaseUtils.rendererBoolean},
                {text:'需要登录', dataIndex:'CheckLogin_', xtype: 'checkcolumn', width:80, disabled:true},
                {text:'需要授权', dataIndex:'CheckPermission_', xtype: 'checkcolumn', width:80, disabled:true},
                {text:'记录日志', dataIndex:'RequireLog_', xtype: 'checkcolumn', width:80, disabled:true},
                {text:'记录参数', dataIndex:'LogRequestParam_', xtype: 'checkcolumn', width:100, disabled:true},
                {text:'记录返回值', dataIndex:'LogReturnValue_', xtype: 'checkcolumn', width:100, disabled:true}
            ]
        });
        me.callParent(arguments);
    }
});
Ext.define('Admin.view.sys.role.perm.MainView', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_role_perm_mainview',

    controller: 'sys_role_perm_mainviewcontroller',
    width: 1200,
    height: 550,

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_role_perm_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'sys_role_perm_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
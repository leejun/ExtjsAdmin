Ext.define('Admin.view.sys.role.perm.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_role_perm_mainviewcontroller',

    listen: {
        component: {
            'yesnocombobox': {
                //combo选择事件
                select: 'roleBindChange'
            },

            'basesearchform[reference=searchForm] > textfield': {
                //antionPanel查询条件控件键盘事件
                specialkey: 'specialkeyHandler'
            }
        }
    },

    //角色ID
    SysRoleId_: 0,

    //全选
    selectAll: function () {
        var store = this.lookup('gridlist').getStore();
        store.each(function (r) {
            r.set('RoleBind_', true);
        });
    },

    //反选
    unselectAll: function () {
        var store = this.lookup('gridlist').getStore();
        store.each(function (r) {
            r.set('RoleBind_', false);
        });
    },

    //保存角色权限
    saveRecord: function () {
        var grid = this.lookup('gridlist');
        if (grid.getStore().getModifiedRecords().length <= 0) {
            Ext.popup.Msg('提示信息', '没有修改过的数据');
            return;
        }
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.doSaveRecord,
            scope: this
        });
    },
    doSaveRecord: function (buttonId) {
        if (buttonId === 'ok') {
            var me = this;
            me.getView().setLoading('数据保存中...');
            var gridlist = this.lookup('gridlist');
            gridlist.getStore().sync({
                success: function () {
                    Ext.popup.Msg('提示信息', '操作成功');
                    me.searchRecord();
                },
                callback: function () {
                    me.getView().setLoading(false);
                }
            });
        }
    },

    /*角色绑定combo选择改变事件*/
    roleBindChange: function (combo, newValue, oldValue, eOpts) {
        this.searchRecord();
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function (field, e) {
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, false);
        requestParams.SysRoleId_ = this.SysRoleId_;
        Ext.apply(requestParams, this.searchPlusParams);
        this.lookup('gridlist').search(requestParams);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchPlusParams = {};
        this.searchRecord();
    }
});

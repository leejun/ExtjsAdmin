Ext.define('Admin.view.sys.role.perm.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'sys_role_perm_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'basesearchform',
                        reference: 'searchForm',
                        items: [
                            {
                                xtype: 'yesnocombobox',
                                name: 'RoleBind_',
                                allowBlank: false,
                                yesValue: 'true',
                                noValue: 'false',
                                value: true,
                                yesText: '已绑定角色',
                                noText: '未绑定角色',
                                fieldLabel: '绑定状态'
                            },
                            {
                                xtype: 'textfield',
                                name: 'QueryParam_',
                                emptyText: '请求路径|功能说明',
                                margin: '0 0 0 0',
                                fieldLabel: '查询条件'
                            }
                        ]
                    }, {
                        iconCls: 'x-fa fa-times-circle',
                        ui: 'soft-blue',
                        handler: 'clearSearch'
                    }, {
                        iconCls: 'x-fa fa-search',
                        ui: 'soft-blue',
                        handler: 'searchRecord'
                    },
                    {
                        xtype: 'tbfill'
                    }, {
                        text: '全选',
                        handler: 'selectAll',
                        iconCls: 'x-fa  fa-check-square',
                        ui: 'soft-green'
                    }, '-', {
                        text: '反选',
                        iconCls: 'x-fa fa-square',
                        ui: 'gray',
                        handler: 'unselectAll'
                    }, '-', {
                        xtype: 'savebutton',
                        handler: 'saveRecord'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
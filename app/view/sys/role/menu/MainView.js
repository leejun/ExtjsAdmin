Ext.define('Admin.view.sys.role.memu.MainView', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_role_memu_mainview',

    controller: 'sys_role_menu_mainviewcontroller',

    width: 1200,
    height: 550,

    layout : {
        align : 'stretch',
        type : 'vbox'
    },

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_role_menu_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'sys_role_menu_treegridlist',
                reference: 'treeGrid',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
Ext.define('Admin.view.sys.role.menu.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_role_menu_mainviewcontroller',

    //树全选
    selectAll: function () {
        this.lookup('treeGrid').getRootNode().cascadeBy(function (child) {
            child.set('checked', true);
        });
    },

    //树反选
    unselectAll: function () {
        this.lookup('treeGrid').getRootNode().cascadeBy(function (child) {
            child.set('checked', false);
        });
    },

    //保存角色菜单绑定信息
    saveRecord: function () {
        if (this.lookup('treeGrid').getModifiedRecordSize() <= 0) {
            Ext.popup.Msg('提示信息', '没有修改过的数据');
            return;
        }
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.doSaveRecord,
            scope: this
        });
    },
    doSaveRecord: function (buttonId) {
        if (buttonId === 'ok') {
            var me = this;
            me.getView().setLoading('数据保存中...');
            var treeGrid = me.lookup('treeGrid');
            treeGrid.getStore().sync({
                success: function () {
                    Ext.popup.Msg('提示信息', '保存成功');
                },
                callback: function () {
                    me.getView().setLoading(false);
                    me.getView().close();
                }
            });
        }
    },

    //加载菜单数据
    loadRoleMenuTreeList: function (sysRoleId) {
        var treeGrid = this.lookup('treeGrid');
        treeGrid.store.proxy.extraParams = {sysRoleId: sysRoleId};
        treeGrid.refreshView();
    }
});

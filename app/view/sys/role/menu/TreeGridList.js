Ext.define('Admin.view.sys.role.menu.TreeGridList', {
    extend: 'Admin.base.BaseTreePanel',
    xtype: 'sys_role_menu_treegridlist',

    //是否隐藏roo节点  false:隐藏  true:显示
    rootVisible: false,

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            store: Ext.create('Admin.store.sys.role.SysRoleMenuStore'),
            viewConfig: {
                loadingText: "数据读取中..."
            },
            columns: [{
                xtype: 'treecolumn',
                sortable: false,
                text: '菜单名称',
                dataIndex: 'text',
                flex: 1,
                minWidth: 200
            }, {
                sortable: false,
                text: '排序',
                dataIndex: 'index',
                align: 'center',
                width: 60
            }, {
                sortable: false,
                text: '类型',
                dataIndex: 'leaf',
                width: 80,
                renderer: function (value) {
                    return value ? '菜单' : '目录';
                }
            }, {
                sortable: false,
                text: '状态',
                dataIndex: 'stop',
                width: 80,
                renderer: Admin.base.BaseUtils.rendererStop
            }, {
                sortable: false,
                text: '图标',
                dataIndex: 'iconCls',
                width: 200
            }, {
                sortable: false,
                text: '模块视图',
                dataIndex: 'viewType',
                flex: 1,
                minWidth: 200
            }]
        });

        me.callParent(arguments);
        me.relayEvents(me.getStore(), ['load'], 'store');
        me.on('beforeedit', me.handleBeforeEdit, me);
    },

    handleBeforeEdit: function (editingPlugin, e) {
        if (e.record.get('id') === '0') {
            Ext.popup.Msg('提示信息', '根节点不允许编辑');
            return false;
        }
        return true;
    }
});
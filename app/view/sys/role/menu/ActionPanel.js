Ext.define('Admin.view.sys.role.menu.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'sys_role_menu_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'tbfill'
                    }, {
                        text: '全选',
                        handler: 'selectAll',
                        iconCls: 'x-fa  fa-check-square',
                        ui: 'soft-green'
                    }, '-', {
                        text: '反选',
                        iconCls: 'x-fa fa-square',
                        ui: 'gray',
                        handler: 'unselectAll'
                    }, '-', {
                        xtype: 'savebutton',
                        handler: 'saveRecord'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
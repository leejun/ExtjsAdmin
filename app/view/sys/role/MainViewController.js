Ext.define('Admin.view.sys.role.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_role_mainviewcontroller',

    listen: {

        controller: {
            'sys_role_insertformwincontroller': {
                //监听数据新增事件(刷新表格)
                recordAdded: 'onRecordAdded'
            },
            'sys_role_editformwincontroller': {
                //监听数据编辑事件(刷新表格)
                recordUpdated: 'onRecordUpdated'
            },
            'sys_role_searchformwincontroller': {
                //监听高级查询事件(高级查询)
                searchPlus: 'onSearchPlus'
            }
        },

        component: {
            'sys_role_gridlist': {
                //监听单元格单击事件
                cellclick: 'cellclickHandler'
            },

            'basesearchform[reference=searchForm] > textfield': {
                //antionPanel查询条件控件键盘事件
                specialkey: 'specialkeyHandler'
            }
        }
    },

    /*高级查询条件*/
    searchPlusParams: {},

    /*禁用 启动节点*/
    onStop: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: function (buttonId) {
                if (buttonId === "ok") {
                    Ext.Ajax.request({
                        url: ServerUrl + '/SysRoleController/stopById',
                        successHint: true,
                        maskContainer: me.getView(),
                        params: {id: record.getId()},
                        success: function () {
                            me.refreshGridList();
                        }
                    });
                }
            },
            scope: me
        });
    },

    /*处理单元格点击事件*/
    cellclickHandler: function (grid, td, cellIndex, record, tr, rowIndex, e, eOpts) {
        var roleNameCode = '[' + record.get('RoleName_') + '][' + record.get('RoleCode_') + ']';
        //配置用户
        if (e.getTarget('.bindUser')) {
            var win = Ext.create({
                xtype: 'sys_role_user_mainview',
                title: '配置用户' + roleNameCode
            });
            win.show();
            win.getController().SysRoleId_ = record.getId();
            win.getController().searchRecord();
        }

        //配置菜单
        if (e.getTarget('.bindMenu')) {
            var win = Ext.create({
                xtype: 'sys_role_memu_mainview',
                title: '配置菜单' + roleNameCode,
            });
            win.show();
            win.getController().loadRoleMenuTreeList(record.getId());
        }

        //配置权限
        if (e.getTarget('.bindPermission')) {
            var win = Ext.create({
                xtype: 'sys_role_perm_mainview',
                title: '配置权限' + roleNameCode
            });
            win.show();
            win.getController().SysRoleId_ = record.getId();
            win.getController().searchRecord();
        }
    },

    /*actionpanel-新增事件*/
    onAdd: function () {
        var me = this;
        var win = Ext.create({
            xtype: 'sys_role_insertformwin',
            title: '新增'
        });
        me.getView().add(win).show();
    },

    /*actionpanel-批量删除事件*/
    onDel: function () {
        var me = this;
        if (!me.lookup('gridlist').isSelected()) {
            Ext.popup.Msg('提示信息', '请选择一行记录!');
            return;
        }
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: me.deleteBatchRequest,
            scope: me
        });
    },
    deleteBatchRequest: function (buttonId) {
        if (buttonId === "ok") {
            var me = this;
            var records = this.lookup('gridlist').getSelectedRecords(),
                ids = [];
            Ext.each(records, function (item) {
                ids.push(item.getId());
            });
            ids = ids.join(",");
            Ext.Ajax.request({
                url: ServerUrl + '/SysRoleController/deleteBatchById',
                method: 'POST',
                successHint: true,
                maskContainer: me.getView(),
                params: {
                    ids: ids
                },
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*gridlist-编辑事件*/
    onEdit: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        var win = Ext.create({
            xtype: 'sys_role_editformwin',
            title: '编辑'
        });
        me.getView().add(win).show();
        win.getController().loadDataById(record.getId());
    },

    /*gridlist-详细事件*/
    onShow: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        var win = Ext.create({
            xtype: 'sys_role_detailformwin',
            title: '详细'
        });
        me.getView().add(win).show();
        win.getController().loadDataById(record.getId());
    },

    /*gridlist-删除事件*/
    onDelete: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            recordId: record.getId(),
            fn: me.deleteRequest,
            scope: me
        });
    },

    /*发送删除数据请求*/
    deleteRequest: function (buttonId, text, opt) {
        var me = this;
        if (buttonId === "ok") {
            console.log(opt.recordId);
            Ext.Ajax.request({
                url: ServerUrl + '/SysRoleController/deleteById',
                //请求方式
                method: 'POST',
                //操作成功提示
                successHint: true,
                //网络请求时遮罩当前视图
                maskContainer: me.getView(),
                //请求参数
                params: {id: opt.recordId},
                //成功回调
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*添加数据完成事件*/
    onRecordAdded: function () {
        this.refreshGridList();
    },

    /*编辑数据完成事件*/
    onRecordUpdated: function () {
        this.refreshGridList();
    },

    /*高级查询事件*/
    onSearchPlus: function (searchParams) {
        this.searchPlusParams = searchParams;
        this.searchRecord();
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function (field, e) {
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, true);
        Ext.apply(requestParams, this.searchPlusParams);
        this.lookup('gridlist').search(requestParams);
    },

    /*ActionPanel高级查询*/
    searchPlusRecord: function () {
        var me = this;
        var win = Ext.create({
            xtype: 'sys_role_searchformwin',
            title: '查询'
        });
        me.getView().add(win).show();
        win.getController().setFormData(me.searchPlusParams);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchPlusParams = {};
        this.searchRecord();
    }
});

Ext.define('Admin.view.sys.role.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'sys_role_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [

                    {
                        xtype: 'basesearchform',
                        reference: 'searchForm',
                        items: [
                            {
                                xtype: 'textfield',
                                name: 'QueryParam_',
                                fieldLabel: '查询条件',
                                emptyText: '角色名称|角色编码',
                                margin: '0 0 0 0'
                            }
                        ]
                    }, {
                        iconCls: 'x-fa fa-times-circle',
                        ui: 'soft-blue',
                        handler: 'clearSearch'
                    }, {
                        iconCls: 'x-fa fa-search',
                        ui: 'soft-blue',
                        handler: 'searchRecord'
                    },
                    {
                        xtype: 'tbfill'
                    }, {
                        xtype: 'addbutton',
                        handler: 'onAdd'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
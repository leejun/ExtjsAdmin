Ext.define('Admin.view.sys.role.DetailFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_role_detailformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_role_detailformwincontroller',
    height: 300,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
            xtype: 'baseformpanel',
            reference: 'formpanel',
                items: [
                    {
                        xtype: 'textfield',
                        name : 'RoleName_',
                        fieldLabel : '角色名称'
                    },

                    {
                        xtype: 'textfield',
                        name : 'RoleCode_',
                        fieldLabel : '角色编码'
                    },

                    {
                        xtype: 'textfield',
                        name : 'Index_',
                        fieldLabel : '显示顺序'
                    },

                    {
                        xtype: 'textfield',
                        name : 'Stop_',
                        fieldLabel : '是否禁用'
                    },

                    {
                        xtype: 'textfield',
                        name : 'CreateTime_',
                        fieldLabel : '创建时间'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        text: '关闭',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
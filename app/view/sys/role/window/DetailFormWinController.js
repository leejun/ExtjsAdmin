Ext.define('Admin.view.sys.role.DetailFormWinController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_role_detailformwincontroller',

    /*根据id加载数据*/
    loadDataById: function (id) {
        var form = this.lookup("formpanel");
        form.load({
            url: ServerUrl + '/SysRoleController/selectById',
            method: 'get',
            params: {id: id}
        });
    },

    /*关闭窗口*/
    onCancel: function() {
        this.getView().close();
    }
});

Ext.define('Admin.view.sys.role.EditFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_role_editformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_role_editformwincontroller',
    height: 300,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [
                    {
                        xtype: 'hiddenfield',
                        name : 'Id_',
                        fieldLabel : 'PK'
                    },

                    {
                        xtype: 'textfield',
                        name : 'RoleName_',
                        allowBlank: false,
                        fieldLabel : '角色名称'
                    },

                    {
                        xtype: 'textfield',
                        name : 'RoleCode_',
                        allowBlank: false,
                        fieldLabel : '角色编码'
                    },

                    {
                        xtype: 'numberfield',
                        name : 'Index_',
                        allowBlank: false,
                        fieldLabel : '显示顺序'
                    },

                    {
                        xtype: 'yesnocombobox',
                        name : 'Stop_',
                        allowBlank: false,
                        fieldLabel : '是否禁用'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        xtype: 'savebutton',
        handler: 'onSave'
    }, {
        text: '取消',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
Ext.define('Admin.view.sys.role.SearchFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_role_searchformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_role_searchformwincontroller',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [                ]
            }]
        });
        me.callParent(arguments);
    },

    tools: [{
        type:'refresh',
        tooltip: '清空查询条件',
        handler: 'onClean'
    }],

    buttons: [{
        text: '查询',
        iconCls : 'x-fa fa-search-plus',
        ui: 'soft-blue',
        handler: 'onSearch'
    }, {
        text: '取消',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
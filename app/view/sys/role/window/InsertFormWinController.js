Ext.define('Admin.view.sys.role.InsertFormWinController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_role_insertformwincontroller',

    /*保存按钮事件*/
    onSave: function(){
        Ext.Msg.show({
            title:'提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.doSaveRecord,
            scope: this
        });
    },

    /*发送请求*/
    doSaveRecord: function(buttonId) {
        if(buttonId === "ok") {
            var me = this;
            var form = this.lookup("formpanel");
            form.submit({
                url: ServerUrl + '/SysRoleController/insert',
                successHint: true,
                success: function() {
                    me.fireEvent('recordAdded');
                    me.getView().close();
                }
            });
        }
    },

    /*关闭窗口*/
    onCancel: function() {
        this.getView().close();
    }
});

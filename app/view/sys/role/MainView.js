Ext.define('Admin.view.sys.role.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_role_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_role_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_role_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'sys_role_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
Ext.define('Admin.view.sys_role_user.GridList', {
    extend: 'Admin.base.BaseGridPanel',
    xtype: 'sys_role_user_gridlist',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Admin.store.sys.role.SysUserRoleStore'),
            columns: [
                {text: 'PK', dataIndex: 'Id_', hidden: true},
                {xtype: 'checkcolumn', text: '绑定', dataIndex: 'RoleBind_', width: 70},
                {
                    xtype: 'gridcolumn',
                    renderer: Admin.base.BaseUtils.rendererProfile,
                    width: 70,
                    dataIndex: 'PictureUrl_',
                    text: '头像'
                },
                {text: '姓名', dataIndex: 'Name_', width: 100},
                {
                    text: '用户状态',
                    dataIndex: 'Stop_',
                    width: 80,
                    align: 'center',
                    renderer: Admin.base.BaseUtils.rendererStop
                },
                {text: '登录名', dataIndex: 'LoginName_', minWidth: 100, flex: 1},
                {text: '邮箱', dataIndex: 'Email_', minWidth: 100, flex: 1.5},
                {text: '电话', dataIndex: 'Phone_', minWidth: 100, flex: 1},
                {text: '部门名称', dataIndex: 'SysDeptName_', minWidth: 100, flex: 1},
                {text: '部门编码', dataIndex: 'SysDeptCode_', minWidth: 100, flex: 1},
                {text: '序号', dataIndex: 'Index_', width: 60}
            ]
        });
        me.callParent(arguments);
    }
});
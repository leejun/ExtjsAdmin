Ext.define('Admin.view.sys.role.user.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_role_user_mainviewcontroller',

    listen: {
        component: {
            'treepanel[reference=leftTreeGrid]': {
                //左侧树单击事件
                itemclick: 'leftTreeItemclick'
            },

            'yesnocombobox': {
                //combobox控件选择事件
                select: 'roleBindChange'
            },

            'basesearchform[reference=searchForm] > textfield': {
                //antionPanel查询条件控件键盘事件
                specialkey: 'specialkeyHandler'
            }
        }
    },

    //角色ID
    SysRoleId_: 0,

    /*高级查询条件*/
    searchPlusParams: {},

    //全选
    selectAll: function () {
        var store = this.lookup('gridlist').getStore();
        store.each(function (r) {
            r.set('RoleBind_', true);
        });
    },

    //反选
    unselectAll: function () {
        var store = this.lookup('gridlist').getStore();
        store.each(function (r) {
            r.set('RoleBind_', false);
        });
    },

    //保存用户角色
    saveRecord: function () {
        var grid = this.lookup('gridlist');
        if (grid.getStore().getModifiedRecords().length <= 0) {
            Ext.popup.Msg('提示信息', '没有修改过的数据');
            return;
        }
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.doSaveRecord,
            scope: this
        });
    },
    doSaveRecord: function (buttonId) {
        if (buttonId === 'ok') {
            var me = this;
            me.getView().setLoading('数据保存中...');
            this.lookup('gridlist').getStore().sync({
                // 携带其他参数
                // params: {metaKey: 'value'},
                success: function () {
                    //保存成功刷新 去掉红色三角号
                    Ext.popup.Msg('提示信息', '操作成功');
                    me.searchRecord();
                },
                callback: function () {
                    me.getView().setLoading(false);
                }
            });
        }
    },

    /*角色绑定combo选择改变事件*/
    roleBindChange: function (combo, newValue, oldValue, eOpts) {
        this.searchRecord();
    },

    /*左侧树单击事件*/
    leftTreeItemclick: function (tree, record, item, index, e, eOpts) {
        this.searchRecord();
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function(field, e){
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, false);

        //左侧树选中节点ID
        var treeNodeId = this.lookup('leftTreeGrid').getSelectedRecordId();
        var treeNodeCode = this.lookup('leftTreeGrid').getSelecedRecordValue('deptCode');
        if (treeNodeId && treeNodeId !== '0') {
            requestParams.treeNodeId = treeNodeId;
            requestParams.treeNodeCode = treeNodeCode;
        }
        requestParams.SysRoleId_ = this.SysRoleId_;

        Ext.apply(requestParams, this.searchPlusParams);
        this.lookup('gridlist').search(requestParams);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchPlusParams = {};
        this.searchRecord();
    }
});

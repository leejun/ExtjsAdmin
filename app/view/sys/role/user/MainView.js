Ext.define('Admin.view.sys.role.user.MainView', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_role_user_mainview',

    controller: 'sys_role_user_mainviewcontroller',
    width: 1200,
    height: 550,

    layout : {
        align : 'stretch',
        type : 'hbox'
    },

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_dept_sysdepttree',
                reference: 'leftTreeGrid',
                margin: '0 5 0 0',
                header: false,
                width: 200
            }, {
                xtype: 'container',
                flex: 1,
                layout: {
                    align: 'stretch',
                    type: 'vbox'
                },
                items: [{
                    xtype: 'sys_role_user_actionpanel',
                    reference: 'actionpanel'
                }, {
                    xtype: 'sys_role_user_gridlist',
                    reference: 'gridlist',
                    flex: 1
                }]
            }]
        });

        me.callParent(arguments);
    }

});
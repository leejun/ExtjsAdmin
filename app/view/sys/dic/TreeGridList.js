Ext.define('Admin.view.sys.dic.TreeGridList', {
    extend: 'Admin.base.BaseTreePanel',
    xtype: 'sys_dic_treegridlist',

    initComponent: function () {
        var me = this;

        me.plugins = [me.cellEditingPlugin = Ext.create('Ext.grid.plugin.CellEditing')];

        Ext.apply(me, {
            store: Ext.create('Admin.store.sys.dic.SysDicStore'),
            viewConfig: {
                loadingText: "数据读取中...",
                plugins: {
                    ptype: 'treeviewdragdrop',
                    containerScroll: true
                }
            },
            columns: [{
                xtype: 'treecolumn',
                sortable: false,
                text: '字典名称',
                dataIndex: 'text',
                flex: 1,
                minWidth: 100,
                editor: {
                    xtype: 'textfield',
                    selectOnFocus: true,
                    allowOnlyWhitespace: false
                }
            }, {
                sortable: false,
                text: '字典编码',
                dataIndex: 'code',
                editor: 'textfield',
                flex: 1,
                minWidth: 100
            }, {
                sortable: false,
                text: '字典类别编码',
                dataIndex: 'sysDicTypeCode',
                flex: 1,
                minWidth: 100
            }, {
                sortable: false,
                text: '排序',
                dataIndex: 'index',
                align: 'center',
                width: 60
            }, {
                sortable: false,
                text: '类型',
                dataIndex: 'leaf',
                width: 80,
                renderer: function (value) {
                    return value ? '菜单' : '目录';
                }
            }, {
                sortable: false,
                text: '状态',
                dataIndex: 'stop',
                width: 80,
                renderer: Admin.base.BaseUtils.rendererStop
            }, {
                sortable: false,
                xtype: 'checkcolumn',
                text: '展开',
                dataIndex: 'expandedEdit',
                stopSelection: false,
                menuDisabled: true,
                width: 100,
                listeners: {
                    beforecheckchange: function (ck, rowIndex, checked, eOpts) {
                        return (rowIndex !== 0);
                    }
                }
            }, {
                xtype: 'actioncolumn',
                items: [
                    {
                        xtype: 'button',
                        iconCls: 'x-fa fa-pencil-alt',
                        tooltip: '编辑',
                        handler: 'onEdit'
                    },
                    {
                        xtype: 'button',
                        iconCls: 'x-fa fa-times',
                        tooltip: '删除',
                        handler: 'onDelete'
                    },
                    {
                        xtype: 'button',
                        iconCls: 'x-fa fa-ban',
                        tooltip: '启用|禁用',
                        handler: 'onStop'
                    }
                ],
                dataIndex: 'bool',
                text: '操作'
            }]
        });

        me.callParent(arguments);
        me.relayEvents(me.getStore(), ['load'], 'store');
        me.on('beforeedit', me.handleBeforeEdit, me);
    },

    handleBeforeEdit: function (editingPlugin, e) {
        if (e.record.get('id') === '0') {
            Ext.popup.Msg('提示信息', '根节点不允许编辑');
            return false;
        }
        return true;
    }
});
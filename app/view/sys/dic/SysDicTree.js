//系统字典树组件
Ext.define('Admin.view.sys.dic.SysDicTree', {
    extend: 'Admin.base.BaseTreePanel',
    xtype: 'sys_dic_sysdictree',

    tools: [{
        type: 'refresh',
        tooltip: '刷新',
        handler: function (event, toolEl, panelHeader) {
            this.up('sys_dic_sysdictree').refreshView();
        }
    }],

    /*字典类别编码 根据编码查询字典列表返回数据*/
    sysDicTypeCode: '',

    //是否隐藏roo节点  false:隐藏  true:显示
    rootVisible: false,

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            viewConfig: {
                loadingText: "数据读取中..."
            },
            store: Ext.create('Ext.data.TreeStore', {
                model: 'Admin.model.sys.dic.SysDicModel',
                autoLoad: true,

                root: {
                    expanded: true,
                    id: '0',
                    text: '系统字典',
                    leaf: false
                },
                proxy: {
                    type: 'ajax',
                    url: ServerUrl + '/SysDicController/selectTreeListByCode',
                    extraParams: {
                        sysDicTypeCode: me.sysDicTypeCode
                    },
                    reader: {
                        type: 'json',
                        rootProperty: function (data) {
                            return data.data || data.children;
                        },
                        successProperty: 'success'
                    }
                }
            })
        });
        me.callParent(arguments);
    }
});
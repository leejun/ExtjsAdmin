Ext.define('Admin.view.sys.dic.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_dic_mainview',

    controller: 'sys_dic_mainviewcontroller',

    layout : {
        align : 'stretch',
        type : 'hbox'
    },

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_dictype_sysdictypetree',
                title: '字典类别',
                reference: 'leftTreeGrid',
                margin: '0 5 0 0',
                width: 200
            }, {
                xtype : 'container',
                flex: 1,
                layout : {
                    align : 'stretch',
                    type : 'vbox'
                },
                items: [{
                    xtype: 'sys_dic_actionpanel'
                },{
                    xtype: 'sys_dic_treegridlist',
                    reference: 'treeGrid',
                    flex: 1
                }]
            }]
        });

        me.callParent(arguments);
    }

});
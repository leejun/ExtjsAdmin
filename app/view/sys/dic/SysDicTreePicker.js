//系统字典下拉选择树组件
Ext.define('Admin.view.sys.dic.SysDicTreePicker', {
    extend: 'Ext.ux.TreePicker',
    alias: 'widget.sys_dic_sysdictreepicker',

    selectChildren: true,
    rootVisible: false,
    displayField: 'text',
    valueField: 'id',

    /*字典类别编码 根据编码查询字典列表返回数据*/
    sysDicTypeCode: '',

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            viewConfig: {
                loadingText: "数据读取中..."
            },
            store: Ext.create('Ext.data.TreeStore', {
                model: 'Admin.model.sys.dic.SysDicModel',
                autoLoad: true,
                root: {
                    expanded: false,
                    id: '0',
                    text: '系统字典',
                    leaf: false
                },
                proxy: {
                    type: 'ajax',
                    url: ServerUrl + '/SysDicController/selectTreeListByCodeNoStop',
                    extraParams: {
                        sysDicTypeCode: me.sysDicTypeCode
                    },
                    reader: {
                        type: 'json',
                        rootProperty: function (data) {
                            return data.data || data.children;
                        },
                        successProperty: 'success'
                    }
                }
            }),
            columns: [{
                xtype: 'treecolumn',
                sortable: false,
                text: '类别名称',
                dataIndex: 'text',
                flex: 1,
                minWidth: 200
            }, {
                sortable: false,
                text: '类别编码',
                dataIndex: 'code',
                editor: 'textfield',
                width: 200
            }]
        });
        me.callParent(arguments);
    },

    setValue: function (value) {
        var me = this;

        // if(value) {
        //     var node = me.store.getNodeById(value);
        //     if(!node.isLeaf()) {
        //         Ext.popup.Msg("提示信息", "不允许选择目录节点");
        //         return;
        //     }
        // }

        me.callParent(arguments);
    }
});
//系统字典combobox组件
Ext.define("Admin.view.sys.dic.SysDicComboBox", {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.sys_dic_sysdiccombobox',

    /*是否自动选择Combo中第一项*/
    defaultFirst: false,

    editable: false,

    /*字典类别编码 根据编码查询字典列表返回数据*/
    sysDicTypeCode: '',

    displayField: 'Name_',
    valueField: 'Id_',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Ext.data.Store', {
                fields: ['Id_', 'Name_'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: ServerUrl + '/SysDicController/selectListByCodeNoStop',
                    extraParams: {
                        sysDicTypeCode: me.sysDicTypeCode
                    },
                    reader: {
                        type: 'json',
                        rootProperty: 'data',
                        successProperty: 'success'
                    }
                }
            }),
            queryMode: 'local'
        });

        me.store.on('load', function (store, records) {
            if (records && me.defaultFirst) {
                me.setValue(records[0].get(me.valueField));
            }
        });

        me.callParent(arguments);
    }

});
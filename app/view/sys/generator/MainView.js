Ext.define('Admin.view.sys.generator.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_generator_mainview',

    controller: 'sys_generator_mainviewcontroller',

    layout : {
        align : 'stretch',
        type : 'hbox'
    },

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_dic_sysdictree',
                sysDicTypeCode: 'DIC_CITY',
                title: '字典标题手动修改',
                reference: 'leftTreeGrid',
                margin: '0 5 0 0',
                width: 200
            }, {
                xtype: 'container',
                flex: 1,
                layout: {
                    align: 'stretch',
                    type: 'vbox'
                },
                items: [{
                    xtype: 'sys_generator_actionpanel',
                    reference: 'actionpanel'
                }, {
                    xtype: 'sys_generator_gridlist',
                    reference: 'gridlist',
                    flex: 1
                }]
            }]
        });

        me.callParent(arguments);
    }

});
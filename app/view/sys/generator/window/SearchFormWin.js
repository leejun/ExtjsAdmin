Ext.define('Admin.view.sys.generator.SearchFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_generator_searchformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_generator_searchformwincontroller',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [
                    {
                        xtype: 'textfield',
                        name : 'StringVal1_',
                        fieldLabel : '字符串1'
                    },

                    {
                        xtype: 'textfield',
                        name : 'StringVal2_',
                        fieldLabel : '字符串2'
                    },

                    {
                        xtype: 'sys_dic_sysdiccombobox',
                        sysDicTypeCode: 'DIC_SEX',
                        name : 'SysDicSexId_',
                        fieldLabel : '字典(性别)ID'
                    },

                    {
                        xtype: 'sys_dic_sysdictreepicker',
                        sysDicTypeCode: 'DIC_CITY',
                        name : 'SysDicCityId_',
                        fieldLabel : '字典(城市)ID'
                    },

                    {
                        xtype: 'numberfield',
                        name : 'IntVal1_',
                        fieldLabel : '数值1'
                    },

                    {
                        xtype: 'numberfield',
                        name : 'IntVal2_',
                        fieldLabel : '数值2'
                    },

                    {
                        xtype: 'datefield',
                        name : 'DateVal_',
                        fieldLabel : '日期类型'
                    },

                    {
                        xtype: 'yesnocombobox',
                        name : 'BooleanVal_',
                        fieldLabel : '布尔类型'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    tools: [{
        type:'refresh',
        tooltip: '清空查询条件',
        handler: 'onClean'
    }],

    buttons: [{
        text: '查询',
        iconCls : 'x-fa fa-search-plus',
        ui: 'soft-blue',
        handler: 'onSearch'
    }, {
        text: '取消',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
Ext.define('Admin.view.sys.generator.EditFormWinController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_generator_editformwincontroller',

    /*保存按钮事件*/
    onSave: function(){
        Ext.Msg.show({
            title:'提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.doSaveRecord,
            scope: this
        });
    },

    /*发送请求*/
    doSaveRecord: function(buttonId) {
        if(buttonId === "ok") {
            var me = this;
            var form = this.lookup("formpanel");
            form.submit({
                url: ServerUrl + '/SysGeneratorController/update',
                successHint: true,
                success: function() {
                    me.fireEvent('recordUpdated');
                    me.getView().close();
                }
            });
        }
    },

    /*根据id加载数据*/
    loadDataById: function (id) {
        var form = this.lookup("formpanel");
        form.load({
            url: ServerUrl + '/SysGeneratorController/selectById',
            method: 'get',
            params: {id: id}
        });
    },

    /*关闭窗口*/
    onCancel: function() {
        this.getView().close();
    }
});

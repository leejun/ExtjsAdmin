Ext.define('Admin.view.sys.generator.InsertFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_generator_insertformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_generator_insertformwincontroller',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [
                    {
                        xtype: 'textfield',
                        name : 'StringVal1_',
                        fieldLabel : '字符串1'
                    },

                    {
                        xtype: 'textfield',
                        name : 'StringVal2_',
                        fieldLabel : '字符串2'
                    },

                    {
                        xtype: 'sys_dic_sysdiccombobox',
                        sysDicTypeCode: 'DIC_SEX',
                        name : 'SysDicSexId_',
                        fieldLabel : '字典(性别)ID'
                    },

                    {
                        xtype: 'sys_dic_sysdictreepicker',
                        sysDicTypeCode: 'DIC_CITY',
                        name : 'SysDicCityId_',
                        fieldLabel : '字典(城市)ID'
                    },

                    {
                        xtype: 'numberfield',
                        name : 'IntVal1_',
                        fieldLabel : '数值1'
                    },

                    {
                        xtype: 'numberfield',
                        name : 'IntVal2_',
                        fieldLabel : '数值2'
                    },

                    {
                        xtype: 'datefield',
                        name : 'DateVal_',
                        fieldLabel : '日期类型'
                    },

                    {
                        xtype: 'yesnocombobox',
                        name : 'BooleanVal_',
                        fieldLabel : '布尔类型'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        xtype: 'savebutton',
        handler: 'onSave'
    }, {
        text: '取消',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
Ext.define('Admin.view.sys.generator.SearchFormWinController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_generator_searchformwincontroller',

    /*高级查询*/
    onSearch: function () {
        var searchParams = this.lookup("formpanel").getValues(false, true);
        this.fireEvent('searchPlus', searchParams);
        this.getView().close();
    },

    /*清空查询条件*/
    onClean: function () {
        this.lookup("formpanel").reset();
        this.fireEvent('searchPlus', {});
    },

    /*初始化上次的查询条件数据*/
    setFormData: function (formData) {
        this.lookup("formpanel").getForm().setValues(formData);
    },

    /*关闭窗口*/
    onCancel: function() {
        this.getView().close();
    }
});

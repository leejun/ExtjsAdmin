Ext.define('Admin.view.sys.generator.DetailFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_generator_detailformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_generator_detailformwincontroller',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
            xtype: 'baseformpanel',
            reference: 'formpanel',
                items: [
                    {
                        xtype: 'textfield',
                        name : 'StringVal1_',
                        readOnly: true,
                        fieldLabel : '字符串1'
                    },

                    {
                        xtype: 'textfield',
                        name : 'StringVal2_',
                        readOnly: true,
                        fieldLabel : '字符串2'
                    },

                    {
                        xtype: 'textfield',
                        name : 'SysDicSexId_',
                        readOnly: true,
                        fieldLabel : '字典(性别)ID'
                    },

                    {
                        xtype: 'textfield',
                        name : 'SysDicCityId_',
                        readOnly: true,
                        fieldLabel : '字典(城市)ID'
                    },

                    {
                        xtype: 'textfield',
                        name : 'IntVal1_',
                        readOnly: true,
                        fieldLabel : '数值1'
                    },

                    {
                        xtype: 'textfield',
                        name : 'IntVal2_',
                        readOnly: true,
                        fieldLabel : '数值2'
                    },

                    {
                        xtype: 'textfield',
                        name : 'DateVal_',
                        readOnly: true,
                        fieldLabel : '日期类型'
                    },

                    {
                        xtype: 'textfield',
                        name : 'BooleanVal_',
                        readOnly: true,
                        fieldLabel : '布尔类型'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        text: '关闭',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
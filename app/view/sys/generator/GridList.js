Ext.define('Admin.view.sys.generator.GridList', {
    extend : 'Admin.base.BaseGridPanel',
    xtype : 'sys_generator_gridlist',

    multiSelect: true,
    pagination: true,

    initComponent : function() {
        var me = this;
        Ext.applyIf(me, {
            store : Ext.create('Admin.store.sys.generator.SysGeneratorStore', {
                autoLoad : true
            }),
            columns : [
                {text:'主键PK', dataIndex:'Id_', hidden:true},
                {text:'字符串1', dataIndex:'StringVal1_', minWidth:100, flex : 1},
                {text:'字符串2', dataIndex:'StringVal2_', width:100},
                {text:'数值1', dataIndex:'IntVal1_', width:100},
                {text:'数值2', dataIndex:'IntVal2_', width:100},
                {text:'日期类型', dataIndex:'DateVal_', width:100},
                {text:'布尔类型', dataIndex:'BooleanVal_', width:100},
                {
                    xtype: 'actioncolumn',
                    dataIndex: 'bool',
                    text: '操作',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-pencil-alt',
                            tooltip: '编辑',
                            handler: 'onEdit'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-times',
                            tooltip: '删除',
                            handler: 'onDelete'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-file',
                            tooltip: '详细',
                            handler: 'onShow'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});
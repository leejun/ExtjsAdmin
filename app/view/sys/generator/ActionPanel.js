Ext.define('Admin.view.sys.generator.ActionPanel', {
	extend : 'Admin.base.BaseActionPanel',
	xtype : 'sys_generator_actionpanel',

	initComponent : function() {
		var me = this;

		Ext.applyIf(me, {
			items: {
				xtype: 'toolbar',
				items: [
					 
					{
						xtype : 'basesearchform',
						reference : 'searchForm',
						items : [ 
							{
								xtype: 'textfield',
								name : 'StringVal1_',
								fieldLabel : '字符串1'
							}, 
 
							{
								xtype: 'datefield',
								name : 'DateVal_',
								margin : '0 0 0 0',
								fieldLabel : '日期类型'
							} 
						]
					}, {
						iconCls : 'x-fa fa-times-circle',
						ui : 'soft-blue',
						handler : 'clearSearch'
					}, {
						iconCls : 'x-fa fa-search',
						ui : 'soft-blue',
						handler : 'searchRecord'
					}, {
						iconCls : 'x-fa fa-search-plus',
						ui : 'soft-blue',
						handler : 'searchPlusRecord'
					},
					{
						xtype: 'tbfill'
					}, {
						xtype: 'addbutton',
						handler: 'onAdd'
					}, '-', {
						xtype: 'delbutton',
						handler: 'onDel'
					}
				]
		    }
		});

		me.callParent(arguments);
	}
});
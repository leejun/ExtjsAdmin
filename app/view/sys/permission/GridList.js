Ext.define('Admin.view.sys.permission.GridList', {
    extend : 'Admin.base.BaseGridPanel',
    xtype : 'sys_permission_gridlist',

    multiSelect: true,
    pagination: true,

    initComponent : function() {
        var me = this;

        me.plugins = [ me.cellEditingPlugin = Ext.create('Ext.grid.plugin.CellEditing') ];

        Ext.applyIf(me, {
            store : Ext.create('Admin.store.sys.permission.SysPermissionStore', {
                autoLoad : true
            }),
            columns : [
                {text:'PK', dataIndex:'Id_', hidden:true},
                // {text:'Controller类名称', dataIndex:'ControllerName_', minWidth:100, flex : 1},
                // {text:'Controller方法名称', dataIndex:'MethodName_', width:100},
                {text:'请求路径', dataIndex:'UrlPattern_', minWidth:200, flex : 3},
                {text:'功能说明', dataIndex:'ActionName_', editor:'textfield', minWidth:140, flex: 2},
                {text:'请求方式', dataIndex:'HttpMethod_', width:100},
                {text:'请求参数类型', dataIndex:'ParametersType_', minWidth:120, flex: 1, renderer: Admin.base.BaseUtils.rendererQtip},
                {text:'代码已删除', dataIndex:'CodeIDelete_', align: 'center', width:100, renderer: Admin.base.BaseUtils.rendererBoolean},
                {text:'需要登录', dataIndex:'CheckLogin_', xtype: 'checkcolumn', width:80},
                {text:'需要授权', dataIndex:'CheckPermission_', xtype: 'checkcolumn', width:80},
                {text:'记录日志', dataIndex:'RequireLog_', xtype: 'checkcolumn', width:80},
                {text:'记录参数', dataIndex:'LogRequestParam_', xtype: 'checkcolumn', width:100},
                {text:'记录返回值', dataIndex:'LogReturnValue_', xtype: 'checkcolumn', width:100},
                //{text:'最后同步时间', dataIndex:'LastSyncTime_', width:100},
                {
                    width: 80,
                    xtype: 'actioncolumn',
                    dataIndex: 'bool',
                    text: '操作',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-times',
                            tooltip: '删除',
                            handler: 'onDelete'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-file',
                            tooltip: '详细',
                            handler: 'onShow'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});
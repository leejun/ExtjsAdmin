Ext.define('Admin.view.sys.permission.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_permission_mainviewcontroller',

    listen: {
        component: {
            'basesearchform[reference=searchForm] > textfield': {
                //antionPanel查询条件控件键盘事件
                specialkey: 'specialkeyHandler'
            },

            'basesearchform[reference=searchForm] > checkboxfield': {
                //多选框改变事件
                change: 'checkboxChangeHandler'
            }
        }
    },

    /*高级查询条件*/
    searchPlusParams: {},

    /*多选框改变事件-条件查询*/
    checkboxChangeHandler: function () {
        this.searchRecord();
    },

    /*与数据库同步*/
    onRefresh: function () {
        Ext.Msg.show({
            title:'提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.doRefresh,
            scope: this
        });
    },
    doRefresh: function (buttonId) {
        if(buttonId === "ok") {
            var me = this;
            Ext.Ajax.request({
                url: ServerUrl + '/SysPermissionController/codeSync',
                method: 'get',
                maskContainer: me.getView(),
                successHint: true,
                success: function() {
                    me.refreshGridList();
                }
            });
        }
    },

    /*保存数据*/
    onSave: function(){
        var me = this;
        if(me.getGridList().getStore().getModifiedRecords().length <= 0) {
            Ext.popup.Msg('提示信息', '没有修改过的数据');
            return;
        }
        Ext.Msg.show({
            title:'提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.doSaveRecord,
            scope: this
        });
    },
    doSaveRecord: function(buttonId) {
        if(buttonId === "ok") {
            var me = this;
            me.getGridList().setLoading("数据保存中...");
            me.getGridList().getStore().sync({
                success: function() {
                    Ext.popup.Msg('提示信息', '保存成功');
                    me.refreshGridList();
                },
                callback: function () {
                    me.getGridList().setLoading(false);
                }
            });
        }
    },

    getGridList: function () {
        return this.lookup('gridlist');
    },

    /*actionpanel-批量删除事件*/
    onDel: function () {
        var me = this;
        if (!me.lookup('gridlist').isSelected()) {
            Ext.popup.Msg('提示信息', '请选择一行记录!');
            return;
        }
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: me.deleteBatchRequest,
            scope: me
        });
    },
    deleteBatchRequest: function (buttonId) {
        if (buttonId === "ok") {
            var me = this;
            var records = this.lookup('gridlist').getSelectedRecords(),
                ids = [];
            Ext.each(records, function (item) {
                ids.push(item.getId());
            });
            ids = ids.join(",");
            Ext.Ajax.request({
                url: ServerUrl + '/SysPermissionController/deleteBatchById',
                method: 'POST',
                successHint: true,
                maskContainer: me.getView(),
                params: {
                    ids: ids
                },
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*gridlist-详细事件*/
    onShow: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        var win = Ext.create({
            xtype: 'sys_permission_detailformwin',
            title: '详细'
        });
        me.getView().add(win).show();
        win.getController().loadDataById(record.getId());
    },

    /*gridlist-删除事件*/
    onDelete: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            recordId: record.getId(),
            fn: me.deleteRequest,
            scope: me
        });
    },

    /*发送删除数据请求*/
    deleteRequest: function (buttonId, text, opt) {
        var me = this;
        if (buttonId === "ok") {
            console.log(opt.recordId);
            Ext.Ajax.request({
                url: ServerUrl + '/SysPermissionController/deleteById',
                //请求方式
                method: 'POST',
                //操作成功提示
                successHint: true,
                //网络请求时遮罩当前视图
                maskContainer: me.getView(),
                //请求参数
                params: {id: opt.recordId},
                //成功回调
                success: function () {
                    me.refreshGridList()
                }
            });
        }
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function(field, e) {
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, true);
        Ext.apply(requestParams, this.searchPlusParams);
        this.lookup('gridlist').search(requestParams);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchPlusParams = {};
        this.searchRecord();
    }
});

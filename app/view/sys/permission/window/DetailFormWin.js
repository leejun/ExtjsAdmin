Ext.define('Admin.view.sys.permission.DetailFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_permission_detailformwin',

    /*视图控制器 定义事件*/
    controller: 'sys_permission_detailformwincontroller',
    height: 450,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
            xtype: 'baseformpanel',
            reference: 'formpanel',
                items: [
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name : 'ControllerName_',
                            readOnly: true,
                            fieldLabel : '类名称'
                        }, {
                            xtype: 'textfield',
                            name : 'MethodName_',
                            readOnly: true,
                            fieldLabel : '方法名称'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name : 'ActionName_',
                            readOnly: true,
                            fieldLabel : '功能说明'
                        }, {
                            xtype: 'textfield',
                            name : 'HttpMethod_',
                            readOnly: true,
                            fieldLabel : '请求方式'
                        }]
                    },
                    {
                        xtype: 'textfield',
                        name : 'UrlPattern_',
                        readOnly: true,
                        fieldLabel : '请求路径'
                    },
                    {
                        xtype: 'textfield',
                        name : 'ParametersType_',
                        readOnly: true,
                        fieldLabel : '参数类型'
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name : 'CodeIDelete_',
                            readOnly: true,
                            fieldLabel : '代码已删除'
                        }, {
                            xtype: 'textfield',
                            name : 'CheckLogin_',
                            readOnly: true,
                            fieldLabel : '需要登录'
                        }]
                    },

                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name : 'CheckPermission_',
                            readOnly: true,
                            fieldLabel : '需要授权'
                        }, {
                            xtype: 'textfield',
                            name : 'RequireLog_',
                            readOnly: true,
                            fieldLabel : '记录日志'
                        }]
                    },

                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name : 'LogRequestParam_',
                            readOnly: true,
                            fieldLabel : '记录请求参数'
                        }, {
                            xtype: 'textfield',
                            name : 'LogReturnValue_',
                            readOnly: true,
                            fieldLabel : '记录返回值'
                        }]
                    },
                    {
                        xtype: 'textfield',
                        name : 'LastSyncTime_',
                        readOnly: true,
                        fieldLabel : '最后同步时间'
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        text: '关闭',
        iconCls: 'x-fa fa-times-circle',
        ui: 'soft-red',
        handler: 'onCancel'
    }]

});
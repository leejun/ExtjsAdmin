Ext.define('Admin.view.sys.permission.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_permission_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_permission_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_permission_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'sys_permission_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
Ext.define('Admin.view.sys.permission.ActionPanel', {
	extend : 'Admin.base.BaseActionPanel',
	xtype : 'sys_permission_actionpanel',

	initComponent : function() {
		var me = this;

		Ext.applyIf(me, {
			items: {
				xtype: 'toolbar',
				items: [
					 
					{
						xtype : 'basesearchform',
						reference : 'searchForm',
						items : [ 
							{
								xtype: 'textfield',
								name : 'QueryParam_',
								emptyText: '请求路径|功能说明',
								fieldLabel : '查询条件'
							},
							{
								xtype: 'checkboxfield',
								boxLabel  : '需要登录',
								name      : 'CheckLogin_',
								inputValue: '1'
							},
							{
								xtype: 'checkboxfield',
								boxLabel  : '需要权限',
								name      : 'CheckPermission_',
								inputValue: '1'
							},
							{
								xtype: 'checkboxfield',
								boxLabel  : '记录日志',
								margin : '0 0 0 0',
								name      : 'RequireLog_',
								inputValue: '1'
							}
						]
					}, {
						iconCls : 'x-fa fa-times-circle',
						ui : 'soft-blue',
						handler : 'clearSearch'
					}, {
						iconCls : 'x-fa fa-search',
						ui : 'soft-blue',
						handler : 'searchRecord'
					},
					{
						xtype: 'tbfill'
					}, {
						xtype: 'savebutton',
						handler: 'onSave'
					}, '-',
					{
						xtype: 'refreshbutton',
						tooltip: '全部Controller同步到数据库',
						handler: 'onRefresh'
					}, '-',
					{
						xtype: 'delbutton',
						handler: 'onDel'
					}
				]
		    }
		});

		me.callParent(arguments);
	}
});
Ext.define('Admin.view.sys.task.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'sys_task_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'basesearchform',
                        reference: 'searchForm',
                        items: [
                            {
                                xtype: 'textfield',
                                name: 'queryParam',
                                emptyText: '任务说明|任务ID',
                                margin: '0 0 0 0',
                                fieldLabel: '查询条件'
                            }
                        ]
                    }, {
                        iconCls: 'x-fa fa-times-circle',
                        ui: 'soft-blue',
                        handler: 'clearSearch'
                    }, {
                        iconCls: 'x-fa fa-search',
                        ui: 'soft-blue',
                        handler: 'searchRecord'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
Ext.define('Admin.view.sys.task.EditFormWin', {
    extend: 'Admin.base.BaseFormWindow',
    xtype: 'sys_task_editformwin',

    controller: 'sys_task_editformwincontroller',
    height: 330,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            items: [{
                xtype: 'baseformpanel',
                reference: 'formpanel',
                items: [
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name: 'jobDescription',
                            readOnly: true,
                            fieldLabel: '任务说明'
                        }, {
                            xtype: 'textfield',
                            name: 'triggerState',
                            readOnly: true,
                            fieldLabel: '任务状态'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name: 'jobName',
                            readOnly: true,
                            fieldLabel: '任务ID'
                        }, {
                            xtype: 'textfield',
                            name: 'jobGroupName',
                            readOnly: true,
                            fieldLabel: '任务组名称'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name: 'triggerName',
                            readOnly: true,
                            fieldLabel: '触发器ID'
                        }, {
                            xtype: 'textfield',
                            name: 'triggerGroupName',
                            readOnly: true,
                            fieldLabel: '触发器组名称'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name: 'cronExpression',
                            fieldLabel: '执行表达式'
                        }, {
                            xtype: 'textfield',
                            name: 'startTime',
                            readOnly: true,
                            fieldLabel: '任务开始时间'
                        }]
                    },
                    {
                        xtype: 'basehboxfieldcontainer',
                        items: [{
                            xtype: 'textfield',
                            name: 'previousFireTime',
                            readOnly: true,
                            fieldLabel: '上次运行时间'
                        }, {
                            xtype: 'textfield',
                            name: 'nextFireTime',
                            readOnly: true,
                            fieldLabel: '下次运行时间'
                        }]
                    }
                ]
            }]
        });
        me.callParent(arguments);
    },

    buttons: [{
        xtype: 'savebutton',
        handler: 'onSave'
    }, {
        xtype: 'cancelbutton',
        handler: 'onCancel'
    }]

});
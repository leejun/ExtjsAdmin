Ext.define('Admin.view.sys.task.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_task_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_task_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_task_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'sys_task_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
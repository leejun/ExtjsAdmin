Ext.define('Admin.view.sys.task.GridList', {
    extend: 'Admin.base.BaseGridPanel',
    xtype: 'sys_task_gridlist',

    pagination: false,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Admin.store.sys.task.SysTaskStore', {
                autoLoad: true
            }),
            columns: [
                {text: '任务说明', dataIndex: 'jobDescription', minWidth: 80, flex: 1},
                {text: '任务类型', dataIndex: 'jobClassName', minWidth: 80, flex: 1},
                {text: '任务ID', dataIndex: 'jobName', minWidth: 80, flex: 1},
                //{text: '任务组名称', dataIndex: 'jobGroupName', width: 100},
                {text: '任务状态', dataIndex: 'triggerState', width: 100, renderer: Admin.base.BaseUtils.rendererValue},
                {text: '执行表达式', dataIndex: 'cronExpression', width: 140},
                {text: '触发器ID', dataIndex: 'triggerName', width: 120},
                // {text:'触发器组', dataIndex:'triggerGroupName', width: 120},
                {text: '任务开始时间', dataIndex: 'startTime', width: 150},
                {text: '上次运行时间', dataIndex: 'previousFireTime', width: 150},
                {text: '下次运行时间', dataIndex: 'nextFireTime', width: 150},
                {
                    xtype: 'actioncolumn',
                    dataIndex: 'bool',
                    text: '操作',
                    items: [
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-pencil-alt',
                            tooltip: '编辑',
                            handler: 'onEdit'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-times',
                            tooltip: '删除',
                            handler: 'onDelete'
                        },
                        {
                            xtype: 'button',
                            iconCls: 'x-fa fa-ban',
                            tooltip: '启用|禁用',
                            handler: 'onStop'
                        }
                    ]
                }
            ]
        });
        me.callParent(arguments);
    }
});
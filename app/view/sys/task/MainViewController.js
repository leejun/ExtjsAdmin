Ext.define('Admin.view.sys.task.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_task_mainviewcontroller',

    listen: {
        component: {
            'basesearchform[reference=searchForm] > textfield': {
                //antionPanel查询条件控件键盘事件
                specialkey: 'specialkeyHandler'
            }
        },

        controller: {
            'sys_task_editformwincontroller': {
                //监听数据编辑事件(刷新表格)
                recordUpdated: 'onRecordUpdated'
            }
        }
    },

    /*表单编辑事件*/
    onRecordUpdated: function () {
        this.searchRecord();
    },

    /*gridlist-详细事件*/
    onEdit: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        var win = Ext.create({
            xtype: 'sys_task_editformwin',
            title: '编辑'
        });
        win.show();
        win.getController().loadRecord(record);
    },

    /*gridlist-删除事件*/
    onDelete: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            jobName: record.get('jobName'),
            jobGroupName: record.get('jobGroupName'),
            fn: me.deleteRequest,
            scope: me
        });
    },

    /*发送删除数据请求*/
    deleteRequest: function (buttonId, text, opt) {
        var me = this;
        if (buttonId === "ok") {
            Ext.Ajax.request({
                url: ServerUrl + '/QuartzController/deleteJob',
                method: 'POST',
                successHint: true,
                maskContainer: me.getView(),
                params: {jobName: opt.jobName, jobGroupName: opt.jobGroupName},
                success: function () {
                    me.refreshGridList();
                }
            });
        }
    },

    /*禁用 启动节点*/
    onStop: function (view, rowIndex, colIndex, item, e, record, row) {
        var me = this;
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: function (buttonId) {
                if (buttonId === "ok") {
                    var requestUrl;
                    var jobName = record.get('jobName');
                    var jobGroupName = record.get('jobGroupName');
                    if(record.get('triggerState') === 'NORMAL') {
                        requestUrl = ServerUrl + '/QuartzController/pauseJob';
                    } else {
                        requestUrl = ServerUrl + '/QuartzController/resumeJob';
                    }
                    Ext.Ajax.request({
                        url: requestUrl,
                        successHint: true,
                        maskContainer: me.getView(),
                        params: {jobName: jobName, jobGroupName: jobGroupName},
                        success: function () {
                            me.refreshGridList();
                        }
                    });
                }
            },
            scope: me
        });
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function (field, e) {
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, true);
        this.lookup('gridlist').search(requestParams);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchRecord();
    }
});

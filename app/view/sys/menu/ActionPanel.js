Ext.define('Admin.view.sys.menu.ActionPanel', {
	extend : 'Ext.container.Container',
	xtype : 'sys_menu_actionpanel',

	frame : true,
	margin: '0 0 5 0',
		
	initComponent : function() {
		var me = this;

		Ext.applyIf(me, {
			items: [{
	    		xtype: 'toolbar',
				items: [{
					xtype: 'tbfill'
				}, {
					xtype: 'addbutton',
					handler: 'onAdd'
				}, '-', {
					xtype: 'savebutton',
					handler: 'onSave'
				},  '-', {
					xtype: 'refreshbutton',
					handler: 'onRefresh'
				}]
		    }]
		});

		me.callParent(arguments);
	}
});
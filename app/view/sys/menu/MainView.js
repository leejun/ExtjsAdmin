Ext.define('Admin.view.sys.menu.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_menu_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_menu_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_menu_actionpanel'
            }, {
                xtype: 'sys_menu_treegridlist',
                reference: 'treeGrid',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
Ext.define('Admin.view.sys.menu.TreeGridList', {
    extend: 'Admin.base.BaseTreePanel',
    xtype: 'sys_menu_treegridlist',

    //是否隐藏roo节点  false:隐藏  true:显示
    rootVisible: true,

    //useArrows: true,

    initComponent: function () {
        var me = this;

        me.plugins = [me.cellEditingPlugin = Ext.create('Ext.grid.plugin.CellEditing')];

        Ext.apply(me, {
            store: Ext.create('Admin.store.sys.menu.SysMenuStore'),
            viewConfig: {
                loadingText: "数据读取中...",
                plugins: {
                    ptype: 'treeviewdragdrop',
                    containerScroll: true
                }
            },
            columns: [{
                xtype: 'treecolumn',
                sortable: false,
                text: '菜单名称',
                dataIndex: 'text',
                flex: 2,
                minWidth: 100,
                editor: {
                    xtype: 'textfield',
                    selectOnFocus: true,
                    allowOnlyWhitespace: false
                }
            }, {
                sortable: false,
                text: '排序',
                dataIndex: 'index',
                align: 'center',
                width: 70
            }, {
                sortable: false,
                text: '类型',
                dataIndex: 'leaf',
                width: 70,
                renderer: function (value) {
                    return value ? '菜单' : '目录';
                }
            }, {
                sortable: false,
                text: '状态',
                dataIndex: 'stop',
                width: 70,
                renderer: Admin.base.BaseUtils.rendererStop
            }, {
                sortable: false,
                xtype: 'checkcolumn',
                text: '展开',
                dataIndex: 'expandedEdit',
                stopSelection: false,
                menuDisabled: true,
                width: 70,
                listeners: {
                    beforecheckchange: function (ck, rowIndex, checked, eOpts) {
                        return (rowIndex !== 0);
                    }
                }
            }, {
                sortable: false,
                text: '左侧图标',
                dataIndex: 'iconCls',
                editor: 'textfield',
                flex: 1,
                minWidth: 100
            }, {
                sortable: false,
                text: '模块视图',
                dataIndex: 'viewType',
                editor: 'textfield',
                flex: 2,
                minWidth: 100
            }, {
                xtype: 'actioncolumn',
                items: [
                    {
                        xtype: 'button',
                        iconCls: 'x-fa fa-pencil-alt',
                        tooltip: '编辑',
                        handler: 'onEdit'
                    },
                    {
                        xtype: 'button',
                        iconCls: 'x-fa fa-times',
                        tooltip: '删除',
                        handler: 'onDelete'
                    },
                    {
                        xtype: 'button',
                        iconCls: 'x-fa fa-ban',
                        tooltip: '启用|禁用',
                        handler: 'onStop'
                    }
                ],
                dataIndex: 'bool',
                text: '操作'
            }]
        });

        me.callParent(arguments);
        me.relayEvents(me.getStore(), ['load'], 'store');
        me.on('beforeedit', me.handleBeforeEdit, me);
    },

    handleBeforeEdit: function (editingPlugin, e) {
        if (e.record.get('id') === '0') {
            Ext.popup.Msg('提示信息', '根节点不允许编辑');
            return false;
        }
        return true;
    }
});
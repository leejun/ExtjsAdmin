Ext.define('Admin.view.sys.logback.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'sys_logback_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [
                    {
                        xtype: 'tbfill'
                    }, {
                        xtype: 'delbutton',
                        text: '清空',
                        handler: 'clearStore'
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
Ext.define('Admin.view.sys.logback.LogDataModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.sys_logback_logdatamodel',

    data: {
        logs: [{
            logdata: '开始连接服务器查询日志信息....'
        }]
    },

    stores: {
        logStore: {
            fields: ['logdata'],
            data: '{logs}'
        }
    }
});

Ext.define('Admin.view.sys.logback.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_logback_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_logback_mainviewcontroller',

    viewModel: 'sys_logback_logdatamodel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_logback_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'sys_logback_logdataview',
                reference: 'dataview',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
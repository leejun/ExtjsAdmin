Ext.define('Admin.view.sys.logback.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sys_logback_mainviewcontroller',

    listen: {
        controller: {
            'main_maincontroller': {
                onSystemLogUpdate: 'onSystemLogUpdate'
            }
        }
    },

    onSystemLogUpdate: function (data) {
        var store = this.lookup('dataview').store;
        if (store.getCount() >= 350) {
            store.removeAt(0, 50);
        }
        store.add({logdata: data});
    },

    clearStore: function () {
        this.lookup('dataview').store.removeAll();
    }
});

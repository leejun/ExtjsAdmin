Ext.define('Admin.view.sys.logback.LogDataView', {
    extend: 'Ext.view.View',
    xtype: 'sys_logback_logdataview',

    bind: {
        store: '{logStore}'
    },

    style: "background-color:white;",

    scrollable: 'y',

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            tpl: [
                '<tpl for=".">',
                '<div style="margin-bottom: 10px;" class="thumb-wrap">',
                '<span>{logdata}</span><br/>',
                '</div>',
                '</tpl>'
            ],
            itemSelector: 'div.thumb-wrap'
        });

        me.callParent(arguments);
    }

});
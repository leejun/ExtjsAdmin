Ext.define('Admin.view.sys.dictype.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'sys_dictype_mainview',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    controller: 'sys_dictype_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'sys_dictype_actionpanel'
            }, {
                xtype: 'sys_dictype_treegridlist',
                reference: 'treeGrid',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
Ext.define('Admin.view.sys.dictype.SysDicTypeTreePicker', {
    extend: 'Ext.ux.TreePicker',
    alias : 'widget.sys_dictype_sysdictypetreepicker',

    selectChildren: true,
    rootVisible : false,
    displayField : 'text',
    valueField: 'id',

    initComponent : function() {
        var me = this;
        Ext.apply(me, {
            store : Ext.create('Admin.store.sys.dictype.SysDicTypeStore'),
            viewConfig: {
                loadingText: "数据读取中..."
            },
            columns: [{
                xtype: 'treecolumn',
                sortable: false,
                text: '类别名称',
                dataIndex: 'text',
                flex: 1,
                minWidth: 200
            }, {
                sortable: false,
                text: '类别编码',
                dataIndex: 'code',
                editor: 'textfield',
                width: 200
            }]
        });
        me.callParent(arguments);
    },

    setValue: function(value) {
        var me = this;

        // if(value) {
        //     var node = me.store.getNodeById(value);
        //     if(!node.isLeaf()) {
        //         Ext.popup.Msg("提示信息", "不允许选择目录节点");
        //         return;
        //     }
        // }

        me.callParent(arguments);
    }
});
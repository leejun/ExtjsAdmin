Ext.define("Admin.view.sys.dictype.SysDicTypeComboBox", {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.sys_dictype_sysdictypecombobox',

    //是否默认选择第一项
    defaultFirst: false,

    editable: false,

    displayField: 'text',
    valueField: 'code',

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Ext.data.Store', {
                fields: ['code', 'text'],
                autoLoad: true,
                proxy: {
                    type: 'ajax',
                    url: ServerUrl + '/SysDictypeController/selectNoRootList',
                    reader: {
                        type: 'json',
                        rootProperty: 'data',
                        successProperty: 'success'
                    }
                }
            }),
            queryMode: 'local'
        });

        me.store.on('load', function (store, records, successful) {
            if (records && me.defaultFirst) {
                me.setValue(records[0].get(me.valueField));
            }
        });

        me.callParent(arguments);
    }

});
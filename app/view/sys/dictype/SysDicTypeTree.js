Ext.define('Admin.view.sys.dictype.SysDicTypeTree', {
    extend: 'Admin.base.BaseTreePanel',
    xtype: 'sys_dictype_sysdictypetree',

    //是否隐藏roo节点  false:隐藏  true:显示
    rootVisible: false,

    tools:[{
        type:'refresh',
        tooltip: '刷新',
        handler: function(event, toolEl, panelHeader) {
            this.up('sys_dictype_sysdictypetree').refreshView();
        }
    }],

    initComponent: function () {
        var me = this;
        Ext.apply(me, {
            viewConfig: {
                loadingText: "数据读取中..."
            },
            store: Ext.create('Admin.store.sys.dictype.SysDicTypeStore')
        });
        me.callParent(arguments);
    }
});
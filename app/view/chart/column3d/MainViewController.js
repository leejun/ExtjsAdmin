Ext.define('Admin.view.chart.column3d.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_column3d_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=column3d]': {
                successfullyRenderd: 'column3dSuccessfullyRenderd'
            }
        }
    },

    column3dSuccessfullyRenderd: function () {
        var me = this;
        var column3d = me.lookup('column3d');
        //延迟0.2秒设置数据 模拟网络加载
        Ext.Function.defer(function () {
            column3d.setDataSource({
                chart: {
                    caption: "Countries with Highest Deforestation Rate",
                    subcaption: "For the year 2017",
                    yaxisname: "Deforested Area{br}(in Hectares)",
                    decimals: "1",
                    theme: "fusion"
                },
                data: [
                    {
                        label: "Brazil",
                        value: "146600",
                        color: 'AFD8F8'
                    },
                    {
                        label: "Indonesia",
                        value: "114780",
                        color: 'F6BD0F'
                    },
                    {
                        label: "Russian Federation",
                        value: "532200",
                        color: '8BBA00'
                    },
                    {
                        label: "Mexico",
                        value: "395000",
                        color: 'FF8E46'
                    },
                    {
                        label: "Papua New Guinea",
                        value: "250200",
                        color: '008E8E'
                    },
                    {
                        label: "Peru",
                        value: "224600",
                        color: 'D64646'
                    },
                    {
                        label: "U.S.A",
                        value: "215200",
                        color: '588526'
                    },
                    {
                        label: "Bolivia",
                        value: "195200",
                        color: '008ED6'
                    },
                    {
                        label: "Sudan",
                        value: "217807",
                        color: 'A186BE'
                    },
                    {
                        label: "Nigeria",
                        value: "220000",
                        color: '9D080D'
                    }
                ]
            });
        }, 200);
    }
});

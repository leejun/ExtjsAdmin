Ext.define('Admin.view.chart.column3d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_column3d_mainview',

    layout: 'fit',

    controller: 'chart_column3d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "column3d",
                reference: 'column3d'
            }]
        });
        me.callParent(arguments);
    }

});
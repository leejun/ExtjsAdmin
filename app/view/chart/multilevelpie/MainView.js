Ext.define('Admin.view.chart.multilevelpie.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_multilevelpie_mainview',

    layout: 'fit',

    controller: 'chart_multilevelpie_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "multilevelpie",
                reference: 'multilevelpie'
            }]
        });
        me.callParent(arguments);
    }

});
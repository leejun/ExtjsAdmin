Ext.define('Admin.view.chart.msline.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_msline_mainview',

    layout: 'fit',

    controller: 'chart_msline_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "msline",
                reference: 'msline'
            }]
        });
        me.callParent(arguments);
    }

});
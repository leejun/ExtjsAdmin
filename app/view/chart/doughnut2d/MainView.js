Ext.define('Admin.view.chart.doughnut2d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_doughnut2d_mainview',

    layout: 'fit',

    controller: 'chart_doughnut2d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "doughnut2d",
                reference: 'doughnut2d'
            }]
        });
        me.callParent(arguments);
    }

});
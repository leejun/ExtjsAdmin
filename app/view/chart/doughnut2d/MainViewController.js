Ext.define('Admin.view.chart.doughnut2d.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_doughnut2d_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=doughnut2d]': {
                successfullyRenderd: 'doughnut2dSuccessfullyRenderd'
            }
        }
    },

    doughnut2dSuccessfullyRenderd:function () {
        var me = this;
        var doughnut2d = me.lookup('doughnut2d');
        Ext.Ajax.request({
            url: ServerUrl + '/FusionchartsApi/doughnut2d',
            method: 'GET',
            success: function (response, options) {
                var result = Ext.JSON.decode(response.responseText);
                if (result.success) {
                    doughnut2d.setDataSource(result.data);
                }
            }
        });
    }
});

Ext.define('Admin.view.chart.stackedcolumn2d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_stackedcolumn2d_mainview',

    layout: 'fit',

    controller: 'chart_stackedcolumn2d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "stackedcolumn2d",
                reference: 'stackedcolumn2d'
            }]
        });
        me.callParent(arguments);
    }

});
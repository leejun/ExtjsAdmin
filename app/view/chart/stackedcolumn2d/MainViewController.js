Ext.define('Admin.view.chart.stackedcolumn2d.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_stackedcolumn2d_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=stackedcolumn2d]': {
                successfullyRenderd: 'stackedcolumn2dSuccessfullyRenderd'
            }
        }
    },

    stackedcolumn2dSuccessfullyRenderd: function () {
        var me = this;
        var stackedcolumn2d = me.lookup('stackedcolumn2d');
        //延迟0.2秒设置数据 模拟网络加载
        Ext.Function.defer(function () {
            stackedcolumn2d.setDataSource({
                chart: {
                    caption: "Yearly Energy Production Rate",
                    subcaption: " Top 5 Developed Countries",
                    numbersuffix: " TWh",
                    showsum: "1",
                    plottooltext:
                        "$label produces <b>$dataValue</b> of energy from $seriesName",
                    theme: "fusion",
                    drawcrossline: "1"
                },
                categories: [
                    {
                        category: [
                            {
                                label: "Canada"
                            },
                            {
                                label: "China"
                            },
                            {
                                label: "Russia"
                            },
                            {
                                label: "Australia"
                            },
                            {
                                label: "United States"
                            },
                            {
                                label: "France"
                            }
                        ]
                    }
                ],
                dataset: [
                    {
                        seriesname: "Coal",
                        data: [
                            {
                                value: "400"
                            },
                            {
                                value: "830"
                            },
                            {
                                value: "500"
                            },
                            {
                                value: "420"
                            },
                            {
                                value: "790"
                            },
                            {
                                value: "380"
                            }
                        ]
                    },
                    {
                        seriesname: "Hydro",
                        data: [
                            {
                                value: "350"
                            },
                            {
                                value: "620"
                            },
                            {
                                value: "410"
                            },
                            {
                                value: "370"
                            },
                            {
                                value: "720"
                            },
                            {
                                value: "310"
                            }
                        ]
                    },
                    {
                        seriesname: "Nuclear",
                        data: [
                            {
                                value: "210"
                            },
                            {
                                value: "400"
                            },
                            {
                                value: "450"
                            },
                            {
                                value: "180"
                            },
                            {
                                value: "570"
                            },
                            {
                                value: "270"
                            }
                        ]
                    },
                    {
                        seriesname: "Gas",
                        data: [
                            {
                                value: "180"
                            },
                            {
                                value: "330"
                            },
                            {
                                value: "230"
                            },
                            {
                                value: "160"
                            },
                            {
                                value: "440"
                            },
                            {
                                value: "350"
                            }
                        ]
                    },
                    {
                        seriesname: "Oil",
                        data: [
                            {
                                value: "60"
                            },
                            {
                                value: "200"
                            },
                            {
                                value: "200"
                            },
                            {
                                value: "50"
                            },
                            {
                                value: "230"
                            },
                            {
                                value: "150"
                            }
                        ]
                    }
                ]
            });
        }, 200);
    }
});

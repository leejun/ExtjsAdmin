Ext.define('Admin.view.chart.stackedarea2dlinedy.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_stackedarea2dlinedy_mainview',

    layout: 'fit',

    controller: 'chart_stackedarea2dlinedy_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "stackedarea2dlinedy",
                reference: 'stackedarea2dlinedy'
            }]
        });
        me.callParent(arguments);
    }

});
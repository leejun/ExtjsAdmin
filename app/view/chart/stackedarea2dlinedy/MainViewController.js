Ext.define('Admin.view.chart.stackedarea2dlinedy.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_stackedarea2dlinedy_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=stackedarea2dlinedy]': {
                successfullyRenderd: 'stackedarea2dlinedySuccessfullyRenderd'
            }
        }
    },

    stackedarea2dlinedySuccessfullyRenderd: function () {
        var me = this;
        var stackedarea2dlinedy = me.lookup('stackedarea2dlinedy');
        //延迟0.2秒设置数据 模拟网络加载
        Ext.Function.defer(function () {
            stackedarea2dlinedy.setDataSource({
                chart: {
                    caption: "Energy Data for consumption vs. Price ",
                    subcaption: "2010-2014",
                    xaxisname: "Year",
                    pyaxisname: "Total consumption-in billion BTU",
                    syaxisname: "Price-in USD per million BTU",
                    snumbersuffix: "M",
                    theme: "fusion"
                },
                categories: [
                    {
                        category: [
                            {
                                label: "2010"
                            },
                            {
                                label: "2011"
                            },
                            {
                                label: "2012"
                            },
                            {
                                label: "2013"
                            },
                            {
                                label: "2014"
                            }
                        ]
                    }
                ],
                dataset: [
                    {
                        seriesname: "Coal",
                        data: [
                            {
                                value: "41656762"
                            },
                            {
                                value: "39327714"
                            },
                            {
                                value: "34761848"
                            },
                            {
                                value: "36077545"
                            },
                            {
                                value: "35993151"
                            }
                        ]
                    },
                    {
                        seriesname: "Electricity",
                        data: [
                            {
                                value: "25623035"
                            },
                            {
                                value: "25588952"
                            },
                            {
                                value: "25212289"
                            },
                            {
                                value: "25418495"
                            },
                            {
                                value: "25690310"
                            }
                        ]
                    },
                    {
                        seriesname: "Natural Gas",
                        data: [
                            {
                                value: "49267013"
                            },
                            {
                                value: "50029967"
                            },
                            {
                                value: "52276699"
                            },
                            {
                                value: "53716269"
                            },
                            {
                                value: "55026395"
                            }
                        ]
                    },
                    {
                        seriesname: "LPG",
                        data: [
                            {
                                value: "5641972"
                            },
                            {
                                value: "5677909"
                            },
                            {
                                value: "5823618"
                            },
                            {
                                value: "6333475"
                            },
                            {
                                value: "6180102"
                            }
                        ]
                    },
                    {
                        seriesname: "Total Price",
                        renderas: "line",
                        showanchors: "1",
                        parentyaxis: "S",
                        showvalues: "0",
                        data: [
                            {
                                value: "3386.97"
                            },
                            {
                                value: "3571.81‬"
                            },
                            {
                                value: "5370.72‬"
                            },
                            {
                                value: "5658.28‬"
                            },
                            {
                                value: "5974.85‬‬"
                            }
                        ]
                    }
                ]
            });
        }, 200);
    }
});

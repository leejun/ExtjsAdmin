Ext.define('Admin.view.chart.zoomline.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_zoomline_mainview',

    layout: 'fit',

    controller: 'chart_zoomline_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "zoomline",
                reference: 'zoomline'
            }]
        });
        me.callParent(arguments);
    }

});
Ext.define('Admin.view.chart.stackedbar2d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_stackedbar2d_mainview',

    layout: 'fit',

    controller: 'chart_stackedbar2d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "stackedbar2d",
                reference: 'stackedbar2d'
            }]
        });
        me.callParent(arguments);
    }

});
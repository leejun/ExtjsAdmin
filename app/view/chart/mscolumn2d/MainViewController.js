Ext.define('Admin.view.chart.mscolumn2d.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_mscolumn2d_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=mscolumn2d]': {
                successfullyRenderd: 'mscolumn2dSuccessfullyRenderd'
            }
        }
    },

    mscolumn2dSuccessfullyRenderd: function () {
        var me = this;
        var mscolumn2d = me.lookup('mscolumn2d');
        //延迟0.2秒设置数据 模拟网络加载
        Ext.Function.defer(function () {
            mscolumn2d.setDataSource({
                chart: {
                    caption: "App Publishing Trend",
                    subcaption: "2012-2016",
                    xaxisname: "Years",
                    yaxisname: "Total number of apps in store",
                    formatnumberscale: "1",
                    plottooltext:
                        "<b>$dataValue</b> apps were available on <b>$seriesName</b> in $label",
                    theme: "fusion",
                    drawcrossline: "1"
                },
                categories: [
                    {
                        category: [
                            {
                                label: "2012"
                            },
                            {
                                label: "2013"
                            },
                            {
                                label: "2014"
                            },
                            {
                                label: "2015"
                            },
                            {
                                label: "2016"
                            }
                        ]
                    }
                ],
                dataset: [
                    {
                        seriesname: "iOS App Store",
                        data: [
                            {
                                value: "125000"
                            },
                            {
                                value: "300000"
                            },
                            {
                                value: "480000"
                            },
                            {
                                value: "800000"
                            },
                            {
                                value: "1100000"
                            }
                        ]
                    },
                    {
                        seriesname: "Google Play Store",
                        data: [
                            {
                                value: "70000"
                            },
                            {
                                value: "150000"
                            },
                            {
                                value: "350000"
                            },
                            {
                                value: "600000"
                            },
                            {
                                value: "1400000"
                            }
                        ]
                    },
                    {
                        seriesname: "Amazon AppStore",
                        data: [
                            {
                                value: "10000"
                            },
                            {
                                value: "100000"
                            },
                            {
                                value: "300000"
                            },
                            {
                                value: "600000"
                            },
                            {
                                value: "900000"
                            }
                        ]
                    }
                ]
            });
        }, 200);
    }
});

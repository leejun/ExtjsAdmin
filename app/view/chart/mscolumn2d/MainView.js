Ext.define('Admin.view.chart.mscolumn2d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_mscolumn2d_mainview',

    layout: 'fit',

    controller: 'chart_mscolumn2d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "mscolumn2d",
                reference: 'mscolumn2d'
            }]
        });
        me.callParent(arguments);
    }

});
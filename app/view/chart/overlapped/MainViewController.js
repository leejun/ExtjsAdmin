Ext.define('Admin.view.chart.overlapped.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_overlapped_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=overlapped]': {
                successfullyRenderd: 'overlappedSuccessfullyRenderd'
            }
        }
    },

    overlappedSuccessfullyRenderd: function () {
        var me = this;
        var overlapped = me.lookup('overlapped');
        //延迟0.2秒设置数据 模拟网络加载
        Ext.Function.defer(function () {
            overlapped.setDataSource({
                chart: {
                    caption: "Sales Targets vs Achieved",
                    subcaption: "Bilbus",
                    yaxisname: "Revenue (In USD)",
                    numberprefix: "$",
                    drawcrossline: "1",
                    theme: "fusion",
                    showvalues: "0"
                },
                categories: [
                    {
                        category: [
                            {
                                label: "Oliver"
                            },
                            {
                                label: "Andy"
                            },
                            {
                                label: "Peter"
                            },
                            {
                                label: "Natasha"
                            },
                            {
                                label: "Robert"
                            },
                            {
                                label: "Bruce"
                            },
                            {
                                label: "Wanda"
                            }
                        ]
                    }
                ],
                dataset: [
                    {
                        seriesname: "Target",
                        data: [
                            {
                                value: "250000"
                            },
                            {
                                value: "200000"
                            },
                            {
                                value: "300000"
                            },
                            {
                                value: "200000"
                            },
                            {
                                value: "270000"
                            },
                            {
                                value: "350000"
                            },
                            {
                                value: "200000"
                            }
                        ]
                    },
                    {
                        seriesname: "Achieved",
                        data: [
                            {
                                value: "260000"
                            },
                            {
                                value: "180000"
                            },
                            {
                                value: "290000"
                            },
                            {
                                value: "195000"
                            },
                            {
                                value: "300000"
                            },
                            {
                                value: "380000"
                            },
                            {
                                value: "210000"
                            }
                        ]
                    }
                ]
            });
        }, 200);
    }
});

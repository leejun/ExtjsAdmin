Ext.define('Admin.view.chart.overlapped.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_overlapped_mainview',

    layout: 'fit',

    controller: 'chart_overlapped_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "overlappedcolumn2d",
                reference: 'overlapped'
            }]
        });
        me.callParent(arguments);
    }

});
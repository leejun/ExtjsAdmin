Ext.define('Admin.view.chart.area2d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_area2d_mainview',

    layout: 'fit',

    controller: 'chart_area2d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "area2d",
                reference: 'area2d'
            }]
        });
        me.callParent(arguments);
    }

});
Ext.define('Admin.view.chart.msstackedcolumn2dlinedy.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_msstackedcolumn2dlinedy_mainview',

    layout: 'fit',

    controller: 'chart_msstackedcolumn2dlinedy_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "msstackedcolumn2dlinedy",
                reference: 'msstackedcolumn2dlinedy'
            }]
        });
        me.callParent(arguments);
    }

});
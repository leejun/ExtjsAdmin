Ext.define('Admin.view.chart.aspose.GridList', {
    extend: 'Admin.base.BaseGridPanel',
    xtype: 'chart_aspose_gridlist',

    singleSelect: true,
    pagination: false,

    initComponent: function () {
        var me = this;
        Ext.applyIf(me, {
            store: Ext.create('Admin.store.chart.aspose.AsposeCellsStore', {
                autoLoad: true
            }),
            columns: [
                {text: '主键PK', dataIndex: 'id', hidden: true},
                {text: '姓名', dataIndex: 'name', width: 140},
                {text: '年龄', dataIndex: 'age', width: 60},
                {text: '邮箱', dataIndex: 'email', width: 220, flex:1},
                {text: '地址', dataIndex: 'address', width: 220},
                {text: '电话', dataIndex: 'phone', width: 120},
                {text: '体重', dataIndex: 'weight', width: 180, flex:1},
                {text: '性别', dataIndex: 'sex', width: 100},
                {text: '社会保险号', dataIndex: 'cardno', width: 120}
            ]
        });
        me.callParent(arguments);
    }
});
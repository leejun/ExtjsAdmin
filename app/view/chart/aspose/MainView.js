Ext.define('Admin.view.chart.aspose.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_aspose_mainview',

    controller: 'chart_aspose_mainviewcontroller',

    layout: {
        align: 'stretch',
        type: 'vbox'
    },

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'chart_aspose_actionpanel',
                reference: 'actionpanel'
            }, {
                xtype: 'chart_aspose_gridlist',
                reference: 'gridlist',
                flex: 1
            }]
        });

        me.callParent(arguments);
    }

});
Ext.define('Admin.view.chart.aspose.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_aspose_mainviewcontroller',

    listen: {
        component: {
            'basesearchform[reference=searchForm] > textfield': {
                //antionPanel查询条件控件键盘事件
                specialkey: 'specialkeyHandler'
            }
        }
    },

    //导出列表
    onList: function () {
        window.location.href = ServerUrl + '/AsposeCellsApi/exportList';
    },

    //下载列表模板
    onListTpl: function () {
        window.location.href = ServerUrl + '/AsposeCellsApi/exportListTpl';
    },

    //列表分组显示
    onListGroup: function () {
        window.location.href = ServerUrl + '/AsposeCellsApi/exportGroupList';
    },

    //详细报表
    onDetail: function () {
        var me = this;
        if (!me.lookup('gridlist').isSelected()) {
            Ext.popup.Msg('提示信息', '请选择一行记录!');
            return;
        }
        var id = this.lookup('gridlist').getSelectedRecordId();
        window.location.href = ServerUrl + '/AsposeCellsApi/exportDetail?id=' + id + "&format=0";
    },

    //详细报表
    onDetailPdf: function () {
        var me = this;
        if (!me.lookup('gridlist').isSelected()) {
            Ext.popup.Msg('提示信息', '请选择一行记录!');
            return;
        }
        var id = this.lookup('gridlist').getSelectedRecordId();
        window.location.href = ServerUrl + '/AsposeCellsApi/exportDetail?id=' + id + "&format=1";
    },

    //详细模板
    onDetailTpl: function () {
        window.location.href = ServerUrl + '/AsposeCellsApi/exportDetailTpl';
    },

    //详细导出word
    onDetailWord: function () {
        var me = this;
        if (!me.lookup('gridlist').isSelected()) {
            Ext.popup.Msg('提示信息', '请选择一行记录!');
            return;
        }
        var id = this.lookup('gridlist').getSelectedRecordId();
        window.location.href = ServerUrl + '/AsposeCellsApi/exportDetailWord?id=' + id;
    },

    //详细模板
    onDetailWordTpl: function () {
        window.location.href = ServerUrl + '/AsposeCellsApi/exportDetailWordTpl';
    },

    /*刷新表格*/
    refreshGridList: function () {
        this.lookup('gridlist').refresh();
    },

    /*ActionPanel查询条件控件回车事件*/
    specialkeyHandler: function (field, e) {
        if (e.getKey() === e.ENTER) {
            this.searchRecord();
        }
        if (e.getKey() === e.ESC) {
            field.reset();
        }
    },

    /*ActionPanel条件查询*/
    searchRecord: function () {
        var requestParams = this.lookup('searchForm').getValues(false, true);
        this.lookup('gridlist').search(requestParams);
    },

    /*ActionPanel清空查询条件*/
    clearSearch: function () {
        this.lookup('searchForm').reset();
        this.searchRecord();
    }
});

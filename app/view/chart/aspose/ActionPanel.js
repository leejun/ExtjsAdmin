Ext.define('Admin.view.chart.aspose.ActionPanel', {
    extend: 'Admin.base.BaseActionPanel',
    xtype: 'chart_aspose_actionpanel',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: {
                xtype: 'toolbar',
                items: [

                    {
                        xtype: 'basesearchform',
                        reference: 'searchForm',
                        items: [
                            {
                                name: 'QueryParam_',
                                margin: '0 0 0 0',
                                fieldLabel: '查询条件'
                            }
                        ]
                    }, {
                        iconCls: 'x-fa fa-times-circle',
                        ui: 'soft-blue',
                        handler: 'clearSearch'
                    }, {
                        iconCls: 'x-fa fa-search',
                        ui: 'soft-blue',
                        handler: 'searchRecord'
                    },
                    {
                        xtype: 'tbfill'
                    }, {
                        xtype: 'splitbutton',
                        text: '导出列表',
                        ui: 'soft-green',
                        iconCls: 'x-fa fa-file-excel',
                        menu: [{
                            text: '列表数据',
                            iconCls: 'x-fa fa-file-excel',
                            handler: 'onList'
                        }, {
                            text: '列表模板',
                            iconCls: 'x-fa fa-file-excel',
                            handler: 'onListTpl'
                        }, {
                            text: '列表分组',
                            iconCls: 'x-fa fa-file-excel',
                            handler: 'onListGroup'
                        }]
                    }, '-', {
                        xtype: 'splitbutton',
                        text: '导出详细',
                        ui: 'soft-green',
                        iconCls: 'x-fa fa-file-excel',
                        menu: [{
                            text: '详细数据',
                            iconCls: 'x-fa fa-file-excel',
                            handler: 'onDetail'
                        }, {
                            text: '详细数据',
                            iconCls: 'x-fa fa-file-powerpoint',
                            handler: 'onDetailPdf'
                        }, {
                            text: '详细模板',
                            iconCls: 'x-fa fa-file-excel',
                            handler: 'onDetailTpl'
                        }]
                    }, '-', {
                        xtype: 'splitbutton',
                        text: '导出详细',
                        ui: 'facebook',
                        iconCls: 'x-fa fa-file-word',
                        menu: [{
                            text: '详细数据',
                            iconCls: 'x-fa fa-file-word',
                            handler: 'onDetailWord'
                        }, {
                            text: '详细模板',
                            iconCls: 'x-fa fa-file-word',
                            handler: 'onDetailWordTpl'
                        }]
                    }
                ]
            }
        });

        me.callParent(arguments);
    }
});
Ext.define('Admin.view.chart.multiaxisline.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_multiaxisline_mainview',

    layout: 'fit',

    controller: 'chart_multiaxisline_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "multiaxisline",
                reference: 'multiaxisline'
            }]
        });
        me.callParent(arguments);
    }

});
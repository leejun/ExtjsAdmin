Ext.define('Admin.view.chart.spline.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_spline_mainview',

    layout: 'fit',

    controller: 'chart_spline_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "spline",
                reference: 'spline'
            }]
        });
        me.callParent(arguments);
    }

});
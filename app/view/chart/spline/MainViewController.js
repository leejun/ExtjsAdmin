Ext.define('Admin.view.chart.spline.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_spline_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=spline]': {
                successfullyRenderd: 'splineSuccessfullyRenderd'
            }
        }
    },

    splineSuccessfullyRenderd: function () {
        var me = this;
        var spline = me.lookup('spline');
        //延迟0.2秒设置数据 模拟网络加载
        Ext.Function.defer(function () {
            spline.setDataSource({
                chart: {
                    caption: "Average Monthly Temperature in Texas",
                    yaxisname: "Average Monthly Temperature",
                    anchorradius: "5",
                    plottooltext: "Average temperature in $label is <b>$dataValue</b>",
                    showhovereffect: "1",
                    showvalues: "0",
                    numbersuffix: "°C",
                    theme: "fusion",
                    anchorbgcolor: "#72D7B2",
                    palettecolors: "#72D7B2"
                },
                data: [
                    {
                        label: "Jan",
                        value: "1"
                    },
                    {
                        label: "Feb",
                        value: "5"
                    },
                    {
                        label: "Mar",
                        value: "10"
                    },
                    {
                        label: "Apr",
                        value: "12"
                    },
                    {
                        label: "May",
                        value: "14"
                    },
                    {
                        label: "Jun",
                        value: "16"
                    },
                    {
                        label: "Jul",
                        value: "20"
                    },
                    {
                        label: "Aug",
                        value: "22"
                    },
                    {
                        label: "Sep",
                        value: "20"
                    },
                    {
                        label: "Oct",
                        value: "16"
                    },
                    {
                        label: "Nov",
                        value: "7"
                    },
                    {
                        label: "Dec",
                        value: "2"
                    }
                ]
            });
        }, 200);
    }
});

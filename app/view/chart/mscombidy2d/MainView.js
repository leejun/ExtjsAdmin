Ext.define('Admin.view.chart.mscombidy2d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_mscombidy2d_mainview',

    layout: 'fit',

    controller: 'chart_mscombidy2d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "mscombidy2d",
                reference: 'mscombidy2d'
            }]
        });
        me.callParent(arguments);
    }

});
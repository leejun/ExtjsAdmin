Ext.define('Admin.view.chart.mscombidy3d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_mscombidy3d_mainview',

    layout: 'fit',

    controller: 'chart_mscombidy3d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "mscombidy3d",
                reference: 'mscombidy3d'
            }]
        });
        me.callParent(arguments);
    }

});
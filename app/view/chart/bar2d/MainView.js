Ext.define('Admin.view.chart.bar2d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_bar2d_mainview',

    layout: 'fit',

    controller: 'chart_bar2d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "bar2d",
                reference: 'bar2d'
            }]
        });
        me.callParent(arguments);
    }

});
Ext.define('Admin.view.chart.bar2d.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_bar2d_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=bar2d]': {
                successfullyRenderd: 'bar2dSuccessfullyRenderd',
                onDataplotClick: 'onDataplotClickHandle'
            }
        }
    },

    bar2dSuccessfullyRenderd: function () {
        var me = this;
        var bar2d = me.lookup('bar2d');
        //延迟0.2秒设置数据 模拟网络加载
        Ext.Function.defer(function () {
            bar2d.setDataSource({
                chart: {
                    caption: "Lead sources by industry",
                    yaxisname: "Number of Leads",
                    aligncaptionwithcanvas: "0",
                    plottooltext: "<b>$dataValue</b> leads received",
                    theme: "fusion"
                },
                data: [
                    {
                        label: "Travel & Leisure",
                        value: "41"
                    },
                    {
                        label: "Advertising/Marketing/PR",
                        value: "39"
                    },
                    {
                        label: "Other",
                        value: "38"
                    },
                    {
                        label: "Real Estate",
                        value: "32"
                    },
                    {
                        label: "Communications/Cable/Phone",
                        value: "26"
                    },
                    {
                        label: "Construction",
                        value: "25"
                    },
                    {
                        label: "Entertainment",
                        value: "25"
                    },
                    {
                        label: "Staffing Firm/Full Time/Temporary",
                        value: "24"
                    },
                    {
                        label: "Transportation/Logistics",
                        value: "23"
                    },
                    {
                        label: "Utilities",
                        value: "22"
                    },
                    {
                        label: "Aerospace/Defense Products",
                        value: "18"
                    },
                    {
                        label: "Banking/Finance/Securities",
                        value: "16"
                    },
                    {
                        label: "Consumer Products - Non-Durables",
                        value: "15"
                    },
                    {
                        label: "Distribution",
                        value: "13"
                    },
                    {
                        label: "Education",
                        value: "12"
                    },
                    {
                        label: "Health Products & Services",
                        value: "11"
                    },
                    {
                        label: "Hospitality & Hotels",
                        value: "10"
                    },
                    {
                        label: "Non-Business/Residential",
                        value: "6"
                    },
                    {
                        label: "Pharmaceutical",
                        value: "4"
                    },
                    {
                        label: "Printing & Publishing",
                        value: "1"
                    },
                    {
                        label: "Professional Services",
                        value: "1"
                    },
                    {
                        label: "VAR/ISV",
                        value: "1"
                    },
                    {
                        label: "Warranty Administrators",
                        value: "1"
                    }
                ]
            });
        }, 200);
    },

    //图表节点单击事件
    onDataplotClickHandle: function (evt, data) {
        Ext.popup.Msg('提示信息', '图表点击事件 ' + data.categoryLabel + ' | ' + data.value);
    }
});

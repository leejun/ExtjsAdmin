Ext.define('Admin.view.chart.scrollstackedcolumn2d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_scrollstackedcolumn2d_mainview',

    layout: 'fit',

    controller: 'chart_scrollstackedcolumn2d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "scrollstackedcolumn2d",
                reference: 'scrollstackedcolumn2d'
            }]
        });
        me.callParent(arguments);
    }

});
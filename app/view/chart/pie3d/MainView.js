Ext.define('Admin.view.chart.pie3d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_pie3d_mainview',

    layout: 'fit',

    controller: 'chart_pie3d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "pie3d",
                reference: 'pie3d'
            }]
        });
        me.callParent(arguments);
    }

});
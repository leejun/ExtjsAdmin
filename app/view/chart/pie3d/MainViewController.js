Ext.define('Admin.view.chart.pie3d.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_pie3d_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=pie3d]': {
                successfullyRenderd: 'pie3dSuccessfullyRenderd'
            }
        }
    },

    pie3dSuccessfullyRenderd: function () {
        var me = this;
        var pie3d = me.lookup('pie3d');
        //延迟0.2秒设置数据 模拟网络加载
        Ext.Function.defer(function () {
            pie3d.setDataSource({
                chart: {
                    caption: "Recommended Portfolio Split",
                    subcaption: "For a net-worth of $1M",
                    showvalues: "1",
                    showpercentintooltip: "0",
                    numberprefix: "$",
                    enablemultislicing: "1",
                    theme: "fusion"
                },
                data: [
                    {
                        label: "Equity",
                        value: "300000"
                    },
                    {
                        label: "Debt",
                        value: "230000"
                    },
                    {
                        label: "Bullion",
                        value: "180000"
                    },
                    {
                        label: "Real-estate",
                        value: "270000"
                    },
                    {
                        label: "Insurance",
                        value: "20000"
                    }
                ]
            });
        }, 200);
    }
});

Ext.define('Admin.view.chart.pie2d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_pie2d_mainview',

    layout: 'fit',

    controller: 'chart_pie2d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "pie2d",
                reference: 'piein2d'
            }]
        });
        me.callParent(arguments);
    }

});
Ext.define('Admin.view.chart.pie2d.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_pie2d_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=piein2d]': {
                successfullyRenderd: 'piein2dSuccessfullyRenderd'
            }
        }
    },

    piein2dSuccessfullyRenderd: function () {
        var me = this;
        var pieIn2d = me.lookup('piein2d');
        //延迟0.2秒设置数据 模拟网络加载
        Ext.Function.defer(function () {
            pieIn2d.setDataSource({
                "chart": {
                    caption: "Market Share of Web Servers",
                    plottooltext: "<b>$percentValue</b> of web servers run on $label servers",
                    showlegend: "1",
                    showpercentvalues: "1",
                    legendposition: "bottom",
                    usedataplotcolorforlabels: "1",
                    "exportEnabled": "1", //是否导出图表
                    theme: "fusion"
                },
                "data": [
                    {
                        "label": "Jan",
                        "value": "420000"
                    },
                    {
                        "label": "Feb",
                        "value": "810000"
                    },
                    {
                        "label": "Mar",
                        "value": "720000"
                    },
                    {
                        "label": "Apr",
                        "value": "550000"
                    },
                    {
                        "label": "May",
                        "value": "910000"
                    },
                    {
                        "label": "Jun",
                        "value": "510000"
                    },
                    {
                        "label": "Jul",
                        "value": "680000"
                    },
                    {
                        "label": "Aug",
                        "value": "620000"
                    },
                    {
                        "label": "Sep",
                        "value": "610000"
                    },
                    {
                        "label": "Oct",
                        "value": "490000"
                    },
                    {
                        "label": "Nov",
                        "value": "900000"
                    },
                    {
                        "label": "Dec",
                        "value": "730000"
                    }
                ]
            });
        }, 200);
    }
});

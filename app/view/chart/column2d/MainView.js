Ext.define('Admin.view.chart.column2d.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_column2d_mainview',

    layout: 'fit',

    controller: 'chart_column2d_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "column2d",
                reference: 'column2d'
            }]
        });
        me.callParent(arguments);
    }

});
Ext.define('Admin.view.chart.column2d.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_column2d_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=column2d]': {
                successfullyRenderd: 'column2dSuccessfullyRenderd',
                onDataplotClick: 'onDataplotClickHandle'
            }
        }
    },

    column2dSuccessfullyRenderd: function () {
        var me = this;
        var column2d = me.lookup('column2d');
        //延迟0.2秒设置数据 模拟网络加载
        Ext.Function.defer(function () {
            column2d.setDataSource({
                chart: {
                    caption: "Countries With Most Oil Reserves [2017-18]",
                    subcaption: "In MMbbl = One Million barrels",
                    xaxisname: "Country",
                    yaxisname: "Reserves (MMbbl)",
                    numbersuffix: "K",
                    theme: "fusion"
                },
                data: [
                    {
                        label: "Venezuela",
                        value: "290"
                    },
                    {
                        label: "Saudi",
                        value: "260"
                    },
                    {
                        label: "Canada",
                        value: "180"
                    },
                    {
                        label: "Iran",
                        value: "140"
                    },
                    {
                        label: "Russia",
                        value: "115"
                    },
                    {
                        label: "UAE",
                        value: "100"
                    },
                    {
                        label: "US",
                        value: "30"
                    },
                    {
                        label: "China",
                        value: "300"
                    }
                ]
            });
        }, 200);
    },

    //图表节点单击事件
    onDataplotClickHandle: function (evt, data) {
        Ext.popup.Msg('提示信息', '图表点击事件 ' + data.categoryLabel + ' | ' + data.value);
    }
});

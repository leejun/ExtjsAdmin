Ext.define('Admin.view.chart.line.MainViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.chart_line_mainviewcontroller',

    listen: {
        component: {
            'basefusionchart[reference=line]': {
                successfullyRenderd: 'lineSuccessfullyRenderd'
            }
        }
    },

    lineSuccessfullyRenderd: function () {
        var me = this;
        var line = me.lookup('line');
        Ext.Ajax.request({
            url: ServerUrl + '/FusionchartsApi/line',
            method: 'GET',
            success: function (response, options) {
                var result = Ext.JSON.decode(response.responseText);
                if (result.success) {
                    line.setDataSource(result.data);
                }
            }
        });
    }
});

Ext.define('Admin.view.chart.line.MainView', {
    extend: 'Ext.container.Container',
    xtype: 'chart_line_mainview',

    layout: 'fit',

    controller: 'chart_line_mainviewcontroller',

    initComponent: function () {
        var me = this;

        Ext.applyIf(me, {
            items: [{
                xtype: 'basefusionchart',
                chartType: "line",
                reference: 'line'
            }]
        });
        me.callParent(arguments);
    }

});
Ext.define('Admin.view.dashboard.Todos', {
    extend: 'Ext.panel.Panel',
    xtype: 'todo',

    requires: [
        'Ext.grid.Panel',
        'Ext.grid.View',
        'Ext.form.field.Text',
        'Ext.button.Button',
        'Ext.selection.CheckboxModel'
    ],

    cls: 'todo-list shadow-panel',

    title: 'TODO List',
    height: 230,
    bodyPadding: 15,
    layout: 'fit',
    items: [
        {
            xtype: 'gridpanel',
            cls: 'dashboard-todo-list',
            header: false,
            title: 'My Grid Panel',
            hideHeaders: true,
            scrollable: {
                x: false,
                y: false
            },
            bind: {
                store: '{todos}'
            },
            columns: [
                {
                    xtype: 'gridcolumn',
                    dataIndex: 'task',
                    text: 'Task',
                    flex: 1
                }
            ],
            selModel: {
                selType: 'checkboxmodel'
            }
        }
    ]
});

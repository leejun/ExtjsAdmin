Ext.define('Admin.view.charts.Charts', {
    extend: 'Ext.container.Container',
    xtype: 'charts',

    requires: [
        'Admin.view.charts.Area',
        'Admin.view.charts.Bar',
        'Admin.view.charts.ChartsModel',
        'Admin.view.charts.Gauge',
        'Admin.view.charts.Pie3D',
        'Admin.view.charts.Polar',
        'Admin.view.charts.Stacked',
        'Ext.layout.container.Column'
    ],

    viewModel: {
        type: 'charts'
    },

    scrollable: 'y',

    layout:'column',

    defaults: {
        columnWidth: 0.5,
        defaults: {
            animation : !Ext.isIE9m && Ext.os.is.Desktop
        }
    },

    items: [
        {
            xtype: 'chartsareapanel',
            padding : '0 10 10 0'
        },
        {
            xtype: 'chartspie3dpanel',
            padding : '0 10 10 0'
        },
        {
            xtype: 'chartspolarpanel',
            padding : '0 10 10 0'
        },
        {
            xtype: 'chartsstackedpanel',
            padding : '0 10 10 0'
        },
        {
            xtype: 'chartsbarpanel',
            padding : '0 10 0 0'
        },
        {
            xtype: 'chartsgaugepanel',
            padding : '0 10 0 0'
        }
    ]
});

Ext.define('Admin.view.authentication.AuthenticationController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.authentication',

    onFaceBookLogin : function() {
        this.redirectTo('dashboard', true);
    },

    onLoginButton: function() {
        //this.redirectTo('dashboard', true);
        var me =this;
        var basicForm = this.getView();

        if(basicForm.isValid()) {
            basicForm.submit({
                waitMsg : "登录验证中...",
                url: 'AuthController.do?m=loginAjax',
                success: function(form, action) {
                    if(action.result.data.name) {
                        var expires = new Date(new Date().getTime()+(1000*60*60*24*30));
                        var path = "/webapp/";
                        Ext.util.Cookies.set('loginName',action.result.data.loginName,expires,path);
                    }
                    me.getView().destroy();
                    me.showMainView(action.result.data);
                },
                failure: function(form, action) {
                    Ext.Msg.show({
                        title: '登录失败',
                        msg: action.result.message,
                        buttons: Ext.Msg.OK,
                        icon: Ext.Msg.ERROR
                    });
                }
            });
        }
    },

    onLoginAsButton: function() {
        this.redirectTo('login', true);
    },

    onNewAccount:  function() {
        this.redirectTo('register', true);
    },

    onSignupClick:  function() {
        this.redirectTo('dashboard', true);
    },

    onResetClick:  function() {
        this.redirectTo('dashboard', true);
    }
});
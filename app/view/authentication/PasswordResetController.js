Ext.define('Admin.view.authentication.PasswordResetController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.passwordresetcontroller',

    /*重置密码*/
    onResetClick: function () {
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.onResetConfirmed,
            scope: this
        });
    },

    /*重置密码提交数据*/
    onResetConfirmed: function (choice) {
        if (choice === 'ok') {
            var me = this;
            var basicForm = this.lookup('form');
            if (basicForm.isValid()) {
                basicForm.submit({
                    waitMsg: "修改密码中...",
                    url: ServerUrl + '/AppController/passwordReset',
                    success: function (form, action) {
                        Ext.popup.Msg('提示信息', '密码修改成功');
                        me.redirectTo(LoginViewXtype);
                        me.getView().destroy();
                    },
                    failure: function (form, action) {
                        Ext.Msg.show({
                            title: '修改密码失败',
                            msg: action.result.message,
                            buttons: Ext.Msg.OK,
                            icon: Ext.Msg.ERROR
                        });
                    }
                });
            }
        }
    }
});
Ext.define('Admin.view.authentication.LoginWindowController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.loginwindowcontroller',

    //系统登录
    onLoginButton: function () {
        var me = this;
        var basicForm = this.lookup('form');

        if (basicForm.isValid()) {
            basicForm.submit({
                waitMsg: "登录验证中...",
                url: ServerUrl + '/AppController/login',
                errorHint: false, //不使用全局错误提示,在failure自定义错误消息
                success: function (form, action) {
                    Ext.popup.Msg('提示信息', '登录成功');
                    me.loginSuccess(action.result.data);
                },
                failure: function (form, action) {
                    //Ext.popup.Msg('登录失败', action.result.message);
                }
            });
        }
    },

    // {
    //     "userId": "747801336438984706",
    //     "authToken": "7fda0c26-bed0-487b-b209-8e7bec7d6a91",
    //     "jwtToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NDM0MTk4NDgsInVzZXJuYW1lIjoiYWRtaW4xIn0.nO8aR4TMWkiwNK3uqrn6-0wPXFaA2hD6AS8rqE5ZfQ4",
    //     "username": "admin1",
    //     "roles": null,
    //     "permissions": null,
    //     "loginTime": "2022-01-14 09:30:48",
    //     "userHost": "127.0.0.1"
    // }
    loginSuccess: function (user) {
        var me = this;
        console.log("登录成功.当前账户信息:--> " + Ext.JSON.encode(user));

        //登录成功把token写入浏览器缓存
        console.log('登录成功.设置auth-token :-> ' + user.authToken + '到浏览器LocalStorage中');
        Ext.util.LocalStorage.get('ext').setItem('auth-token', user.authToken);

        console.log('登录成功.设置jwt-token :-> ' + user.jwtToken + '到浏览器LocalStorage中');
        Ext.util.LocalStorage.get('ext').setItem('jwt-token', user.jwtToken);

        //获取系统主视图
        var mainView = Admin.getApplication().getMainView();
        //设置ViewModel(登录用户数据),渲染首页用户信息 头像等.
        mainView.getController().setMainViewData({user: user});
        //重新加载首页菜左侧单数据.
        mainView.lookup("navigationTreeList").getStore().reload({
            callback: function () {
                //跳转到首页面
                me.redirectTo(MainViewXtype, true);
            }
        });
        //销毁当前登录窗口
        me.getView().destroy();
    }

});
/*系统登录弹窗*/
Ext.define('Admin.view.authentication.LoginWindow', {
    extend: 'Admin.view.authentication.LockingWindow',
    xtype: 'login',

    controller: 'loginwindowcontroller',

    defaultFocus: 'authdialog',
    header: false,

    items: [
        {
            xtype: 'authdialog',
            defaultButton : 'loginButton',
            autoComplete: true,
            bodyPadding: '20 20',
            cls: 'auth-dialog-login',
            header: false,
            width: 415,
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            reference: 'form',

            defaults : {
                margin : '5 0'
            },

            items: [
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    name: 'username',
                    height: 55,
                    hideLabel: true,
                    allowBlank : false,
                    emptyText: '账户',
                    triggers: {
                        glyphed: {
                            cls: 'trigger-glyph-noop auth-email-trigger'
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    height: 55,
                    hideLabel: true,
                    emptyText: '密码',
                    inputType: 'password',
                    name: 'password',
                    allowBlank : false,
                    triggers: {
                        glyphed: {
                            cls: 'trigger-glyph-noop auth-password-trigger'
                        }
                    }
                },
                {
                    xtype: 'container',
                    layout: 'hbox',
                    items: [
                        {
                            xtype: 'checkboxfield',
                            flex : 1,
                            cls: 'form-panel-font-color rememberMeCheckbox',
                            height: 30,
                            name: 'rememberMe',
                            boxLabel: '记住我(15天)'
                        },
                        {
                            xtype: 'box',
                            html: '<a href="#login" class="link-forgot-password">忘记密码请联系管理员</a>'
                        }
                    ]
                },
                {
                    xtype: 'button',
                    reference: 'loginButton',
                    scale: 'large',
                    ui: 'soft-blue',
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '登录',
                    formBind: true,
                    listeners: {
                        click: 'onLoginButton'
                    }
                }
            ]
        }
    ],

    initComponent: function() {
        this.addCls('user-login-register-container');
        this.callParent(arguments);
    }
});

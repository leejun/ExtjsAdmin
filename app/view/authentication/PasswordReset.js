/*修改密码弹窗*/
Ext.define('Admin.view.authentication.PasswordReset', {
    extend: 'Admin.view.authentication.LockingWindow',
    xtype: 'passwordreset',

    controller: 'passwordresetcontroller',

    defaultFocus: 'authdialog',
    header: false,

    items: [
        {
            xtype: 'authdialog',
            width: 455,
            defaultButton: 'resetPassword',
            autoComplete: true,
            bodyPadding: '20 20',
            layout: {
                type: 'vbox',
                align: 'stretch'
            },

            reference: 'form',

            defaults: {
                margin: '5 0'
            },

            cls: 'auth-dialog-login',
            items: [
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    height: 55,
                    name: 'password',
                    hideLabel: true,
                    allowBlank: false,
                    emptyText: '输入旧密码',
                    inputType: 'password',
                    triggers: {
                        glyphed: {
                            cls: 'trigger-glyph-noop auth-password-trigger'
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    height: 55,
                    hideLabel: true,
                    emptyText: '输入新密码',
                    inputType: 'password',
                    name: 'passwordNew1',
                    minLength: 6,
                    maxLength: 20,
                    allowBlank: false,
                    triggers: {
                        glyphed: {
                            cls: 'trigger-glyph-noop auth-password-trigger'
                        }
                    }
                },
                {
                    xtype: 'textfield',
                    cls: 'auth-textbox',
                    height: 55,
                    hideLabel: true,
                    emptyText: '输入新密码',
                    inputType: 'password',
                    name: 'passwordNew2',
                    minLength: 6,
                    maxLength: 20,
                    allowBlank: false,
                    triggers: {
                        glyphed: {
                            cls: 'trigger-glyph-noop auth-password-trigger'
                        }
                    }
                },
                {
                    xtype: 'button',
                    reference: 'resetPassword',
                    scale: 'large',
                    ui: 'soft-blue',
                    formBind: true,
                    iconAlign: 'right',
                    iconCls: 'x-fa fa-angle-right',
                    text: '修改密码',
                    listeners: {
                        click: 'onResetClick'
                    }
                },
                {
                    xtype: 'component',
                    html: '<div style="text-align:right">' +
                        '<a href="#admindashboard" class="link-forgot-password">' +
                        '返回到首页</a>' +
                        '</div>'
                }
            ]
        }
    ]
});

//ViewModel 数据双向绑定
Ext.define('Admin.view.main.MainModel', {
    extend: 'Ext.app.ViewModel',
    alias: 'viewmodel.main_mainmodel',

    data: {
        currentView: null,
        //websocket当前状态
        wsState: '离线',
        //websocket图标颜色 (x-fa fa-rss  |  x-fa fa-rss green)
        wsStateIconcls: 'x-fa fa-rss',
        //未读消息数量
        unReadMsgCount: 0,
        //用户信息
        user: {}
    }
});

// user数据结构
// {
//     "userId": "747807614129278981",
//     "authToken": "0fc9dac8-13ba-4445-9a79-6ecef12e124a",
//     "jwtToken": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJleHAiOjE2NDM0MjEzNDUsInVzZXJuYW1lIjoiYWRtaW4xIn0.w1GxFUHbYugH2U16fVi4f3mzGUDaAsFd4dPnbohoY7o",
//     "username": "admin1",
//     "roles": null,
//     "permissions": null,
//     "loginTime": "2022-01-14 09:55:45",
//     "userHost": "127.0.0.1"
// }
Ext.define('Admin.view.main.MainController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.main_maincontroller',

    listen: {
        controller: {
            '#': {
                //路由事件处理
                unmatchedroute: 'onRouteChange'
            }
        }
    },

    //当前视图
    lastView: null,

    //用户数据
    userInfo: null,

    //websocket客户端
    webSocketClient: null,

    //心跳任务
    heartbeatTimer: null,

    /*控制器初始化方法*/
    init: function () {
        var me = this;
        //每分钟请求一次后端接口，刷新一下session.
        //防止长时间无操作session丢失，导致来不急保存数据.
        this.heartbeatTimer = Ext.interval(function() {
            Ext.Ajax.request({
                url: ServerUrl + '/AppController/heartbeat',
                method: 'GET'
            });
        }, 60000);
        me.callParent(arguments);
    },

    /*路由事件处理*/
    onRouteChange: function (hashTag) {
        console.log("onRouteChange(路由事件处理) :--> " + hashTag);
        this.setCurrentView(hashTag);
    },

    /*加载视图渲染到页面*/
    setCurrentView: function (hashTag) {
        hashTag = (hashTag || '').toLowerCase();
        console.log("setCurrentView(加载视图渲染到页面) :--> " + hashTag);

        var me = this,
            //获取设置了reference=xxx的全部组件
            refs = me.getReferences(),
            //模块容器，我们开发的模块都放在它下面
            mainCard = refs.mainCardPanel,
            //获取布局对象
            mainLayout = mainCard.getLayout(),
            //左侧菜单导航树
            navigationList = refs.navigationTreeList,
            //菜单treeStore
            store = navigationList.getStore(),
            //根据hashTag找到树菜单节点对象
            node = store.findNode('viewType', hashTag),
            //获取视图的xtype 如果不存在则返回404页面
            view = (node && node.get('viewType')) || 'login',
            //当前页面
            lastView = me.lastView,
            //查找mainCard是否已经存在(hashTag代表的视图组件)
            existingItem = mainCard.child('component[routeId=' + hashTag + ']'),
            //新创建的视图组件
            newView;

        //如果当前页面存在 并且是window窗口则销毁
        if (lastView && lastView.isWindow) {
            lastView.destroy();
        }

        //赋值当前显示的视图
        lastView = mainLayout.getActiveItem();

        //如果hashTag视图不存在则创建
        if (!existingItem) {
            newView = Ext.create({
                xtype: view,
                //给视图组件添加一个属性，方便以后查找.
                // mainCard.child('component[routeId=' + hashTag + ']'),
                routeId: hashTag,
                hideMode: 'offsets'
            });
        }

        if (!newView || !newView.isWindow) {
            // !newView means we have an existing view, but if the newView isWindow we don't add it to the card layout.
            if (existingItem) {
                // We don't have a newView, so activate the existing view.
                // 如果要加载的视图已经存在于mainCard中，则设置你显示状态.
                if (existingItem !== lastView) {
                    mainLayout.setActiveItem(existingItem);
                }
                newView = existingItem;
            } else {
                // newView is set (did not exist already), so add it and make it the activeItem
                // 新创建的视图要加入到mainCard中，并且设置其显示状态.
                Ext.suspendLayouts();
                mainLayout.setActiveItem(mainCard.add(newView));
                Ext.resumeLayouts(true);
            }
        }

        //设置菜单树选中这个节点
        navigationList.setSelection(node);

        //如果新建的主图可以获得焦点，则设置其焦点.
        if (newView.isFocusable(true)) {
            newView.focus();
        }

        me.lastView = newView;
    },

    /*左侧菜单树点击事件*/
    onTreeSelectionChange: function (tree, record, item, index, e, eOpts) {
        var viewType = record.get('viewType');
        if(viewType) {
            console.log("onTreeSelectionChange(菜单树点击事件) :--> " + viewType);
            //如果进入系统消息模块，则去掉红色角标.
            if('sys_message_mainview' === viewType) {
                record.set('rowCls', '');
            }
            //跳转到模块
            this.redirectTo(viewType);
        }
    },

    /*菜单显示|隐藏处理逻辑*/
    onToggleNavigationSize: function () {
        var me = this,
            //获取全局组件
            refs = me.getReferences(),
            //导航树
            navigationList = refs.navigationTreeList,
            //导航树父容器
            navTreePanel = refs.navTreePanel,
            //容器
            wrapContainer = refs.mainContainerWrap,
            collapsing = !navigationList.getMicro(),
            new_width = collapsing ? 64 : 250;

        Ext.suspendLayouts();
        refs.senchaLogo.setWidth(new_width);
        navTreePanel.setWidth(new_width);
        navigationList.setMicro(collapsing);
        Ext.resumeLayouts();
        wrapContainer.updateLayout();
    },

    /* Viewport渲染后事件,加载系统首页 */
    onMainViewRender: function () {
        if (!window.location.hash) {
            this.redirectTo(MainViewXtype);
        }
    },

    /*安全退出*/
    onLogout: function () {
        Ext.Msg.show({
            title: '提示信息',
            msg: '确定该操作吗?',
            buttons: Ext.Msg.OKCANCEL,
            icon: Ext.Msg.QUESTION,
            fn: this.onLogoutConfirmed,
            scope: this
        });
    },

    /*发送Ajax请求退出系统*/
    onLogoutConfirmed: function (choice) {
        if (choice === 'ok') {
            var me = this;
            Ext.Ajax.request({
                url: ServerUrl + '/AppController/logout',
                method: 'POST',
                maskContainer: this.getView(),
                success: function () {
                    me.userInfo = null;
                    //清空JwtToken
                    Ext.util.LocalStorage.get('ext').removeItem ('jwt-token');
                    //未读消息重置
                    me.getViewModel().setData({unReadMsgCount: 0});
                    //正常关闭websocket
                    me.closeWebsocket();
                    //跳转到登录页
                    me.redirectTo('login');
                }
            });
        }
    },

    /*修改密码*/
    onModifyPassword: function () {
        //路由到修改密码视图
        this.redirectTo('passwordreset');
    },

    /*设置视图ViewModel的数据*/
    setMainViewData: function (data) {
        this.userInfo = data.user;
        this.getViewModel().setData(data);
        this.closeWebsocket();
        this.connectWebsocket();
    },

    /*WebSocket连接服务器*/
    connectWebsocket: function () {
        var me = this;

        function connect() {
            //与服务器进行连接
            var wsClient = new WebSocket(WebSocketUrl + '/userId=' + me.userInfo.userId);

            //连接成功
            wsClient.onopen = function () {
                console.log('WebSocket连接成功.');
                me.getViewModel().setData({wsState: '在线', wsStateIconcls: 'x-fa fa-rss green'});
                me.webSocketClient = wsClient;
            };

            //收到服务器消息
            wsClient.onmessage = function (e) {
                //console.log('WebSocket收到消息. ', e.data);
                me.websocketMessageHandle(e.data);
            };

            //连接关闭
            wsClient.onclose = function (e) {
                me.getViewModel().setData({wsState: '离线', wsStateIconcls: 'x-fa fa-rss'});
                if(e.target.websocketReconnect === false) {
                    console.log('WebSocket连接正常断开. 不进行重连');
                } else {
                    console.log('WebSocket连接异常断开. 5秒后尝试重新连接...', e.reason);
                    /*jshint browser: true */
                    setTimeout(function () {
                        connect();
                    }, 5000);
                }
            };

            //连接出错
            wsClient.onerror = function (err) {
                console.log('WebSocket连接出错.', err.message, '.关闭连接');
                wsClient.close();
            };
        }

        //开始连接
        connect();
    },

    //正常关闭Websocket连接
    closeWebsocket: function () {
        var wsClient = this.webSocketClient;
        if (wsClient != null && wsClient.readyState === 1) {
            //自定义属性 正常关闭后 不进行自动重连
            wsClient.websocketReconnect = false;
            wsClient.close();
            this.webSocketClient = null;
        }
    },

    //处理websocket消息
    websocketMessageHandle: function (value) {
        try {
            var pushMessage = Ext.JSON.decode(value);
            //未读的系统消息数量
            if(pushMessage.type === 'SYS_MESSAGE') {
                //目前前端缓存的未读消息数量
                var unReadMsgCount = this.getViewModel().get('unReadMsgCount');
                //前段未读数量 小于后端推送的数量 点亮红色角标
                if(unReadMsgCount < pushMessage.unReadMsgCount) {
                    this.setSysMessageTreeNodeState();
                }
                //更新前端未读消息数量
                this.getViewModel().setData({unReadMsgCount: pushMessage.unReadMsgCount});
                this.fireEvent('unReadMsgCountUpdate');
            }

            //系统运行信息
            if(pushMessage.type === 'SYS_INFO') {
                this.fireEvent('onMonitoringMessageUpdate', pushMessage.data);
            }

            //服务器推送消息让客户端退出
            if(pushMessage.type === 'OFF_LINE') {
                this.onLogoutConfirmed('ok');
            }

            //服务器推送系统日志
            if(pushMessage.type === 'SYS_LOG') {
                this.fireEvent('onSystemLogUpdate', pushMessage.data);
            }
        } catch (e) {
            console.error("服务器消息Json解码出错", value, e);
        }
    },

    //设置系统消息模块红色角标
    setSysMessageTreeNodeState: function () {
        var me = this;
        var store = me.lookup('navigationTreeList').getStore();
        var record = store.findNode('viewType', 'sys_message_mainview');
        record.set('rowCls', 'nav-tree-badge nav-tree-badge-new');
    },

    //视图销毁时关闭定时任务
    destroy: function() {
        Ext.uninterval(this.heartbeatTimer);
    }
});

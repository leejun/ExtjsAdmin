Ext.define('Admin.view.main.Main', {
    extend: 'Ext.container.Viewport',
    xtype: 'app-main',

    requires: [
        'Ext.button.Segmented',
        'Ext.list.Tree'
    ],

    controller: 'main_maincontroller',
    viewModel: 'main_mainmodel',

    cls: 'sencha-dash-viewport',
    //itemId: 'mainView',

    layout: {
        type: 'vbox',
        align: 'stretch'
    },

    listeners: {
        /*当前视图组件渲染到html中以后触发 会调用main(Controller)当中的onMainViewRender*/
        render: 'onMainViewRender'
    },

    items: [
        {
            xtype: 'toolbar',
            cls: 'sencha-dash-dash-headerbar shadow',
            height: 64,
            itemId: 'headerBar',
            items: [
                {
                    xtype: 'component',
                    reference: 'senchaLogo',
                    cls: 'sencha-logo',
                    html: '<div class="main-logo"><img src="resources/images/company-logo.png">YinXing</div>',
                    width: 250
                },
                {
                    margin: '0 0 0 8',
                    ui: 'header',
                    iconCls:'x-fa fa-bars',
                    id: 'main-navigation-btn',
                    handler: 'onToggleNavigationSize'
                },
                '->',
                {
                    ui: 'header',
                    tooltip: 'WebSocket连接状态',
                    bind: {
                        text: '{wsState}',
                        iconCls: '{wsStateIconcls}'
                    }
                },
                {
                    iconCls:'x-fa fa-bell',
                    ui: 'header',
                    href: '#sys_message_mainview',
                    hrefTarget: '_self',
                    tooltip: '系统消息',
                    bind: {
                        text: '({unReadMsgCount})'
                    }
                },
                {
                    iconCls:'x-fa fa-envelope',
                    ui: 'header',
                    tooltip: '微信:vstardaxin'
                },
                {
                    xtype: 'segmentedbutton',
                    margin: '0 16 0 0',
                    items: [{
                        iconCls: 'x-fa fa-key',
                        handler: 'onModifyPassword',
                        tooltip: '修改密码'
                    }, {
                        iconCls: 'x-fa fa-power-off',
                        handler: 'onLogout',
                        tooltip: '安全退出'
                    }]
                },
                {
                    xtype: 'tbtext',
                    bind: '{user.username}',
                    cls: 'top-user-name'
                },
                {
                    xtype: 'image',
                    cls: 'header-right-profile-image',
                    height: 35,
                    width: 35,
                    bind: {
                        src: '{user.profile}'
                    }
                }
            ]
        },
        {
            xtype: 'maincontainerwrap',
            reference: 'mainContainerWrap',
            flex: 1,
            items: [
                {
                    xtype : 'container',
                    width : 250,
                    reference : 'navTreePanel',
                    itemId : 'navTreePanel',
                    style : 'overflow:hidden',
                    layout : 'absolute',
                    items: {
                        xtype: 'treelist',
                        reference: 'navigationTreeList',
                        itemId: 'navigationTreeList',
                        ui: 'nav',
                        x : 0,
                        y : 0,
                        anchor : '+17 0',
                        store : Ext.create('Admin.store.sys.NavigationTreeStore', {autoLoad : true}),
                        style : {
                            'overflow-y' : 'scroll'
                        },
                        expanderFirst: false,
                        expanderOnly: false,
                        listeners: {
                            selectionchange: 'onTreeSelectionChange'
                        }
                    }
                },
                {
                    //我们开发的模块都放在它里面
                    xtype: 'container',
                    reference: 'mainCardPanel',
                    cls: 'sencha-dash-right-main-container',
                    flex: 1,
                    padding: 10,
                    layout: {
                        type: 'card',
                        anchor: '100%'
                    }
                }
            ]
        }
    ]
});

Ext.define("Admin.base.BaseAddButton", {
    extend: 'Ext.Button',
    alias: 'widget.addbutton',

    text: '新增',
    iconCls: 'x-fa fa-plus-square',
    ui: 'soft-blue'
});
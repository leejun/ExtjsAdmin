Ext.define('Admin.base.BaseUtils', function () {
    return {
        statics: {
            rendererStop: function (value) {
                var v = value ? '禁用' : '启用';
                if (value) {
                    return '<span style="border-radius:0.25em; padding: 2px; background-color: #fc8999; color: #fff">' + v + '</span>';
                } else {
                    return '<span style="border-radius:0.25em; padding: 2px; background-color: #1ab394; color: #fff">' + v + '</span>';
                }
            },

            rendererBoolean: function (value) {
                var v = value ? '是' : '否';
                if (value) {
                    return '<span style="border-radius:0.25em; padding: 0 10px; background-color: #fc8999; color: #fff">' + v + '</span>';
                } else {
                    return '<span style="border-radius:0.25em; padding: 0 10px; background-color: #1ab394; color: #fff">' + v + '</span>';
                }
            },

            rendererValue: function (value) {
                return '<span style="border-radius:0.25em; padding: 0 10px; background-color: #1ab394; color: #fff">' + value + '</span>';
            },

            rendererQtip: function (value, metaData) {
                metaData.tdAttr = 'data-qtip="' + value + '"';
                return value;
            },

            rendererProfile: function (value) {
                return "<img src='" + value + "' onerror=\"this.onerror=null;this.src='resources/images/user-profile/1.png'\" height='30px' width='30px'>";
            }
        }
    };
});
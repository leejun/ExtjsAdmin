Ext.define("Admin.base.BaseFormPanel", {
	extend: 'Ext.form.Panel',
	alias: 'widget.baseformpanel',
	
	defaultType: 'textfield',
	
	waitMsgTarget: true,
	
	autoScroll: true,
	
	bodyPadding: 10,
	
	border: false,

	defaults : {
		labelAlign: 'right',
		allowBlank: true
	},

	layout: {
		align: 'stretch',
		type: 'vbox'
	},
	
    plugins: {
        ptype: 'datatip'
    },
	
    initComponent: function() {
        this.on("beforeaction" ,this.onBeforeaction, this);
        this.on("actioncomplete", this.onActioncomplete, this);
        this.callParent(arguments);
    },
      
    onBeforeaction: function(basic, action, eOpts) {
    	if(action.type && action.type === "submit") {
    		if(action.clientValidation) {
	    		return basic.isValid();
	    	}
    	}
    },
    
    onActioncomplete: function(basic, action, eOpts) {
    	if(action.type === "submit" && action.popupMsg !== false) {
    		Ext.popup.Msg('提示信息', '表单数据保存成功');
    	}
    	if(action.type === "load" && action.popupMsg !== false) {
    		Ext.popup.Msg('提示信息', '表单数据加载成功');
    	}
    },
    
    submit: function(options) {
		Ext.applyIf(options, {
    		clientValidation: true,
    		submitEmptyText: false,
    		waitMsg : "数据保存中..."
    	});
    	this.form.submit(options);
    },
    
	load: function(options) {
    	Ext.applyIf(options, {
    		waitMsg : "数据加载中..."
    	});
    	this.form.load(options);
    },
			    
    /**
     * 获取某个表单元素的值
     * @param {String} fieldName [id or name or hiddenName]
     * @Return {Object}
     */
    getFieldValue: function(fieldName) {
    	return this.getForm().findField(fieldName).getValue();
    },
    
    /**
     * 设置某个表单元素的值
     * @param {String} fieldName [id or name or hiddenName]
     * @param {Object} value
     */
    setFieldValue: function(fieldName, value) {
    	this.getForm().findField(fieldName).setValue(value);
    },
    
    /**
     * 获取某个表单元素提交的值
     * @param {String} fieldName [id or name or hiddenName]
     * @Return {String} [The value to be submitted, or null]
     */
    getFieldSubmitValue: function(fieldName) {
    	return this.getForm().findField(fieldName).getSubmitValue();
    },
    
    /**
     * 清空表单元素
     */
    reset: function() {
    	this.getForm().reset();
    },
    
    /**
     * 设置表单元素可用状态
     */
    setAllFieldReadOnly : function(readOnly) {
    	var components = this.query('field');
    	Ext.each(components, function(field) {
    		field.setReadOnly(readOnly);
    	});
    },
    
    /**
     * 设置元素的状态
     * @param {} fieldName string
     * @param {} readOnly boolean
     */
    setFieldReadOnly : function(fieldName, readOnly) {
    	var field = this.getForm().findField(fieldName);
		field.setReadOnly(readOnly);
    }
    
});
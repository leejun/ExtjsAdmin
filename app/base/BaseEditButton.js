Ext.define("Admin.base.BaseEditButton", {
    extend: 'Ext.Button',
    alias: 'widget.editbutton',

    text: '编辑',
    iconCls : 'x-fa fa-edit',
    ui: 'soft-green'
});
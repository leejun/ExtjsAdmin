Ext.define("Admin.base.BaseGridPanel", {
    extend: 'Ext.grid.Panel',
    alias: 'widget.basegridpanel',

    /*是否显示底部分页工具条*/
    pagination: true,

    /*多选*/
    multiSelect: false,

    /*单选*/
    singleSelect: false,

    /*样式*/
    cls: 'user-grid',

    /*表头边框*/
    headerBorders: false,

    /*表格设置*/
    viewConfig: {
        //表格行 是否隔行变色
        stripeRows: false,
        //表格单元格是否可以选中
        enableTextSelection: true,
        //表格加载时显示文字
        loadingText: "数据读取中...",
        //是否显示表格分割行
        columnLines: false
    },

    /*记住表格状态*/
    stateful: false,

    initComponent: function () {
        var me = this;

        Ext.apply(me, {
            stateId: me.$className,
            sortableColumns: true
        });

        if (me.multiSelect) {
            Ext.apply(me, {
                selModel: {
                    selType: 'checkboxmodel',
                    mode: "MULTI"
                }
            });
        }

        if (me.singleSelect) {
            Ext.apply(me, {
                selModel: {
                    selType: 'checkboxmodel',
                    //multi,simple,single；默认为多选multi
                    mode: "single",
                    //如果值为true，则只用点击checkbox列才能选中此条记录
                    //checkOnly: true,
                    //如果值true，并且mode值为单选（single）时，可以通过点击checkbox取消对其的选择
                    allowDeselect: true
                }
            });
        }

        // finally call the superclasses implementation
        me.callParent(arguments);

        if (me.pagination) {
            me.addDocked(
                Ext.create("Admin.base.PagingToolbar", {
                    store: me.store,
                    dock: 'bottom',
                    displayInfo: true
                })
            );
        }
    },

    /**
     * 条件查询
     */
    search: function (params) {
        //this.getStore().proxy.extraParams = {params: Ext.JSON.encode(params)};
        this.getStore().proxy.extraParams = params;
        this.loadPage(1);
        this.getSelectionModel().deselectAll();
    },

    /**
     * 刷新当前页
     */
    refresh: function () {
        this.getStore().reload();
        this.getSelectionModel().deselectAll();
    },

    /**
     * 加载指定页
     * @param {int} pageNo
     */
    loadPage: function (pageNo) {
        this.getStore().loadPage(pageNo);
    },

    /**
     * 获得选中行的Records(多选)
     * @Return{Ext.data.Model[]}
     */
    getSelectedRecords: function () {
        return this.getSelectionModel().getSelection();
    },

    /**
     * 获得选中行的Record(单选)
     * 注意:如果多选则返回选中第一行
     * @Return{Ext.data.Model}
     */
    getSelectedRecord: function () {
        return this.getSelectionModel().getSelection()[0];
    },

    /**
     * 获得选中行Record(单选)的ID
     * 注意:如果多选则返回选中第一行
     * @Return{Number/String}
     */
    getSelectedRecordId: function () {
        var record = this.getSelectedRecord();
        return record ? record.getId() : "";
    },

    /**
     * 获得选中行Record(单选)单元格的值
     * 注意:如果多选则返回选中第一行
     * @param {string} dataIndex
     * @Return{Object}
     */
    getSelectedCellValue: function (dataIndex) {
        var record = this.getSelectedRecord();
        return record.get(dataIndex);
    },

    /**
     * 获得选中行Record(单选)单元格的值
     * 注意:如果多选则返回选中第一行
     * @param {string} dataIndex
     * @Return{Object}
     */
    getSelectedCellValues: function (dataIndex) {
        var records = this.getSelectedRecords(),
            values = [];
        Ext.each(records, function (item) {
            values.push(item.get(dataIndex));
        });
        return values.join(",");
    },

    /**
     * 获得选中行Record(多选)的ID集合
     * @param {Boolean} join 如果传入true 那么将返回以","(逗号)连接的字符串 例如:"1,2,3" 否则返回数组 例如:[1,2,3]
     * @Return{Array/String}
     */
    getSelectedRecordIds: function (join) {
        var records = this.getSelectedRecords(),
            ids = [];
        Ext.each(records, function (item) {
            ids.push(item.getId());
        });
        return join === true ? ids.join(",") : ids;
    },

    /**
     * 判断是否选中行
     * @Return{Boolean}
     */
    isSelected: function () {
        return this.getSelectionModel().getSelection().length !== 0;
    },

    /**
     * 判断是否选中多行
     * @Return{Boolean}
     */
    isMultiSelected: function () {
        return this.getSelectionModel().getSelection().length > 1;
    }

});
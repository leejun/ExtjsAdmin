Ext.define("Admin.base.BaseFormWindow", {
    extend: 'Ext.window.Window',

    layout: 'fit',

    bodyStyle: "background:#f6f6f6;",
    closeToolText: '关闭窗口',

    width: 800,
    height: 550,

    initComponent: function () {
        var me = this;
        me.callParent(arguments);
    }
});
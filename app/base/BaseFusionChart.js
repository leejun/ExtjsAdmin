Ext.define("Admin.base.BaseFusionChart", {
    extend: 'Ext.Component',
    alias: 'widget.basefusionchart',

    //the tpl templete
    renderTpl: '<div></div>',

    //选择器查找div(Ext.Element)
    childEls: {
        divEl: {selectNode: 'div'}
    },

    //饼图文档 https://www.fusioncharts.com/charts/pie-doughnut-charts
    //线图文档 https://www.fusioncharts.com/charts/line-area-charts
    //柱图文档 https://www.fusioncharts.com/charts/column-bar-charts/
    //图标类型(pie2d, pie3d, line, column2d, ....)
    chartType: '',

    //图表宽度
    chartWidth: "100%",

    //图表高度
    chartHeight: "100%",

    //图表ID
    chartId: '',

    //初始文字
    ChartNoDataText: '数据正在加载中...',

    //是否删除logo
    deleteLogo: true,

    //默认数据
    defaultDataSource: {
        chart: {},
        data: []
    },

    initComponent: function () {
        var me = this;
        me.callParent();

        //当前ext组件渲染完成后触发 我们在此时开始渲染图表组件
        this.on('afterrender', this.afterrenderHandle, this);
        //当前ext组件跟随父布局改变大小时触发，我们需要改变图表的大小
        this.on('resize', this.resizeEventHandle, this);
    },

    /**
     * 初始化图表到组件
     */
    afterrenderHandle: function () {
        var me = this;

        //图表ID 用extjs工具生成 防止重复 用于后续查找FusionCharts
        me.chartId = Ext.id(null, 'myfusioncharts-idgen'),

            Ext.Function.defer(function () {
                var chart = new FusionCharts({
                    type: me.chartType, //图表类型
                    id: me.chartId, //图表ID 必须全局唯一
                    width: me.chartWidth, //图表宽度
                    height: me.chartHeight, //图表高度
                    dataFormat: 'json', //数据格式
                    dataSource: me.defaultDataSource,

                    //https://www.fusioncharts.com/dev/advanced-chart-configurations/events/classifying-events
                    events: {
                        //生命周期事件 渲染开始
                        "beforeRender": function (e, d) {
                            console.log("生命周期事件 渲染开始");
                        },
                        //生命周期事件 完成绘图
                        "drawComplete": function (e, d) {
                            var p = e.sender.ref.parentNode;
                            console.log("生命周期事件 完成绘图");
                        },
                        //生命周期事件 渲染完成
                        "renderComplete": function (e, d) {
                            var p = e.sender.ref.parentNode;
                            console.log("生命周期事件 渲染完成");
                            me.hiddenLogo(50);
                        },
                        //鼠标滑入事件
                        "dataplotRollOver": function (evt, data) {
                            console.log('鼠标滑入事件 :-> ' + data.categoryLabel + ' | ' + data.value);

                        },
                        //鼠标滑出事件
                        "dataplotRollOut": function (evt, data) {
                            console.log('鼠标滑出事件 :-> ' + data.categoryLabel + ' | ' + data.value);
                        },
                        //鼠标单击事件
                        "dataplotClick": function (evt, data) {
                            console.log('鼠标单击事件 :-> ' + data.categoryLabel + ' | ' + data.value);
                            me.fireEvent('onDataplotClick', evt, data);
                        }
                    }
                });

                //配置图表
                chart.configure({ChartNoDataText: me.ChartNoDataText});

                //把图表渲染到Ext.Component中
                chart.render(me.divEl.dom);

                //渲染完成后触发事件 用于填充数据
                me.fireEvent('successfullyRenderd');
            }, 100);
    },

    /**
     * 动态设置图表大小
     */
    resizeEventHandle: function (cmp, width, height, oldWidth, oldHeight, eOpts) {
        console.log("handleResizeEvent: width = " + width + "--- height = " + height);
        var chart = FusionCharts(this.chartId);
        if (chart) {
            chart.resizeTo(width, height);
        }
    },

    /**
     * 组件销毁之前需要先销毁图表组件
     */
    onDestroy: function () {
        FusionCharts(this.chartId).dispose();
        this.callParent();
    },

    /**
     * 设置数据源
     * @param {FusionCharts} dataSource
     */
    setDataSource: function (dataSource) {
        console.log('设置图表数据 :-> ' + Ext.JSON.encode(dataSource));
        FusionCharts(this.chartId).setChartData(dataSource);
    },

    /**
     * 获取数据源
     * @Return{FusionCharts dataSource}
     */
    getDataSource: function () {
        return FusionCharts(this.chartId).getJSONData;
    },

    /**
     * 获得图表对象
     * @Return{FusionCharts ChartObject}
     */
    getChart: function () {
        return FusionCharts(this.chartId);
    },

    /**
     * 打印图表
     */
    print: function () {
        FusionCharts(this.chartId).print();
    },

    /**
     * 隐藏logo
     * @param delay 延迟时间
     */
    hiddenLogo: function (delay) {
        var me = this;

        function hidden() {
            Ext.Function.defer(function () {
                var node = Ext.dom.Query.selectNode('g [transform^=translate]', me.divEl.dom);
                if (node) {
                    Ext.get(node).addCls('basefusionchart-hidden');
                    return;
                } else {
                    setTimeout(function () {
                        console.log('没找Logo-DOM到节点。循环删除.');
                        hidden();
                    }, 200);
                }
            }, delay);
        }

        if (this.deleteLogo) {
            hidden();
        }
    }

});
Ext.define("Admin.base.BaseSaveButton", {
    extend: 'Ext.Button',
    alias: 'widget.savebutton',

    text: '保存',
    iconCls: 'x-fa fa-save',
    ui: 'soft-purple'
});
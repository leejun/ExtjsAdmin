Ext.define("Admin.base.BaseActionPanel", {
    extend: 'Ext.container.Container',
    alias: 'widget.baseactionpanel',

    margin: '0 0 5 0',

    initComponent: function () {
        var me = this;
        me.callParent(arguments);
    }
});
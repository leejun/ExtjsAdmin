Ext.define("Admin.base.BaseDeleteButton", {
    extend: 'Ext.Button',
    alias: 'widget.delbutton',

    text: '删除',
    iconCls: 'x-fa fa-trash-alt',
    ui: 'soft-red'
});
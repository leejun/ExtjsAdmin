Ext.define("Admin.base.PagingToolbar", {
    extend: 'Ext.toolbar.Paging',
    beforePageText: '第',
    afterPageText: '页, 共{0}页',
    displayMsg: '显示{0} - {1}条, 共{2}条',
    emptyMsg: '没有可显示的数据',

    /**
     * 扩展父控件 增加设置每页显示多少行的功能
     * @Overrides {Ext.toolbar.Paging.getPagingItems}
     */
    getPagingItems: function () {
        var me = this;

        return [{
            itemId: 'first',
            tooltip: me.firstText,
            overflowText: me.firstText,
            iconCls: Ext.baseCSSPrefix + 'tbar-page-first',
            disabled: true,
            handler: me.moveFirst,
            scope: me
        }, {
            itemId: 'prev',
            tooltip: me.prevText,
            overflowText: me.prevText,
            iconCls: Ext.baseCSSPrefix + 'tbar-page-prev',
            disabled: true,
            handler: me.movePrevious,
            scope: me
        },
            '-',
            me.beforePageText,
            {
                xtype: 'numberfield',
                itemId: 'inputItem',
                name: 'inputItem',
                cls: Ext.baseCSSPrefix + 'tbar-page-number',
                allowDecimals: false,
                minValue: 1,
                hideTrigger: true,
                enableKeyEvents: true,
                keyNavEnabled: false,
                selectOnFocus: true,
                submitValue: false,
                // mark it as not a field so the form will not catch it when getting fields
                isFormField: false,
                width: me.inputItemWidth,
                margins: '-1 2 3 2',
                listeners: {
                    scope: me,
                    keydown: me.onPagingKeyDown,
                    blur: me.onPagingBlur
                }
            }, {
                xtype: 'tbtext',
                itemId: 'afterTextItem',
                text: Ext.String.format(me.afterPageText, 1)
            },
            '-',
            {
                itemId: 'next',
                tooltip: me.nextText,
                overflowText: me.nextText,
                iconCls: Ext.baseCSSPrefix + 'tbar-page-next',
                disabled: true,
                handler: me.moveNext,
                scope: me
            }, {
                itemId: 'last',
                tooltip: me.lastText,
                overflowText: me.lastText,
                iconCls: Ext.baseCSSPrefix + 'tbar-page-last',
                disabled: true,
                handler: me.moveLast,
                scope: me
            },
            '-',
            '每页显示',
            {
                xtype: 'combo',
                store: Ext.create('Ext.data.Store', {
                    fields: ['value', 'text'],
                    data: [{
                        value: 15, text: 15
                    }, {
                        value: 25, text: 25
                    }, {
                        value: 50, text: 50
                    }, {
                        value: 100, text: 100
                    }]
                }),
                queryMode: 'local',
                value: me.store.pageSize,
                width: 80,
                displayField: 'text',
                editable: false,
                valueField: 'value',
                margin: '0 5 0 5',
                listeners: {
                    change: me.doPageSizeChange,
                    scope: me
                }
            },
            '行',
            '-',
            {
                itemId: 'refresh',
                tooltip: me.refreshText,
                overflowText: me.refreshText,
                iconCls: Ext.baseCSSPrefix + 'tbar-loading',
                handler: me.doRefresh,
                scope: me
            }];
    },

    /**
     * 设置表格显示的行数,并刷新
     * @param {Ext.form.field.ComboBox} combo
     * @param {Array} records
     * @param {Object} eOpts
     */
    doPageSizeChange: function (combo, newValue, oldValue, eOpts) {
        var me = this;
        if (newValue) {
            me.store.pageSize = newValue;
            me.store.currentPage = 1;
            me.doRefresh();
        }
    }
});
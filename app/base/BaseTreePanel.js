Ext.define("Admin.base.BaseTreePanel", {
    extend: 'Ext.tree.Panel',
    alias: 'widget.basetreepanel',

    cls: 'user-grid',

    columnLines: false,
    headerBorders: false,

    initComponent: function () {
        var me = this;
        me.callParent(arguments);
    },

    /*获取选中行的ID*/
    getSelectedRecordId: function () {
        var record = this.getSelectionModel().getSelection()[0];
        return record ? record.getId() : '';
    },

    /*获取选中行Node对象*/
    getSelecedRecord: function () {
        var record = this.getSelectionModel().getSelection()[0];
        return record;
    },

    /*获取选中行的其它值*/
    getSelecedRecordValue: function (key) {
        var record = this.getSelectionModel().getSelection()[0];
        return record ? record.get(key) : '';
    },

    /*获取store当中修改的record数量*/
    getModifiedRecordSize: function () {
        return this.getStore().getModifiedRecords().length;
    },

    /*服务器刷新Store*/
    refreshView: function () {
        this.getStore().reload();
    },

    /*本地刷新*/
    localRefresh: function () {
        this.getView().refresh();
    },

    /*树节点清空选中状态*/
    deselectAll: function () {
        this.getSelectionModel().deselectAll();
    }
});
Ext.define("Admin.base.BaseHboxFieldContainer", {
    extend: 'Ext.form.FieldContainer',
    alias: 'widget.basehboxfieldcontainer',

    layout: 'hbox',

    defaults: {
        flex: 1,
        allowBlank: false,
        xtype: 'textfield',
        labelAlign: 'right'
    }
});
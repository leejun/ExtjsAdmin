Ext.define("Admin.base.BaseCancelButton", {
    extend: 'Ext.Button',
    alias: 'widget.cancelbutton',

    text: '取消',
    iconCls: 'x-fa fa-times-circle',
    ui: 'soft-red'
});
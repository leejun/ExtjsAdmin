Ext.define('Admin.base.BaseSearchForm', {
    extend: 'Ext.form.Panel',
    xtype: 'basesearchform',

    defaults : {
        xtype : 'textfield',
        labelWidth: 'auto',
        margin : '0 8 0 0'
    },

    layout : {
        type : 'hbox',
        align : 'stretch'
    }
});
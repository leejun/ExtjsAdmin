Ext.define("Admin.base.YesNoComboBox", {
    extend: 'Ext.form.field.ComboBox',
    alias: 'widget.yesnocombobox',

    editable: false,

    yesText: '是',
    noText: '否',
    yesValue: true,
    noValue: false,

    queryMode: 'local',
    displayField: 'text',
    valueField: 'code',

    initComponent: function () {
        var me = this;
        var store = Ext.create('Ext.data.Store', {
            fields: ['code', 'text'],
            data: [
                {'code': me.yesValue, 'text': me.yesText},
                {'code': me.noValue, 'text': me.noText}
            ]
        });

        me.callParent(arguments);
        me.bindStore(store);
    }
});
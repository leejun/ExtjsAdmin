/**
 * 扩展Ext.ID生成器,生成Long类型全局唯一ID
 */
Ext.define('Admin.base.LongIdGenerator', {
    extend: 'Ext.data.identifier.Generator',
    alias: 'data.identifier.longIdGenerator',

    generate: function () {
        var longId = null;

        /* 同步调用后台接口获取long类型ID */
        Ext.Ajax.request({
            async: false, //同步调用
            url: ServerUrl + '/AppController/generatorLongId',
            success: function(response, opts) {
                var obj = Ext.decode(response.responseText);
                longId = obj.data;
                console.log("longIdGenerator-> :" + longId);
            },
            failure: function(response, opts) {
                console.log('server-side failure with status code ' + response.status);
            }
        });

        return longId;
    }
});
Ext.define("Admin.base.BaseRefreshButton", {
    extend: 'Ext.Button',
    alias: 'widget.refreshbutton',

    text: '刷新',
    iconCls: 'x-fa fad fa-sync-alt',
    ui: 'soft-cyan'
});